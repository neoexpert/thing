package test;
import java.util.Locale;

import core.*;
import com.neoexpert.Log;
import java.util.Iterator;
import core.attribute.Attribute;
import core.controller.*;
import core.processor.*;
import java.util.Locale;
import org.json.JSONObject;
import static org.junit.Assert.*;
import static org.junit.Assume.*;
import org.junit.FixMethodOrder;
import org.junit.*;
import org.junit.runners.MethodSorters;
import templates.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CoreTest {
	public interface Logger{
		public void log(String s);
		public void info(String s);
		public void ok();
		public void error(String s);
		public void error(Throwable t);
	}
	private static Logger logger;
	public static void setLogger(Logger trl){
		logger=trl;	
	}	
	public static void createConsoleLogger(){
		logger=new Logger(){
			@Override
			public void log(String s){
				System.out.println(s);
			}
			@Override
			public void info(String s){
				System.out.println("[ " + b(s) + " ]");
			}
			@Override
			public void ok(){
				System.out.println("[ " + g("OK") + " ]");
			}
			@Override
			public void error(String s){
				System.out.print("[ " + r("ERROR") + " ] ");
				System.out.println(s);
			}
			@Override
			public void error(Throwable t){
				System.out.print("[ " + r("ERROR") + " ] ");
				t.printStackTrace(System.out);
			}
		};	
	}
	private static void _fail(String s){
		logger.error(s);
		fail(s);
	}

	@Test
	public void test1_checkRootNode() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.log("getting ROOT node...");
		Node root = c.getRoot();
		assertNotNull("ROOT missing",root);
		assertNull("ROOT has a parent",root.getParent());
		logger.ok();
	}

	@Test
	public void test2_checkUsersNode() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.log("getting USERS node...");
		Users users = c.getUsers();
		assertNotNull("Users missing",users);
		logger.ok();
	}

	@Test
	public void test3_checkCategoriesNode() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.log("getting ROOT Category");
		Category cat = c.getRootCategory();
		assertNotNull("ROOT Category missing",cat);
		logger.ok();
	}

	@Test
	public void test4_Users() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("LOGIN TEST");
		logger.log("logging in test User...");
		User u2 = c.userLogin("test", "test");
		assertEquals("logged in user does not equals added user",u, u2);
		logger.ok();
		logger.log("trying to login using wrong password...");
		u2 = c.userLogin("test", "tesT");
		assertNull("logged in with wrong password!",u2);
		logger.ok();
	}

	@Test
	public void test5_Nodes() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("NODES TEST");
		logger.log("getting ROOT node...");
		Node r = c.getRoot();
		logger.ok();
		logger.log("checking if test Node exists...");
		Node test = r.getChild("test");
		if (test != null) {
			logger.ok();
			logger.log("test Node exists! deleting...");
			c.remove(test, u);
			logger.ok();

		}
		else logger.ok();
		logger.log("adding test node to ROOT...");
		test = new Node(u.getID(), c);
		test.setName("test");
		r.addChild(test, u);
		logger.ok();
		logger.log("getting test node with getChild method from ROOT node....");
		Node test2 = c.getRoot().getChild("test");
		logger.ok();
		logger.log("checking if retrieved node equals to the added node...");
		assertEquals(test, test2);
		logger.ok();
		logger.log("adding some nodes to test node....");
		Node n1 = new Node(u.getID(), c);
		n1.setName("node");
		test.addChild(n1, u);
		Node n2 = new StringTemplate(u.getID(), c);
		n2.setName("template");
		test.addChild(n2, u);
		logger.ok();
		logger.log("checking added nodes...");
		assertEquals(n1, test.getChild("node"));
		assertEquals(n2, test.getChild("template"));
		logger.ok();
	}
	@Test
	public void test6_attrribites() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("ATTRIBUTES TEST");
		logger.log("getting test Node...");
		Node test=c.getRoot().getChild("test");		
		if(test==null)
			_fail("test node not found");
		else
			logger.ok();
		logger.log("addAttr...");
		Attribute a=test.addAttr("test_attribute",u);
		if(a==null)
						_fail("addAttr returned null");
		a=c	.getRoot()
				.getChild("test")
				.getAttribute("test_attribute");
		if(a==null)
						_fail("getAttribute returned null");
		logger.ok();
		logger.log("check setValue and getValue");
		a.setValue("test_value",u);
		if(!"test_value".equals(a.getValue()))
			_fail("returned value is wrong");
		if(!"test_value".equals(c.getRoot()
										.getChild("test")
										.getAttribute("test_attribute")
										.getValue()))
			_fail("returned value is wrong");
		logger.ok();

		logger.log("deleteAttr");
		c	.getRoot()
				.getChild("test")
				.deleteAttr("test_attribute",u);
		a=c	.getRoot()
				.getChild("test")
				.getAttribute("test_attribute");
		if(a==null)
			logger.ok();
		else
			_fail("attribute was not deleted");

	}
	@Test
	public void test7_NodeIterator() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("NODEITERATOR TEST");
		logger.log("getting test Node...");
		Node test=c.getRoot().getChild("test");		
		logger.log("getting test Node...");
		Iterator<Node> it=test.getChildren().build();
		logger.ok();
		logger.log("iterate over children...");
		while(it.hasNext()){
			logger.log("child found:");
			logger.log(it.next().toString());
		}
		logger.ok();
	}

	@Test
	public void test8_processorTest() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("PROCESSOR TEST");
		int l=1;

		logger.log("test getNode...");
		JSONObject jo=new JSONObject();
		jo.put("cmd", "getNode");
		jo.put("_id" , u.getID().get());
		jo.put("lock" , l++);
		logger.log("request");
		logger.log(jo.toString());
		Object r=pc(jo);
		assertNotNull(r);
		logger.log("response");
		logger.log(r.toString());
		logger.ok();

		logger.log("test addNode...");
		jo=new JSONObject();
		jo.put("cmd", "addNode");
		JSONObject child=new JSONObject();
		child.put("parent" , u.getID().get());
		child.put("name","test_node");
		jo.put("node",child);
		jo.put("lock" , l++);
		logger.log("request");
		logger.log(jo.toString());
		 r=pc(jo);
		assertNotNull(r);
		logger.log("response");
		logger.log(r.toString());
		logger.ok();

		logger.log("test getChild...");
		 jo=new JSONObject();
		jo.put("cmd", "getChild");
		jo.put("parent" , u.getID().get());
		jo.put("name" , "test_node");
		jo.put("lock" , l++);
		logger.log("request");
		logger.log(jo.toString());
		 r=pc(jo);
		assertNotNull(r);
		logger.log("response");
		logger.log(r.toString());
		logger.ok();
	}

	private Object r;
	private Object pc(JSONObject jo){
		r=null;
		ResultWriter rw=new ResultWriter() {
			@Override
			public void write(String result) {
				r=result;
			}

			@Override
			public void write(JSONObject result) {
				r=result;
			}
		};
		JSONParams hp = new JSONParams(jo, rw);
		Client client = new Client(c, u, Locale.ENGLISH){
		
    public String getSessionID(){
    return "42";
    }
		};
		hp.setClient(client);

		Processor.process(hp, c, u, Locale.ENGLISH);
		return r;
	}

	private static Controller c;
	private static User u;

	private static long starttime;
	@BeforeClass
	public static void getController() {
		starttime=System.currentTimeMillis();
		if(logger==null)
			createConsoleLogger();
		logger.info("THING TEST");
		logger.log("getController...");
		try{
		c= new ControllerBuilder()
				.setDB("test")
				.setDomain("test")
				.setLocale(Locale.ENGLISH)
				.getController();
		}
		catch(RuntimeException e){
			logger.error(e);
		//	assumeTrue(false);
		}
		//assumeTrue(c.isConnected());
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}

		logger.info("INIT DB");
		c.init();
		logger.ok();

		logger.log("check if test user.exists...");
		u = c.getUser("test");
		if (u != null) {
			logger.log("test user exists! removing...");

			c.remove(u, u);
			logger.ok();
		}
		else logger.ok();
		logger.log("adding User...");
		u = c.addUser("test", "test");
		if (u == null) {
			_fail("user not exits");
		} else {
			logger.ok();
		}
		u.setAttribute("email_confirmed", true).commit(u);
		assertTrue((Boolean)u.getAttribute("email_confirmed").getValue());
		
	}

	@AfterClass
	public static void a_delete_test_Nodes() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.log("getting previously added child nodes of the test node...");
		Node node=c.getRoot()
						.getChild("test")
						.getChild("node");
		Node template=c.getRoot()
						.getChild("test")
						.getChild("template");
		logger.log("delete test node recursively... ");
		int deleted=c.delete(c.getRoot().getChild("test"), u);
		logger.log("deleted nodes: "+deleted);
		logger.ok();

		logger.log("checking if the children of test node still exists");
		node = c.getNode(node.getID());
		if (node != null) {
			_fail("a child of test node was not deleted");
		}

		if (!(template instanceof StringTemplate)) {
			_fail("the added Node has a wrong Type");
		}
		logger.ok();
		logger.log("adding test node to ROOT...");
		Node test = new Node(u.getID(), c);
		test.setName("test");
		Node r = c.getRoot();
		r.addChild(test, u);
		logger.ok();

	}

	private static String r(String s) {
		return "\033[31;1m" + s + "\033[0m";
	}

	private static String g(String s) {
		return "\033[32;1m" + s + "\033[0m";
	}

	private static String b(String s) {
		return "\033[34;1m" + s + "\033[0m";
	}
}
