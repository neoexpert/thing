<!DOCTYPE HTML>
<html>
<head>
<title>login</title>
<link rel="icon" type="image/png" href="/favicon.png" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="theme-color" content="#ffffff" />
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

</head>
<body>

	<div class="wrapper" id="wrapper_login">
		<ul class="navigation sub-nav">
			<li><a href="/de/login" class="active">Anmelden</a></li>
			<li><a href="/de/registrieren">Registrieren</a></li>
		</ul>
		<div class="form" id="form-width">
			<form id="login_form" action="/login" method="post">
				<div class="padding-bottom margin-top">
					<input required autofocus type="text" name="username"
						placeholder="E-Mail-Adresse*" /> <input required type="password"
						name="password" placeholder="Passwort*" />
					<div id="forgotpw">
						<a href="forgotpassword">Passwort vergessen?</a>
					</div>
					<input class="button" type="submit" value="Anmelden">
				</div>
			</form>
		</div>
		<script>
			$("#login_form").submit(function(event) {
				event.preventDefault();

				var f = $(this);
				var username = f.find("input[name=username]").val();
				var password = f.find("input[name=password]").val();
				$.post("/login", {
					username : username,
					password : password
				}, login);
			});
			function login(response) {
				window.location = response;
			}
		</script>
	</div>
</body>
</html>