package tools;

import com.neoexpert.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author neoexpert
 */
public class HtmlToPdf {

    /**
     *
     * @param html
     * @throws IOException
     * @throws InterruptedException
     */
    public static void htmltopdf(String html) throws IOException, InterruptedException  {
		Runtime r = Runtime.getRuntime();
		Process p = r.exec("xhtml2pdf Formular-html.html");
		p.waitFor();
		BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;

		while ((line = b.readLine()) != null) {
			Log.log(line);
		}

		b.close();

	
	}
}
