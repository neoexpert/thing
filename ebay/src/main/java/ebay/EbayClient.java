package ebay;

import com.neoexpert.Log;
import core.User;
import core.controller.Controller;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.Base64;
import java.util.UUID;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import shop.Product;

/**
 *
 * @author neoexpert
 */
public class EbayClient {

    private String sessionID;

    /**
     *
     */
    public EbayClient() {

    }

    /**
     *
     * @param out
     * @throws IOException
     */
    public void testIt(PrintWriter out) throws IOException {

        String https_url = "https://api.ebay.com/sell/inventory/v1/offer";
        URL url;
        try {

            url = new URL(https_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("Authorization", "Bearer <OAUTH_token>");
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");

            String content = new JSONObject().toString();
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Language", "en-US");
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(content.getBytes());
            }
            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            print_content(con, out);

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }

    }

    private void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {

                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                Certificate[] certs = con.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
                Log.error(e);
            } catch (IOException e) {
                Log.error(e);
            }

        }

    }

    private String get_content(HttpsURLConnection con) {
        if (con != null) {
            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String input;

                while ((input = br.readLine()) != null) {
                    sb.append(input);

                }
                br.close();
                return sb.toString();

            } catch (IOException e) {
                Log.error(e);
            }

        }
        return null;
    }

    private void print_content(HttpsURLConnection con, PrintWriter out) {
        if (con != null) {

            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null) {
                    out.println(input);

                    // JSONObject jo;
                    // try {
                    // jo = new JSONObject(input);
                    // out.println(jo.toString(2));
                    // this.access_tocken = jo.getString("access_token");
                    // if (!access_tocken.isEmpty()) {
                    // out.println("logged in");
                    //
                    // }
                    // getItems(out, access_tocken);
                    //
                    // } catch (JSONException e) {
                    // Log.error(e);
                    // }
                }
                br.close();

            } catch (IOException e) {
                out.println(e);
                Log.error(e);
            }

        }

    }

    /**
     *
     * @param apiurl
     * @param req
     * @param response
     * @param apicall
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public String getSessionID(String apiurl, String req, HttpServletResponse response, String apicall)
            throws ParserConfigurationException, SAXException, IOException {
        URL url;
        PrintWriter out = response.getWriter();
        try {

            url = new URL(apiurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            // con.setRequestProperty("Content-Type", "text/xml");
            // con.setRequestProperty("X-EBAY-API-APP-ID",
            // "Alexande-Test-PRD-f2466ad41-daaad47d");
            // con.setRequestProperty("X-EBAY-API-SITE-ID", "0");
            // con.setRequestProperty("X-EBAY-API-REQUEST-ENCODING", "xml");
            con.setRequestProperty("X-EBAY-API-COMPATIBILITY-LEVEL", "1029");
            con.setRequestProperty("X-EBAY-API-CALL-NAME", apicall);
            con.setRequestProperty("X-EBAY-API-SITEID", "0");

            con.setRequestProperty("X-EBAY-API-APP-NAME", "Alexande-Test-PRD-f2466ad41-daaad47d");
            con.setRequestProperty("X-EBAY-API-DEV-NAME", "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            con.setRequestProperty("X-EBAY-API-CERT-NAME", "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            // con.setRequestProperty("X-EBAY-API-DEV-NAME",
            // "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            // con.setRequestProperty("X-EBAY-API-CERT-NAME",
            // "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            /*
			 * ' 'X-EBAY-API-SITEID: 0', 'X-EBAY-API-CALL-NAME: GetSellerList'
             */
            // con.setRequestProperty ("Authorization", "Bearer "+token);
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(req.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String xml = "";
                String input;

                while ((input = br.readLine()) != null) {
                    xml += input + "\n";
                }
                out.println(xml);
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(xml)));
                Element rootElement = document.getDocumentElement();
                Node child = rootElement.getLastChild();

                String sessID = child.getFirstChild().getNodeValue();
                out.println("<p>" + sessID + "</p>");
                this.sessionID = sessID;
                String loginurl = "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&RuName=Alexander_Tyury-Alexande-Test-P-hgtzx&SessID="
                        + sessID;

                response.sendRedirect(loginurl);
                out.println("<a href='" + loginurl + "'>login</a>");
                br.close();
                return sessID;
            } catch (IOException e) {
                out.println(e);
                Log.error(e);
            }

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
        return null;
    }

    /**
     *
     * @param apiurl
     * @param out
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public String fetchToken(String apiurl, PrintWriter out)
            throws ParserConfigurationException, SAXException, IOException {
        URL url;
        try {

            url = new URL(apiurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            // con.setRequestProperty("Content-Type", "text/xml");
            // con.setRequestProperty("X-EBAY-API-APP-ID",
            // "Alexande-Test-PRD-f2466ad41-daaad47d");
            // con.setRequestProperty("X-EBAY-API-SITE-ID", "0");
            // con.setRequestProperty("X-EBAY-API-REQUEST-ENCODING", "xml");
            con.setRequestProperty("X-EBAY-API-COMPATIBILITY-LEVEL", "1029");
            con.setRequestProperty("X-EBAY-API-CALL-NAME", "FetchToken");
            con.setRequestProperty("X-EBAY-API-SITEID", "0");

            con.setRequestProperty("X-EBAY-API-APP-NAME", "Alexande-Test-PRD-f2466ad41-daaad47d");
            con.setRequestProperty("X-EBAY-API-DEV-NAME", "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            con.setRequestProperty("X-EBAY-API-CERT-NAME", "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            // con.setRequestProperty("X-EBAY-API-DEV-NAME",
            // "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            // con.setRequestProperty("X-EBAY-API-CERT-NAME",
            // "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            /*
			 * ' 'X-EBAY-API-SITEID: 0', 'X-EBAY-API-CALL-NAME: GetSellerList'
             */
            // con.setRequestProperty ("Authorization", "Bearer "+token);
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                String req = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<FetchTokenRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">\r\n  <RequesterCredentials>    \r\n  </RequesterCredentials>\r\n\t<ErrorLanguage>en_US</ErrorLanguage>\r\n\t<WarningLevel>High</WarningLevel>\r\n   <!-- Enter the SessionID created for the user for whom the token needs to be retrieved -->\r\n <SessionID>"
                        + sessionID + "</SessionID>\r\n</FetchTokenRequest>";
                wr.write(req.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            try {

                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String xml = "";
                String input;

                while ((input = br.readLine()) != null) {
                    xml += input + "\n";
                }
                // out.println(xml);
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(xml)));
                Element rootElement = document.getDocumentElement();
                NodeList childlist = rootElement.getElementsByTagName("eBayAuthToken");
                Node child = childlist.item(0);
                String authtoken = child.getFirstChild().getNodeValue();
                br.close();

                return authtoken;
                // out.println("eBayAuthToken: " + child.getTextContent());

            } catch (IOException e) {
                out.println(e);
                Log.error(e);
            }

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
        return null;
    }

    /**
     *
     * @param authtoken
     * @param from
     * @param to
     * @return
     */
    public String getSellerList(String authtoken, String from, String to) {
        String apiurl = "https://api.ebay.com/ws/api.dll";
        URL url;
        try {

            url = new URL(apiurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            // con.setRequestProperty("Content-Type", "text/xml");
            // con.setRequestProperty("X-EBAY-API-APP-ID",
            // "Alexande-Test-PRD-f2466ad41-daaad47d");
            // con.setRequestProperty("X-EBAY-API-SITE-ID", "0");
            // con.setRequestProperty("X-EBAY-API-REQUEST-ENCODING", "xml");
            con.setRequestProperty("X-EBAY-API-APP-NAME", "Alexande-Test-PRD-f2466ad41-daaad47d");
            con.setRequestProperty("X-EBAY-API-DEV-NAME", "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            con.setRequestProperty("X-EBAY-API-CERT-NAME", "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            con.setRequestProperty("X-EBAY-API-SITEID", "0");
            con.setRequestProperty("X-EBAY-API-COMPATIBILITY-LEVEL", "967");
            con.setRequestProperty("X-EBAY-API-CALL-NAME", "GetSellerList");

            // con.setRequestProperty("X-EBAY-API-DEV-NAME",
            // "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            // con.setRequestProperty("X-EBAY-API-CERT-NAME",
            // "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            /*
			 * ' 'X-EBAY-API-SITEID: 0', 'X-EBAY-API-CALL-NAME: GetSellerList'
             */
            // con.setRequestProperty ("Authorization", "Bearer "+token);
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            String req = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<GetSellerListRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">\r\n  <RequesterCredentials>\r\n    <eBayAuthToken>"
                    + authtoken
                    + "</eBayAuthToken>\r\n  "
                    + "</RequesterCredentials>\r\n  "
                    + "<ErrorLanguage>en_US</ErrorLanguage>\r\n"
                    + "  <WarningLevel>High</WarningLevel>\r\n"
                    + "  <GranularityLevel>Coarse</GranularityLevel> \r\n"
                    + "  <StartTimeFrom>" + from + "</StartTimeFrom> \r\n"
                    + "  <StartTimeTo>" + to + "</StartTimeTo> \r\n"
                    + "  <IncludeWatchCount>true</IncludeWatchCount> \r\n"
                    + "  <Pagination> \r\n    <EntriesPerPage>10</EntriesPerPage> \r\n"
                    + "    <PageNumber>1</PageNumber>  \r\n"
                    + "  </Pagination> \r\n"
                    + "</GetSellerListRequest>";
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(req.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            return get_content(con);

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
        return null;
    }

    public void getItem(core.Node n, User u, Controller c, String authtoken, String itemID, PrintWriter out) throws ParserConfigurationException, SAXException {
        String apiurl = "https://api.ebay.com/ws/api.dll";
        URL url;
        try {

            url = new URL(apiurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            // con.setRequestProperty("Content-Type", "text/xml");
            // con.setRequestProperty("X-EBAY-API-APP-ID",
            // "Alexande-Test-PRD-f2466ad41-daaad47d");
            // con.setRequestProperty("X-EBAY-API-SITE-ID", "0");
            // con.setRequestProperty("X-EBAY-API-REQUEST-ENCODING", "xml");
            con.setRequestProperty("X-EBAY-API-APP-NAME", "Alexande-Test-PRD-f2466ad41-daaad47d");
            con.setRequestProperty("X-EBAY-API-DEV-NAME", "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            con.setRequestProperty("X-EBAY-API-CERT-NAME", "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            con.setRequestProperty("X-EBAY-API-SITEID", "0");
            con.setRequestProperty("X-EBAY-API-COMPATIBILITY-LEVEL", "967");
            con.setRequestProperty("X-EBAY-API-CALL-NAME", "GetItem");

            // con.setRequestProperty("X-EBAY-API-DEV-NAME",
            // "1c142dc6-17be-4cb3-961f-90d8261f3ef5");
            // con.setRequestProperty("X-EBAY-API-CERT-NAME",
            // "PRD-2466ad41d39f-8fbe-4135-aec2-97d9");

            /*
			 * ' 'X-EBAY-API-SITEID: 0', 'X-EBAY-API-CALL-NAME: GetSellerList'
             */
            // con.setRequestProperty ("Authorization", "Bearer "+token);
            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            String req = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                    + "<GetItemRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">\n"
                    + "  <RequesterCredentials>\n"
                    + "    <eBayAuthToken>" + authtoken + "</eBayAuthToken>\n"
                    + "  </RequesterCredentials>\n"
                    + "  <ItemID>" + itemID + "</ItemID>\n"
                    + "  <DetailLevel>ReturnAll</DetailLevel>\n"
                    + "</GetItemRequest>";
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(req.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            String xml = get_content(con);
            System.out.println(xml);
            out.println(xml);

            //out.print(xml);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element item = document.getDocumentElement();
            item = getTagByName(item, "Item");

            Product child = new Product(u.getID(), c);
            child.setName(UUID.randomUUID().toString());

            try {
                child = (Product) n.addChild(child, u);
                NodeList attrs = item.getChildNodes();

                /*for (int j = 0; j < attrs.getLength(); j++) {
                    Node attr = attrs.item(j);
                    switch (attr.getNodeName()) {
                        case "Title":
                            child.setName(attr.getFirstChild().getNodeValue()).commit(u);
                            break;
                        default:
                            child.setAttribute(attr.getNodeName(), attr.getFirstChild().getNodeValue()).commit(u);
                            break;
                    }
            
                }*/
                setAttribute("Currency", item, child,out,u);
                setAttribute("Item.BuyerGuaranteePrice", item, child,out,u);
                setAttribute("Item.BuyItNowPrice", item, child,out,u);
                setAttribute("Description", item, child,out,u);
                setAttribute("ItemID", item, child,out,u);
                //setAttribute("ItemPolicyViolation.PolicyID", item, child,out,u);
                setAttribute("PictureDetails.ExtendedPictureDetails.PictureURLs.eBayPictureURL", item, child,out,u);
                //External image path - Ulf
                setAttribute("Item.PictureDetails.ExtendedPictureDetails.PictureURLs .ExternalPictureURL", item, child,out,u);
                setAttribute("PictureDetails.PictureURL", item, child,out,u);
                setAttribute("PrimaryCategory.CategoryID", item, child,out,u);
                setAttribute("PrimaryCategory.CategoryName", item, child,out,u);
                setAttribute("SecondaryCategory.CategoryID", item, child,out,u);
                setAttribute("SecondaryCategory.CategoryName", item, child,out,u);
                setAttribute("ProductListingDetails.BrandMPN", item, child,out,u);
                //setAttribute("ProductListingDetails.Copyright", item, child,out,u);
                setAttribute("ProductListingDetails.EAN", item, child,out,u);
                setAttribute("ProductListingDetails.ISBN", item, child,out,u);
                //setAttribute("Quantity", item, child);
                //setAttribute("Site", item, child);
                setAttribute("SKU", item, child,out,u);
                setAttribute("SubTitle", item, child,out,u);
                setAttribute("Title", item, child,out,u);
                //setAttribute("UnitInfo", item, child);
                //setAttribute("UUID", item, child);

            } catch (Exception e) {
                Log.error(e);
            }

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
    }

    /**
     *
     * @param n
     * @param c
     * @param u
     * @param out
     * @param authtoken
     * @param from
     * @param to
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void importEbayItems(core.Node n, Controller c, User u, PrintWriter out, String authtoken, String from, String to) throws ParserConfigurationException, SAXException, IOException {
        String xml = getSellerList(authtoken, from, to);
        n.setAttribute("XML_RESPONSE", xml).commit(u);

        //out.print(xml);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(xml)));
        Element rootElement = document.getDocumentElement();
        NodeList items = rootElement.getElementsByTagName("Item");
        out.println(xml);

        for (int i = 0; i < items.getLength(); i++) {
            try {
                Element item = (Element) items.item(i);
                NodeList itemids = item.getElementsByTagName("ItemID");
                String itemid = itemids.item(0).getTextContent();

                out.println("itemID:" + itemids.item(0));
                getItem(n, u, c, authtoken, itemid, out);
            } catch (Exception e) {
                out.println(i + ":" + e);

            }

        }
        out.println("imported:" + items.getLength());

    }

    private void getItems(PrintWriter out, String token) {
        String https_url = "https://api.ebay.com/sell/inventory/v1/inventory_item?limit=5&offset=0";
        URL url;
        try {

            url = new URL(https_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            con.setRequestProperty("Authorization", "Bearer " + token);
            con.setRequestProperty("Accept", "application/json");

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            print_content(con, out);

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
    }

    /**
     *
     * @param authurl
     * @param code
     * @param out
     */
    public void getOAuthToken(String authurl, String code, PrintWriter out) {
        URL url;
        try {

            url = new URL(authurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "Basic " + new String(Base64.getEncoder()
                    .encode("Alexande-Test-PRD-f2466ad41-daaad47d:PRD-2466ad41d39f-8fbe-4135-aec2-97d9".getBytes())));

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            String content = "grant_type=authorization_code&code=" + code
                    + "&redirect_uri=Alexander_Tyury-Alexande-Test-P-hgtzx";
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(content.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            print_content(con, out);

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
    }

    /**
     *
     * @param callurl
     * @param req
     * @param out
     */
    public void makeCall(String callurl, String req, PrintWriter out) {
        URL url;
        try {

            url = new URL(callurl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "Basic " + new String(Base64.getEncoder()
                    .encode("Alexande-Test-PRD-f2466ad41-daaad47d:PRD-2466ad41d39f-8fbe-4135-aec2-97d9".getBytes())));

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(req.getBytes());
            }

            // dumpl all cert info
            print_https_cert(con);

            // dump all the content
            print_content(con, out);

        } catch (MalformedURLException e) {
            Log.error(e);
        } catch (IOException e) {
            Log.error(e);
        }
    }

    /**
     *
     * @param sessID
     */
    public void setSessionID(String sessID) {
        this.sessionID = sessID;
    }

    private Element getTagByName(Element node, String name) {
        NodeList itemids = node.getElementsByTagName(name);
        return (Element) itemids.item(0);
    }

    private void setAttribute(String path, Element item, Product child, PrintWriter out, User u) {
         String[] names;
        try {
            names= path.split("\\.");
            for (String name : names) {
                item = getTagByName(item, name);
                            out.println(name);

            }
            child.setAttribute(names[names.length-1], item.getTextContent()).commit(u);
        } catch (Exception e) {
            out.println(path);
            e.printStackTrace(out);
        }

    }
}
