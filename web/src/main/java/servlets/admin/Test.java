package servlets.admin;

import com.neoexpert.Log;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import test.CoreTest;

/**
 * Servlet implementation class Export
 */
@WebServlet(asyncSupported = true, urlPatterns = {"/admin/test"})
public class Test extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Test() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		response.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User u = c.getUser(session);
		Locale lang = HttpController.getLang(request);

		if (!u.isLoggedIn("admin")) {
			// RequestDispatcher rd =
			// request.getRequestDispatcher("/WEB-INF/login.jsp");

			// rd.forward(request, response);
            String uri = WebControllerBuilder.getFullURL(request);
            session.setAttribute("redirectTO", uri);
     		Log.log("redirectTO: " + uri);
			response.sendRedirect("/" + lang.getLanguage() + "/login");

			return;
		}
		response.setContentType("text/html");

		response.setBufferSize(1);
		PrintWriter out = response.getWriter();
		out.write("<html>");

		out.write("<head>");
		out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">");

		out.write("<style>");
		out.write("body{background-color:#000;color:#fff;font-family: \"Lucida Console\", Monaco, monospace;}");
		out.write("body:nth-child(2n){background-color:#003;}");

		out.write(".green{color:#0f0}");
		out.write(".red{color:#f00}");
		out.write(".blue{color:#88f}");
		out.write(".ok{float:right;}");

		out.write("</style>");
		out.write("</head>");

		out.write("<body>");

		CoreTest test = new CoreTest();

		CoreTest.Logger logger = new CoreTest.Logger() {
			@Override
			public void log(String s) {
				out.println("<br><span>" + s + "</span>");
				out.flush();
			}

			@Override
			public void info(String s) {
				out.println("<div class='blue'>" + s + "</div>");
				out.flush();

			}

			@Override
			public void ok() {
				out.println("<div class='ok'>[ <span class='green'>OK</span> ]</div>");
				out.flush();

			}

			@Override
			public void error(String s) {
				out.println("<div class='error'>[ <span class='red'>ERROR</span> ]</div>");
				out.println("<div>" + s + "</div>");
				out.flush();

			}
			@Override
			public void error(Throwable t){
				out.println("<div class='error'>[ <span class='red'>ERROR</span> ]</div>");
				out.println("<div class='stacktrace'>");
				t.printStackTrace(out);
				out.println("</div>");
				out.flush();
			}
		};

		test.setLogger(logger);
		Class tClass = test.getClass();
		Method[] methods = tClass.getMethods();
		ArrayList<Method> torun=new ArrayList<>();
		ArrayList<Method> afterclass=new ArrayList<>();
		for (Method m : methods) {
			if(m.isAnnotationPresent(org.junit.Test.class))
				torun.add(m);
			if(m.isAnnotationPresent(org.junit.BeforeClass.class))
				try{
					m.invoke(test);
				}
				catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
					Log.error(e);
					logger.error(e);
				}
			if(m.isAnnotationPresent(org.junit.AfterClass.class))
				afterclass.add(m);
		}
		Collections.sort(torun,new MethodComparator());
		for(Method m:torun)
						try{
						m.invoke(test);
						}
						catch(Exception e){
							Log.error(e);
							logger.error(e);
						}
		for(Method m:afterclass)
						try{
						m.invoke(null);
						}
						catch(Exception e){
							Log.error(e);
							logger.error(e);
						}
		out.write("</body>");
		out.write("</html>");


	}
	public class MethodComparator implements Comparator<Method> {

    public int compare(Method m1, Method m2) {

        int val = m1.getName().compareTo(m2.getName());
        if (val == 0) {
            val = m1.getParameterTypes().length - m2.getParameterTypes().length;
            if (val == 0) {
                Class[] types1 = m1.getParameterTypes();
                Class[] types2 = m2.getParameterTypes();
                for (int i = 0; i < types1.length; i++) {
                    val = types1[i].getName().compareTo(types2[i].getName());

                    if (val != 0) {
                        break;
                    }
                }
            }
        }
        return val;
    }

}

}
