package core;

import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 *
 * @author neoexpert
 */
public class GetHttpSessionConfigurator extends ServerEndpointConfig.Configurator {

    /**
     *
     * @param config
     * @param request
     * @param response
     */
    @Override
	public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response) {
		Map<String, Object> props = config.getUserProperties();

		String host = request.getHeaders().get("host").get(0);
		int i = host.indexOf(':');
		if (i > 0)
			host = host.substring(0, host.indexOf(':'));
		props.put("domain", host);

		HttpSession httpSession = (HttpSession) request.getHttpSession();
		if (httpSession == null) {
			super.modifyHandshake(config, request, response);
			return;
		}

		props.remove(HttpSession.class.getName());
		props.put(HttpSession.class.getName(), httpSession);

	}
}