package core.attribute;

import core.Node;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.processor.ProcessorInterface;
import java.util.Locale;
import org.bson.Document;
import templates.Template;

/**
 *
 * @author neoexpert
 */
public class Configuration extends Node {
	static {
		addChildType(Configuration.class, Option.class);
	}

    /**
     *
     * @param c
	 * @param doc
     */
    public Configuration(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @param pi
     */
    @Override
	public void getChildrenTemplates(ProcessorInterface pi) {
		String selection = pi.getParameter(getName());
		NodeIterator<Option> it = getChildren().sort("cid", false).build();
		while (it.hasNext()) {
			Option child = it.next();
			Template t = child.getTemplate();
			if (t == null)
				continue;

			String wt = t.getWrapperTag();
			if (child.getName().equals(selection))
				pi.writeResult("<" + wt + " class='option-selected " + child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH)
						+ " unfilled' data-id='" + child.getID() + "' data-name='" + child.getName() + "'>");
			else
				pi.writeResult("<" + wt + " class='" + child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH)
						+ " unfilled' data-id='" + child.getID() + "' data-name='" + child.getName() + "'>");
			pi.writeResult(t.toString());
			pi.writeResult("</" + wt + ">");
		}
	}

}
