package core.processor;

import core.Client;
import core.User;
import core.controller.Controller;
import core.controller.NodeIterator;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public abstract class ProcessorInterface {

	Client client;

	public Client getClient() {
		return client;
	}

	public void setClient(Client c) {
		this.client = c;
	}

	public String getSessionID() {
		return client.getSessionID();
	}

	protected String cmd;

	/**
	 *
	 * @param string
	 * @return
	 */
	public abstract String getParameter(String string);

	/**
	 *
	 * @return
	 */
	public abstract Set<String> getKeys();

	/**
	 *
	 * @param result
	 */
	public abstract void writeResult(String result);

	/**
	 *
	 * @param result
	 */
	public abstract void writeResult(JSONObject result);

	/**
	 *
	 * @param string
	 * @return
	 */
	public abstract String[] getParameterValues(String string);

	public abstract JSONObject getJSONObject(String key);

	/**
	 *
	 * @param key
	 * @return
	 */
	public abstract String remove(String key);

	/**
	 *
	 * @param key
	 * @return
	 */
	public abstract JSONObject get(String key);

	/**
	 *
	 * @param key
	 * @param jo
	 */
	public abstract void put(String key, JSONObject jo);

	/**
	 *
	 * @param it
	 */
	public void setIterator(NodeIterator it) {
		client.setIterator(it);
	}

	/**
	 *
	 * @return
	 */
	public abstract JSONObject toJSON();

	/**
	 *
	 * @param e
	 */
	public abstract void error(Exception e);

	/**
	 *
	 * @param cmd
	 */
	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	/**
	 *
	 * @return
	 */
	public String getCmd() {
		return cmd;
	}

	/**
	 *
	 * @return
	 */
	public abstract String getURL();

	/**
	 *
	 * @param name
	 * @param c
	 * @param u
	 * @throws Exception
	 */
	public abstract void renderSite(String name, Controller c, User u) throws Exception;

}
