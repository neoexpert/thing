package core.controller.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import core.*;
import core.attribute.Attribute;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.id.ObjectID;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import com.neoexpert.Log;
import core.controller.ControllerBuilder;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

/**
 *
 * @author neoexpert
 */
public class MongoController2 extends Controller {

	//private final MongoClient mongo;
	private final MongoCollection<Document> nodes;
	private final MongoCollection<Document> counters;

	/**
	 *
	 */
	private final Locale lang;
	private final MongoDatabase db;

	private final MongoClient mongo;

	/**
	 *
	 * @param mongo
	 * @param domain
	 * @param lang
	 */
	public MongoController2(MongoClient mongo, String domain, Locale lang) {

		this.mongo = mongo;
		this.lang = lang;

		db = mongo.getDatabase(ControllerBuilder.MONGO_DB);

		nodes = db.getCollection(ControllerBuilder.MONGO_NODES);
		counters = db.getCollection("counters");

		init(domain, false);

	}

	/**
	 *
	 * @param mongo
	 * @param domain
	 * @param root
	 * @param uc
	 * @param lang
	 */
	public MongoController2(MongoClient mongo, String domain, boolean root, Locale lang) {
		this.mongo = mongo;
		this.lang = lang;
		db = mongo.getDatabase(ControllerBuilder.MONGO_DB);

		nodes = db.getCollection(ControllerBuilder.MONGO_NODES);
		counters = db.getCollection("counters");

		init(domain, root);
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public int getNextSequence(String name) {
		Document find = new Document();
		find.put("_id", name);
		Document update = new Document();
		update.put("$inc", new Document("seq", 1));
		Document obj = counters.findOneAndUpdate(find, update);
		if (obj == null) {
			counters.insertOne(find.append("seq", 2));
			return 1;
		}
		int huhu = ((Number) obj.get("seq")).intValue();
		return huhu;
	}

	/**
	 *
	 * @param n
	 * @param u
	 * @return
	 */
	@Override
	public Node addNode(Node n, User u) {
		Map<String, Object> doc = n.doc;
		doc.put("_id", ObjectID.getRandomID());
		doc.put("cid", n.getNextCID());

		doc.put("parent", n.getParentID().get());
		doc.put("type", n.getClass().getName());
		doc.put("path", n.getPath());

		doc.put("updated", System.currentTimeMillis());
		doc.put("created", System.currentTimeMillis());
		doc.put("last_cid", 0);

		List<String> groups = new ArrayList<>();

		groups.add("users");
		doc.put("group", groups);
		doc.put("rights", 0x777);

		if (u != null) {
			doc.put("owner", u.getID().get());
		}

		List<Object> involved = new ArrayList<>();
		if (u != null) {
			involved.add(u.getID().get());
		}

		doc.put("involved", involved);
		try {
			Document mongodoc = new Document();
			mongodoc.putAll(doc);
			nodes.insertOne(mongodoc);
		} catch (MongoWriteException e) {
			if (e.getCode() == 11000) {

				NodeExistsException ex = new NodeExistsException(e);

				throw ex;
			}
		}

		n.onCreate(u);
		return n;
	}

	/**
	 *
	 * @param parent
	 * @param attrname
	 * @return
	 */
	@Override
	public boolean nodeExists(ObjectID parent, String attrname) {
		BasicDBObject fields = new BasicDBObject();

		fields.put("parent", parent.get());
		fields.put("name", attrname);

		Document doc = nodes.find(fields).first();
		return doc != null;
	}

	/**
	 *
	 * @param cat
	 */
	@Override
	public void addToArchive(Node cat) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 * @param contaienr
	 * @param id
	 */
	@Override
	public void addToArchive(String contaienr, int id) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 */
	@Override
	public void close() {
	}

	@Override
	public boolean isConnected() {
		try {
			mongo.getAddress();
		} catch (Exception e) {
			Log.error("Mongo is down");
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param cat
	 * @param container
	 * @param u
	 */
	@Override
	public void copy(Node cat, Node container, User u) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 * @param n
	 * @param u
	 */
	@Override
	public void decrementOrderID(Node n, User u) {
		Document doc = new Document();
		doc.put("order_id", n.getOrderID() - 1);

	}

	/**
	 *
	 * @param childID
	 * @param u
	 * @return
	 */
	@Override
	public int delete(ObjectID childID, User u) {
		Node n = getNode(childID);
		int count = deleteNode(n);
		n.onDelete(u);
		return count;
	}

	private int deleteNode(Node n) {
		if (n == null) {
			return 0;
		}
		int deleted = 1;
		NodeIterator<Node> it = n.getChildren().build();

		while (it.hasNext()) {
			Node ccat = it.next();
			deleted += deleteNode(ccat);
		}
		nodes.deleteOne(eq("_id", n.getID().get()));
		return deleted;
	}

	/**
	 *
	 * @param path
	 * @param id
	 * @param u
	 * @return
	 */
	@Override
	public boolean deletePicture(String path, ObjectID id, User u) {
		Node image = getNode(id);
		Node gallery = image.getParent();
		String fileName = getNode(id).getName();
		File f = new File(path + "/media/" + gallery.getID() + "/" + fileName);
		if (!f.exists()) {
			return delete(id, u) > 0;
		}
		if (f.isFile()) {
			if (f.delete()) {
				return delete(id, u) > 0;
			}
		}
		return false;
	}

	/**
	 *
	 * @param contaienr
	 * @param id
	 * @return
	 */
	@Override
	public Iterator<Node> getArchive(String contaienr, int id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public NodeIterator<Node> find() {
		return new MongoNodeIterator(this);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@Override
	public Node getNode(ObjectID id) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("_id", id.get());

		Document doc = nodes.find(fields).first();
		if (doc == null) {
			return null;
		}
		return createNode(this, doc);
	}

	/**
	 *
	 * @param parent
	 * @param type
	 * @return
	 */
	@Override
	public long getChildrenCount(ObjectID parent, Class<?> type) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", parent.get());
		fields.put("type", type.getName());

		return nodes.count(fields);
	}

	/**
	 *
	 * @param parent
	 * @param name
	 * @return
	 */
	@Override
	public Node getNode(ObjectID parent, String name) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", parent.get());

		fields.put("name", name);
		Document doc = nodes.find(fields).first();
		if (doc == null) {
			return null;
		}
		return createNode(this, doc);
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	@Override
	public Node getNode(String name) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", rootID.get());

		fields.put("name", name);
		Document doc = nodes.find(fields).first();
		if (doc == null) {
			return null;
		}
		return createNode(this, doc);
	}

	/**
	 *
	 * @param c
	 * @param doc
	 * @return
	 * @throws RuntimeException
	 */
	public static Node createNode(Controller c, Document doc) throws RuntimeException {
		try {
			Class<?> clazz = Class.forName(doc.getString("type"));
			Node n = (Node) clazz.getConstructor(Controller.class, Document.class).newInstance(c, doc);
			return n;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new RuntimeException("Could not instatiate Class '" + doc.getString("type") + "' " + e.getMessage());
		}
	}

	/**
	 *
	 * @param parentID
	 * @param name
	 * @param type
	 * @return
	 */
	@Override
	public Node getNode(ObjectID parentID, String name, Class<?> type) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", parentID.get());

		fields.put("name", name);

		Document doc = nodes.find(fields).first();
		if (doc == null) {
			return null;
		}
		Node n = createNode(this, doc);
		if (type.isInstance(n)) {
			return n;
		}
		return null;
	}

	/**
	 *
	 * @param parent
	 * @param name
	 * @param lang
	 * @return
	 */
	@Override
	public Node getNodeByUrl(Node parent, String name, Locale lang) {
		Document doc = nodes
				.find(
						and(
								eq("parent",
										parent.getID().get()),
								eq("URL." + lang.getLanguage(), name
								)
						)
				)
				.first();
		if (doc == null) {
			return parent.getChild(name);
		}
		return createNode(this, doc);
	}

	/**
	 *
	 * @param parent
	 * @param type
	 * @param name
	 * @return
	 */
	@Override
	public Node getNode(ObjectID parent, Class<?> type, String name) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", parent.get());

		fields.put("name", name);

		Document doc = nodes.find(fields).first();
		if (doc == null) {
			return null;
		}
		Node n = createNode(this, doc);
		if (type.isInstance(n)) {
			return n;
		}
		return null;
	}

	/**
	 *
	 * @param parent
	 * @param id
	 * @return
	 */
	@Override
	public Node getNode(ObjectID parent, ObjectID id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @param n
	 * @param confs
	 * @return
	 * @throws JSONException
	 */
	@Override
	public NodeIterator<Node> getNodeByConfiguration(Node n, JSONObject confs) throws JSONException {
		MongoNodeIterator it = new MongoNodeIterator(this);
		it.setPath(n.getPath());
		Iterator<?> keys = confs.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			String value = (String) confs.get(key);
			it.addAttributeToFilter(key, value);
		}
		return it.build();
	}

	/**
	 *
	 * @param id
	 * @param type
	 * @param attrname
	 * @param lang
	 * @return
	 */
	@Override
	public Node getNodeLang(ObjectID id, Class<?> type, String attrname, Locale lang) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Node getRoot() {
		return getNode(rootID);
	}

	/**
	 *
	 * @return
	 */
	@Override
	protected Node createRoot() {
		Document doc = new Document();
		doc.put("_id", "ROOT");
		doc.put("name", "ROOT");

		doc.put("type", "core.Domain");
		doc.put("updated", System.currentTimeMillis());
		doc.put("created", System.currentTimeMillis());
		doc.put("last_cid", 0);

		List<String> groups = new ArrayList<>();
		groups.add("users");
		doc.put("group", groups);
		doc.put("rights", 0x777);

		List<Integer> involved = new ArrayList<>();
		involved.add(0);

		doc.put("involved", involved);
		try {
			nodes.insertOne(doc);
			Log.logGreen("root NODE created");
		} catch (Exception e) {
			Log.error(e);
		}
		return createNode(this, doc);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public ObjectID getRootID() {
		return rootID;
	}

	/**
	 *
	 * @param n
	 * @param u
	 */
	@Override
	public void incrementOrderID(Node n, User u) {
		Document doc = new Document();
		doc.put("order_id", n.getOrderID() + 1);

	}

	/**
	 *
	 * @param u
	 * @param id
	 * @param parent
	 * @return
	 */
	@Override
	public Node moveNode(User u, ObjectID id, ObjectID parent) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @param u
	 * @param child_copy
	 * @param parentnode
	 * @return
	 */
	@Override
	public Node copyNode(User u, Node child_copy, Node parentnode) {
		return copyNode(u, child_copy, parentnode.getID());
	}

	/**
	 *
	 * @param u
	 * @param n
	 * @param parent
	 * @return
	 */
	@Override
	public Node copyNode(User u, Node n, ObjectID parent) {
		Map<String, Object> doc = n.doc;
		doc.remove("_id");

		doc.put("parent", parent);

		doc.remove("cid");
		doc.put("cid", getNextCID(n));

		//TODO: get the new id
		Document d = new Document();
		d.putAll(doc);
		nodes.insertOne(d);
		Node newID = null;

		NodeIterator<Node> it = n.getChildren().build();
		while (it.hasNext()) {
			Node c = it.next();
			copyNode(u, c, newID);
		}
		return createNode(this, d);
	}

	/**
	 *
	 * @param n
	 * @param nodes
	 * @return
	 */
	public static synchronized int huhu(Node n, MongoCollection<Document> nodes) {
		n = n.getParent();
		if (n == null) {
			return 0;
		}
		Object last_cid = n.doc.get("last_cid");
		if (last_cid == null) {
			BasicDBObject fields = new BasicDBObject();

			fields.put("parent", n.getID().get());
			fields.put("cid", new Document("$exists", true));

			int cid = 1;
			FindIterable<Document> it = nodes.find(fields).sort(new Document("cid", -1));
			Document ciddoc = it.first();
			if (ciddoc != null) {
				cid = ciddoc.getInteger("cid");
				cid++;
			}
			return cid;
		}
		Document update = new Document();
		update.put("$inc", new Document("last_cid", 1));
		Document r = nodes.findOneAndUpdate(and(eq("_id", n.getID().get())), update);
		int huhu = ((Number) r.get("last_cid")).intValue();
		return huhu + 1;
	}

	/**
	 *
	 * @param n
	 * @return
	 */
	@Override
	public int getNextCID(Node n) {
		return huhu(n, nodes);
	}

	/**
	 *
	 * @param cat
	 * @param container
	 * @param u
	 */
	@Override
	public void moveNode(Node cat, Node container, User u) {
		cat.setParent(container);
		BasicDBObject fields = new BasicDBObject();

		fields.put("parent", cat.getParentID().get());
		fields.put("cid", new Document("$exists", true));

		int cid = 1;
		FindIterable<Document> it = nodes.find(fields).sort(new Document("cid", -1));
		Document ciddoc = it.first();
		if (ciddoc != null) {
			cid = ciddoc.getInteger("cid");
			cid++;
		}
		cat.doc.put("cid", cid);
		cat.setParent(container).commit(u);
		cat.onMove(u);
		NodeIterator<Node> itc = cat.getChildren().build();
		while (itc.hasNext()) {
			itc.next().onMove(u);
		}
	}

	/**
	 *
	 * @param id
	 */
	@Override
	public void refreshUpdated(ObjectID id) {
		Document doc = new Document();
		doc.put("updated", System.currentTimeMillis());

		nodes.updateOne(and(eq("_id", id.get())), new Document("$set", doc));
	}

	/**
	 *
	 * @param u
	 */
	@Override
	public void setup(User u) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 * @param u
	 * @param n
	 * @return
	 */
	@Override
	public Node updateNode(User u, Node n) {
		if (!n.canUpdate(u)) {
			throw new RuntimeException("cant update");
		}
		Map<String, Object> doc = n.doc;
		doc.put("parent", n.getParentID().get());
		doc.put("type", n.getClass().getName());
		doc.put("path", n.getPath());
		doc.put("updated", System.currentTimeMillis());
		doc.put("order_id", n.getOrderID());
		doc.put("flags", n.getFlags());

		doc.remove("involved");

		Document d = new Document();
		d.putAll(doc);
		Document updatedoc = new Document("$set", d);
		if (u != null) {
			updatedoc.append("$addToSet", new Document("involved", u.getID().get()));
		}
		nodes.updateOne(eq("_id", n.getID().get()), updatedoc);

		n.onUpdate(u);

		return n;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean isSuperRoot() {
		return rootID == null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Locale getLang() {
		return lang;
	}

	/**
	 *
	 * @param n
	 * @param u
	 * @return
	 */
	@Override
	public int delete(Node n, User u) {
		return deleteNode(n);
	}

	/**
	 *
	 * @param parent
	 * @return
	 */
	@Override
	public long getChildrenCount(ObjectID parent) {
		BasicDBObject fields = new BasicDBObject();
		fields.put("parent", parent.get());

		return nodes.count(fields);
	}

	/**
	 *
	 * @param node
	 * @param name
	 * @return
	 */
	@Override
	public NodeIterator getAllChildren(Node node, String name) {
		MongoNodeIterator it = new MongoNodeIterator(this);
		it.setPath(node.getPath());
		it.addAttributeToFilter("name", name);
		return it;
	}

	/**
	 *
	 * @param a
	 */
	@Override
	public void deleteAttr(Attribute a) {

	}

	/**
	 *
	 * @param n
	 */
	@Override
	public void accessNode(Node n) {
		Object requests = n.doc.get("requests");
		if (requests == null) {
			requests = 1;
		}
		Document updatedoc = new Document("$set", new Document("requests", (Integer) requests + 1));
		nodes.updateOne(eq("_id", n.getID().get()), updatedoc);
	}

	boolean importusers = false;

	/**
	 *
	 * @return
	 */
	@Override
	public Node init() {
		IndexOptions indexOptions = new IndexOptions().unique(true);

		nodes.createIndex(Indexes.ascending("parent", "name"), indexOptions);
		//nodes.createIndex(Indexes.ascending("ID"), indexOptions);

		Node root = super.init();
		if (!importusers) {
			return root;
		}
		MongoCollection<Document> cu = db.getCollection("chatusers");
		FindIterable<Document> cusers = cu.find().sort(new Document("id", 1));
		Node users = getNode("USERS");
		for (Document cuser : cusers) {
			User u = new User(null, this);
			u.setName(cuser.get("id") + "");
			// users.removeChild(cuser.get("id") + "", u);
			u = (User) users.addChild(u, u);
			u.setOwner(u);
			java.util.Set<String> keys = cuser.keySet();
			for (String key : keys) {

				switch (key) {
					case "_id":
					case "id":
					case "online":
					case "msgratio":
					case "age":
					case "not_a_bot":

						break;
					case "created":
						Object v;
						try {
							v = cuser.get(key);
							if (v instanceof Long) {
								u.doc.put("created", v);
							} else {
								u.doc.put("created", (new Double(cuser.getDouble(key)).longValue()));
							}
						} catch (Exception e) {
							Log.error(e);
						}
						break;
					case "updated":

						try {
							v = cuser.get(key);
							if (v instanceof Long) {
								u.doc.put("updated", v);
							} else {
								u.doc.put("updated", (new Double(cuser.getDouble(key)).longValue()));
							}
						} catch (Exception e) {
							Log.error(e);
						}
						break;

					case "name":
						u.doc.put("nick", cuser.get(key));

						break;
					case "type":
						break;
					case "pw":
						u.setPassword(cuser.getString(key), u);
						break;
					case "moderator":
						u.addToGroup("moderator", u);
						break;
					case "admin":
						u.addToGroup("admin", u);

						break;

					case "color":
					case "messages":
					case "rockets":
					case "hiddenmsgs":
					case "online_ts":
					case "guser":
					case "ban":
					case "gid":
					case "x":
					case "y":
					case "baned":

						u.doc.put(key, cuser.get(key));

						break;
					default:
						getClass();
						break;
				}
			}
			u.save(u);
			System.out.println(u);
		}
		return root;

	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	@Override
	public Node addToSet(Node n, String name, String value, User u) {
		if (!n.canUpdate(u)) {
			throw new RuntimeException("cant update");
		}
		//Map<String, Object> doc = n.doc;

		int uid = 0;

		Document updatedoc = new Document().append("$addToSet", new Document(name, value));
		nodes.updateOne(eq("_id", n.getID().get()), updatedoc);

		n.onUpdate(u);

		return n;
	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	@Override
	public Node removeFromSet(Node n, String name, String value, User u) {
		if (!n.canUpdate(u)) {
			throw new RuntimeException("cant update");
		}
		Map<String, Object> doc = n.doc;

		Document d = new Document();
		d.putAll(doc);
		Document updatedoc = new Document("$set", d).append("$pull", new Document(name, value));
		nodes.updateOne(eq("ID", n.getID()), updatedoc);

		n.onUpdate(u);

		return n;
	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	@Override
	public Node addToList(Node n, String name, String value, User u) {
		if (!n.canUpdate(u)) {
			throw new RuntimeException("cant update");
		}
		Map<String, Object> doc = n.doc;
		Document d = new Document();
		d.putAll(doc);

		Document updatedoc = new Document("$set", d).append("$push", new Document(name, value));
		nodes.updateOne(eq("ID", n.getID()), updatedoc);

		n.onUpdate(u);

		return n;
	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	@Override
	public Node removeAllFromList(Node n, String name, String value, User u) {
		if (!n.canUpdate(u)) {
			throw new RuntimeException("cant update");
		}
		Map<String, Object> doc = n.doc;

		Document d = new Document();
		d.putAll(doc);
		Document updatedoc = new Document("$set", d).append("$pull", new Document(name, value));
		nodes.updateOne(eq("ID", n.getID()), updatedoc);

		n.onUpdate(u);

		return n;
	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param u
	 */
	@Override
	public void deleteAttr(Node n, String name, User u) {
		Document updatedoc = new Document("$unset", new Document(name, ""));
		updatedoc.append("$addToSet", new Document("involved", u.getID().get()));
		nodes.updateOne(eq("_id", n.getID().get()), updatedoc);
	}

	/**
	 *
	 * @param n
	 * @param name
	 * @param u
	 */
	@Override
	public void addAttr(Node n, String name, User u) {
		Document updatedoc = new Document("$set", new Document(name, ""));
		if (u != null) {
			updatedoc.append("$addToSet", new Document("involved", u.getID().get()));
			nodes.updateOne(eq("_id", n.getID().get()), updatedoc);
		}
	}

	/**
	 *
	 * @param n
	 * @param attr
	 * @param key
	 * @param value
	 * @param locale
	 */
	@Override
	public void addToMap(Node n, Attribute attr, String key, String value, Locale locale) {
		Object doc;
		if (attr.isMultilang()) {
			doc = new Document(locale.getLanguage(), value);
		} else {
			doc = value;
		}
		doc = new Document(key, doc);

		Document updatedoc = new Document("$set", doc).append("$push", new Document(attr.getName(), doc));
		nodes.updateOne(eq("ID", n.getID()), updatedoc);
	}

	@Override
	public LinkedHashMap<String, Attribute> getAttributes(ObjectID parent, String attr_type) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public LinkedHashMap<String, Attribute> getAttributes(int id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Object getConnection() {
		return mongo;
	}

	@Override
	public void deleteSession(String name) {
		nodes.deleteMany(and(eq("name", name), eq("type", "core.Session")));

	}

	@Override
	public void changeID(ObjectID oldid, ObjectID newid, User u) {
		Node n = getNode(oldid);
		nodes.deleteOne(eq("_id", oldid.get()));
		n.doc.put("_id", newid.get());
		Document d = new Document();
		d.putAll(n.doc);

		nodes.insertOne(d);
		Document updatedoc = new Document("$set", new Document("parent", newid.get()));
		nodes.updateMany(eq("parent", oldid.get()), updatedoc);
	}

	@Override
	public NodeIterator find(String query) {
		return new MongoNodeIterator(this, query);
	}

	/**
	 *
	 * @param name
	 * @param password
	 * @return
	 */
	@Override
	public User addUser(String name, String password) {
		name = name.toLowerCase(Locale.ENGLISH);
		User u = new User(null, this);
		u.setName(name);
		Node users = getNode("USERS");
		// u.setParentContainer("CATEGORIES");
		u = (User) users.addChild(u, null);
		u.setOwner(u).save(u);
		// HashMap<String, Attribute> attrs = u.getAttributesWithTypes(true);

		//u.setAttribute("email", email, null, u.getID());
		u.setPassword(password, u);

		u.addToGroup("USER", u);
		Node n = new Addresses(u.getID(), this);
		n.setName("ADDRESSES");
		u.addChild(n, u);

		Filemanager fm = new Filemanager(u.getID(), this);
		fm.setName("FILES");
		u.addChild(fm, u);

		return u;
	}

	public User userLogin(String name, String password) {
		name = name.toLowerCase(Locale.ENGLISH);

		Node users = getNode("USERS");

		User user = (User) users.getChild(name);

		if (user == null) {
			return null;
		}
		if (!user.emailConfirmed()) {
			Log.log(user, "email not confirmed");
			throw new RuntimeException("email not confirmed");
		}
		String dbMail;

		dbMail = user.getName();

		String dbPassword = (String) user.doc.get("password");
		if (dbPassword == null) {
			throw new RuntimeException("wrong dbPassword");
		}

		if (dbMail != null) {
			if (dbMail.equals(name)) {
				try {
					if (PasswordSecurity.validatePassword(password, dbPassword)) {
						Log.log(user + " logged in");
						return user;
					}
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
					throw new RuntimeException("error with validatePassword");
				}
			}
		}
		return null;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	@Override
	public User getUser(String name) {
		return (User) getNode("USERS").getChild(name);
	}
}
