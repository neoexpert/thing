package core;

import static core.Node.addChildType;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 *
 */
public class Company extends Node {

	

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Company(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Company(ObjectID userID, Controller con) {
		super(userID, con);
	}
}
