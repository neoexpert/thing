package web;

import core.Node;
import core.User;
import core.processor.*;
import core.controller.Controller;
import core.controller.NodeIterator;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class HttpParams extends ProcessorInterface {

    private final HashMap<String, String[]> params;
    private final HttpServletResponse response;
    private final PrintWriter out;
    private final HttpServletRequest request;

    /**
     *
     * @param request
     * @param response
     * @throws IOException
     */
    public HttpParams(HttpServletRequest request, HttpServletResponse response) throws IOException {
        this.response = response;
        this.request = request;
        out = response.getWriter();
        cmd = request.getParameter("cmd");
        params = new HashMap<>(request.getParameterMap());
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public String getParameter(String key) {
        String[] v = params.get(key);
        if (v == null) {
            return null;
        }
        if (v.length > 0) {
            return v[0];
        }
        return null;
    }

    /**
     *
     * @param result
     */
    @Override
    public void writeResult(String result) {
        out.write(result);
    }

    @Override
    public void writeResult(JSONObject result) {
        out.write(result.toString());
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public String[] getParameterValues(String key) {
        return params.get(key);
    }
    @Override
    public JSONObject getJSONObject(String key) {
        return new JSONObject(params.get(key));
    }

    /**
     *
     * @return
     */
    @Override
    public Set<String> getKeys() {
        return params.keySet();
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public String remove(String key) {
        String[] v = params.remove(key);
        if (v == null) {
            return null;
        }
        if (v.length > 0) {
            return v[0];
        }
        return null;

    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public JSONObject get(String key) {
        String[] v = params.get(key);
        if (v == null) {
            return null;
        }
        if (v.length > 0) {
            return new JSONObject(v[0]);
        }
        return null;
    }

    /**
     *
     * @param key
     * @param jo
     */
    @Override
    public void put(String key, JSONObject jo) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @param it
     */
    @Override
    public void setIterator(NodeIterator it) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @return
     */
    @Override
    public JSONObject toJSON() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @param e
     */
    @Override
    public void error(Exception e) {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        JSONObject je = new JSONObject().accumulate("e", e.toString());
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        je.accumulate("stacktrace", errors.toString());
        writeResult(je.toString());
    }

    /**
     *
     * @param cmd
     */
    @Override
    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCmd() {
        return cmd;
    }

    /**
     *
     * @return
     */
    @Override
    public String getURL() {
        return request.getScheme() + "://"
                + // "http" + "://
                request.getServerName()
                + // "myhost"
                ":"
                + // ":"
                request.getServerPort();
    }

    /**
     *
     * @param name
     * @param c
     * @param user
     * @throws Exception
     */
    @Override
    public void renderSite(String name, Controller c, User user) throws Exception {
        String wrapper = request.getParameter("wrapper");
        Node n = c.getNode("CATEGORIES").getChild(name);
        if (n == null) {
            throw new Exception("no such Site in CATEGORIES: " + name);
        }
        request.setAttribute("node", n);
        request.setAttribute("controller", c);
        request.setAttribute("user", user);

        RequestDispatcher rd;
        if (wrapper == null) {
            rd = request.getRequestDispatcher("/WEB-INF/ControlBar.jsp");
        } else {
            rd = request.getRequestDispatcher("/WEB-INF/WRAPPER.jsp");
        }

        rd.forward(request, response);

    }

}
