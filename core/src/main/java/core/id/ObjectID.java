/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.id;

import java.security.SecureRandom;
import java.util.Objects;

/**
 *
 * @author neoexpert
 */
public class ObjectID {
	private static SecureRandom sr=new SecureRandom();
	private static String chars ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	private static int length=8;
	protected Object id;

	public ObjectID(Object id) {
		this.id = id;
	}
	
	
	public Object get() {
		return id;
	}

	@Override
	public String toString() {
		return id.toString();
	}
	public final static ObjectID getDefault(Object id){
		return new MongoObjectID((String)id);
	}
	
	public static String getRandomID(){
		StringBuilder newid=new StringBuilder();
		for(int i=0;i<length;i++)
			newid.append(chars.charAt(sr.nextInt(chars.length())));
		return newid.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ObjectID)
			return ((ObjectID) obj).id.equals(id);
		return false;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	
}
