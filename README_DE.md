# thing

ist ein Tool zur verwaltung von Dingen in thing auch "Knoten (englisch Nodes)" genannt.

### Voraussetzungen

Damit thing läuft benötigst Du folgende zusätzliche Software:

* docker-compose

### Installation

```
git clone https://github.com/neo-expert/thing/
cd thing
./install.sh
```

## Datenbank Organisation

Die Datenbank ist wie ein Dateisystem organisiert. Mit dem Unterschied, dass in thing die "Dateien" beliebig viele Attribute haben und Nodes heißen. So kann man einem Node z.B. einen Attribut namens "Beschreibung" hinzuzufügen. Dieser Attribut kann auch mehrsprachig sein. 
Nodes haben ein Paar systemreservierte Attribute diese sind:

* _id - die eindeutige ID eines Nodes
* name - eindeutiger Name innerhalb eines parent-Nodes
* parent - _id des übergeordneten Nodes
* type - Typ des Nodes. es gibt unter anderem Folgende Typen:
	* core.Node
	* core.User
	* core.Category
	* core.Site
	* templates.HtmlTemplate
	* core.File
	* core.Picture (interessant wegen src und alt tag)
* cid - eindeutige ID innerhalb eines parent-Nodes (wird fortlaufend inkrementriert)
* last-cid speichert die letzte cid eines hinzugefügten nodes
* created Zeitstempel 
* updated Zeitstempel 
* owner _id des Erstellers des Nodes
* involved liste von _id's aller involvierter Benutzer die den Node bearbeitet haben

Einem Node kann man beliebgig viele Nodes unterordnen - damit ist die Hierarchie so ähnlich wie in einem Dateisystem möglich. 

Man kann die Nodes auch nach Attributen filtern:
Z.B.
"Zeige mir alle Nodes deren Attribut preis grosser als 5€ ist"
(Zumindest technisch funktioniert das, im Backend müsste man diesen filter nochmal einbauen)

Das geht auch mit sogenannten Indexes.
Man könnte theoretisch so viele Nodes haben wie Atome im Universum, und die obere Abfrage würde nahezu genau so performant ablaufen als wenn man nur 10 Nodes hätte.
Das liegt am Logarithmus
```
ld(8)=3 
//8 nodes =>Max 3 Operationen


ld(2^100)=100
//2^100 nodes (wohl etwas mehr als Atome im sichtbaren Universum) => max 100 Operationen
```
Sucht man dagegen nach einem Dateinamen in einem Dateisystem kann es länger dauern wenn diese nicht indiziert sind

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing


## Authors

## License



