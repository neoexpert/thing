package servlets.admin;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.attribute.Attribute;
import core.controller.ControllerBuilder;
import web.*;
import core.controller.NodeIterator;
import core.id.ObjectID;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 * Servlet implementation class Export
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/admin/export.csv" })
public class Export extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Export() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		response.setCharacterEncoding("UTF-8");

		request.setAttribute("controller", c);

		HttpSession session = request.getSession();
		User u = c.getUser(session);
		String wrapper = request.getParameter("wrapper");
		Locale lang = HttpController.getLang(request);

		if (!u.isLoggedIn("admin")) {
			// RequestDispatcher rd =
			// request.getRequestDispatcher("/WEB-INF/login.jsp");

			// rd.forward(request, response);

			response.sendRedirect("/" + lang.getLanguage() + "/login");

			// Index i = new Index();
			// i.doGet(request, response);
			if (wrapper == null) {
				String uri = WebControllerBuilder.getFullURL(request);

				if (request.getRequestURI().equals("/admin"))
					try {
						uri = c.getRoot().getBackURL();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						Log.error(e);
					}

				session.setAttribute("redirectTO", uri);
			}
			return;
		}
		response.setContentType("text/csv");
		ObjectID id = new ObjectID(request.getParameter("ID"));

		Node n = c.getNode(id);
		PrintWriter out = response.getWriter();
		Map<String, String[]> params = new HashMap<>(request.getParameterMap());
		params.remove("tab");

		LinkedHashMap<String, Attribute> nattrs = n.getAttributes(true);
		Set<String> attrs;
		if (params.containsKey("all")) {
			attrs = new HashSet<>(nattrs.keySet());
			attrs.add("ID");
			attrs.add("type");

		} else
			attrs = params.keySet();

		try {
			for (String attr : attrs) {
				out.print("\"");
				out.print(attr);
				out.print("\"");
				out.print(",");
			}
			out.println();
			exportCSV(n, out, attrs, lang);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}

	}

	private void exportCSV(Node n, PrintWriter out, Set<String> attrs, Locale lang) throws JSONException {
		
		String v;
		for (String attr : attrs) {
			switch (attr) {
			case "ID":
				v = n.getID() + "";
				break;
			case "name":
				v = n.getName();
				break;
			case "parent":
				v = n.getParentID() + "";
				break;
			case "type":
				v = n.getType();
				break;
			default:
				Attribute a = n.getAttribute(attr);
				v = a.getValue(lang).toString();
				break;
			}
			out.print("\"");
			out.print(v.replaceAll("\"", "\"\""));
			out.print("\"");
			out.print(",");

		}
		out.println();

		NodeIterator<Node> it = n.getChildren().build();

		while (it.hasNext()) {
			exportCSV(it.next(), out, attrs, lang);
		}
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
