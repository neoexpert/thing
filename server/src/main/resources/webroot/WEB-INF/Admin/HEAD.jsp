<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="core.*"%>
<%@ page import="java.util.*"%>
<%@ page import="core.controller.*"%>
<%@ page trimDirectiveWhitespaces="true"%>
<link rel="icon" type="image/png" href="/favicon.png" />

<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="theme-color" content="#ffffff" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
	type="text/javascript"></script>
	
<script src="/js/admin/main.js"></script>
<script src="/js/admin/events.js"></script>
<script src="/js/admin/diff.js"></script>
<script src="/js/admin/menu.js"></script>

<link rel="stylesheet" type="text/css" href="/css/admin/main.css">
