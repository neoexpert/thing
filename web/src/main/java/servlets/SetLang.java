package servlets;

import com.neoexpert.Log;
import core.Category;
import core.Node;
import core.controller.ControllerBuilder;
import web.*;
import core.id.ObjectID;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CategoryView
 */
@WebServlet("/setlang")
public class SetLang extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SetLang() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String lang = request.getParameter("lang").toLowerCase(Locale.ENGLISH);
		String params = request.getParameter("params");
		Locale locale = new Locale(lang);
		if (lang.length() == 2) {
			HttpController.setLang(request, locale);
		}

		String ID = request.getParameter("ID");
		if (ID != null) {
			ObjectID id = new ObjectID(ID);

			try {

				Node cat = c.getNode(id);
				String path;
				if (!"setpassword".equals(cat.getName())) {

					if (cat.getName().equals("home")) {
						response.getWriter().append("/");
						return;
					}

					 path = cat.getFrontPath(locale);

					if (cat instanceof Category) {
						path += "/";
					}
				}
				else
					path="forgot";
				if (params != null) {
					path += params;
				}
				response.getWriter().append(path);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
		}

	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);

	}

}
