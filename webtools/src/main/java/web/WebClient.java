package web;
import java.io.IOException;
import java.util.Locale;
import core.Client;
import core.User;
import core.controller.Controller;
import javax.websocket.Session;
public class WebClient extends Client{
    private final Session session;
    public WebClient(Controller c, User u, Session session, Locale lang) {
	 super(c,u,lang);
        this.session = session;
    }
    public String getSessionID(){
    	return session.getId();
    }
    public Session getSession() {
        return session;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Client) {
            return session.getId().equals(((Client) obj).getSessionID());
        }
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return session.hashCode();
    }

    @Override
    public void send(String text) throws IOException {
        session.getBasicRemote().sendText(text);
    }

    @Override
    public void close() throws IOException {
	super.close();
        session.close();
    }
}
