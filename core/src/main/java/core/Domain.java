package core;

import core.controller.Controller;
import org.bson.Document;
import types.Types;

/**
 *
 * @author neoexpert
 */
public class Domain extends Node {
	static{


		addChildType(Domain.class,Users.class);
		addChildType(Domain.class,Category.class);
		addChildType(Domain.class,Filemanager.class);
		addChildType(Domain.class,Node.class);
		addChildType(Domain.class,Types.class);
		addChildType(Domain.class,Domains.class);

	}

    /**
     *
     * @param c
	 * @param doc
     */
    public Domain(Controller c,Document doc) {
		super(c,doc);
	}
}
