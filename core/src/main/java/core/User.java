package core;

import core.controller.Controller;
import core.events.Event;
import core.id.ObjectID;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

/**
 * Ein Benutzer ist ein Node des Nodes "USERS".
 *
 * @author neoexpert
 *
 */
public class User extends Node {

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public User(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public User(ObjectID userID, Controller c) {
		super(userID, c);
	}

	/**
	 *
	 * @param group
	 * @return
	 */
	public boolean isInGroup(String group) {
		Object groups = doc.get("groups");
		if (groups == null) {
			return false;
		}
		return ((List) groups).contains(group);
	}

	/**
	 *
	 * @param as
	 * @return
	 */
	public boolean isLoggedIn(String as) {
		return isInGroup(as);
	}

	/**
	 *
	 * @return
	 */
	public String getEmail() {
		return getName();
	}

	/**
	 *
	 * @param subject
	 * @param text
	 * @param mailsettings
	 * @throws Exception
	 */
	public void sendMail(String subject, String text, Node mailsettings) throws Exception {
		SendEmail sm = new SendEmail(mailsettings);
		sm.sendEmail(getEmail(), subject, text);
	}

	/**
	 *
	 * @param newPassword
	 * @param u
	 */
	public void setPassword(String newPassword, User u) {

		try {
			newPassword = PasswordSecurity.generateStorngPasswordHash(newPassword);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException("error with generateStorngPasswordHash");
		}
		setAttribute("password", newPassword).commit(u);
	}

	/**
	 *
	 * @param group
	 * @param u
	 */
	public void addToGroup(String group, User u) {
		addToSet("groups", group, u);
	}

	static {
		addChildType(User.class, User.class);
		addChildType(User.class, Addresses.class);
	}

	/**
	 *
	 * @return
	 */
	public boolean isAdmin() {
		List groups = (List) doc.get("groups");
		if (groups == null) {
			return false;
		}
		return (groups).contains("admin");
	}

	/**
	 *
	 * @param groups
	 * @return
	 */
	public boolean isInGroup(List groups) {
		List<String> mygroups = (List) doc.get("groups");
		if (mygroups == null || groups == null) {
			return false;
		}
		for (String group : mygroups) {
			if (groups.contains(group)) {
				return true;
			}
		}
		return false;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		return false;
	}

	/**
	 *
	 * @return
	 */
	public List<String> getGroups() {
		return (List<String>) doc.get("groups");
	}

	public void onEvent(Event event) {
	}

	public boolean emailConfirmed() {
		if (doc.containsKey("email_confirmed")) {
			return (boolean) doc.get("email_confirmed");
		}
		return false;
	}

	public void sendMail(String subject, String mail, Node mailSettings, ArrayList<String> filesb64) throws Exception {
		SendEmail sm = new SendEmail(mailSettings);
		sm.sendEmail(getEmail(), subject, mail,filesb64);
	}

}
