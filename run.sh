if ! command -v git >/dev/null; then
	echo "git is not installed"
	exit 1
fi
echo "git pull ..."
if ! command -v mvn >/dev/null; then
	echo "maven is not installed"
	exit 2
fi
git pull &&
mvn install &&
java -jar server/target/server-1-SNAPSHOT-jar-with-dependencies.jar
