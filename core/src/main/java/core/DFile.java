package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class DFile {

    /**
     *
     */
    public static class Line implements Iterable<Character> {

        private List<Character> chars;
        private Object lock;

        /**
         *
         * @param c
         * @return
         */
        public boolean contains(Character c) {
            return chars.contains(c);
        }

        /**
         *
         */
        public Line() {
            chars = new ArrayList<>();
        }

        /**
         *
         * @param chars
         */
        public Line(ArrayList<Character> chars) {
            this.chars = chars;
        }

        /**
         *
         * @param s
         */
        public Line(String s) {
            chars = new ArrayList<>();
            char[] chararray = s.toCharArray();
            for (Character c : chararray) {
                chars.add(c);
            }
        }

        /**
         *
         * @param s
         * @param readonly
         */
        public Line(String s, boolean readonly) {
            this(s);
            setReadonly(readonly);
        }

        /**
         *
         * @param o
         * @return
         */
        public boolean lock(Object o) {
            if (isReadonly()) {
                return false;
            }
            if (lock != null) {
                return lock.equals(o);
            }
            this.lock = o;
            return isLocked();
        }

        /**
         *
         * @param o
         * @return
         */
        public boolean unlock(Object o) {
            if (o.equals(lock)) {
                this.lock = null;
            }
            return lock == null;
        }

        /**
         *
         * @return
         */
        public boolean isLocked() {
            return lock != null;
        }

        private boolean readonly = false;

        /**
         *
         * @return
         */
        public boolean isReadonly() {
            return readonly;
        }

        /**
         *
         * @param readonly
         */
        public final void setReadonly(boolean readonly) {
            this.readonly = readonly;
        }

        /**
         *
         * @return
         */
        @Override
        public Iterator<Character> iterator() {
            return chars.iterator();
        }

        /**
         *
         * @param pos
         * @return
         */
        public Line breakMe(int pos) {
            if (readonly) {
                return null;
            }
            Line newline = new Line(new ArrayList<>(chars.subList(pos, chars.size())));
            chars.subList(pos, chars.size()).clear();
            return newline;
        }

        /**
         *
         * @param pos
         * @param ch
         * @return
         */
        public boolean addChar(int pos, char ch) {
            if (readonly) {
                return false;
            }
            if (ch == '\n') {
                getClass();
            }
            chars.add(pos, ch);
            return true;
        }

        /**
         *
         * @param ch
         * @return
         */
        public boolean addChar(Character ch) {
            if (readonly) {
                return false;
            }
            if (ch == '\n') {
                getClass();
            }
            chars.add(ch);
            return true;
        }

        /**
         *
         * @param line
         * @return
         */
        public boolean append(Line line) {
            if (readonly) {
                return false;
            }
            for (Character c : line) {
                chars.add(c);
            }
            return false;
        }

        /**
         *
         * @return
         */
        public int length() {
            return chars.size();
        }

        /**
         *
         * @param start
         * @return
         */
        public boolean removeChar(int start) {
            if (readonly) {
                return false;
            }
            chars.remove(start);
            return true;
        }

        /**
         *
         * @param start
         * @param end
         * @return
         */
        public boolean removeChars(int start, int end) {
            if (readonly) {
                return false;
            }
            for (int i = start; i < end; i++) {
                chars.remove(start);
            }
            return true;
        }

        /**
         *
         * @param start
         * @return
         */
        public boolean clearAfter(int start) {
            if (readonly) {
                return false;
            }
            chars.subList(start, length()).clear();
            return true;
        }

        /**
         *
         * @param end
         * @return
         */
        public boolean clearUntil(int end) {
            if (readonly) {
                return false;
            }
            chars.subList(0, end).clear();
            return false;
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(chars.size());
            for (Character c : chars) {
                sb.append(c.charValue());
            }
            return sb.toString();
        }

        /**
         *
         * @return
         */
        public JSONObject toJSONObject() {
            JSONObject line = new JSONObject();
            line.put("v", toString());
            line.put("readonly", readonly);
            line.put("locked", isLocked());

            return line;
        }

    }

    /**
     *
     */
    public DFile() {
        addLine(new Line());
    }

    /**
     *
     * @param s
     */
    public DFile(String s) {
        this();
        char[] ca = s.toCharArray();
        Line line = getLine(0);
        for (Character c : ca) {
            if (c == '\n') {
                line = new Line();
                addLine(line);
                continue;
            }
            line.addChar(c);
        }
        // TODO Auto-generated constructor stub
    }

    private final List<Line> lines = new ArrayList<>();

    /**
     *
     * @param c
     * @return
     */
    public boolean contains(Character c) {
        for (Line line : lines) {
            if (line.contains(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param line
     */
    public final void addLine(Line line) {
        lines.add(line);
    }

    /**
     *
     * @param index
     * @param line
     */
    public void addLine(int index, Line line) {
        lines.add(index, line);
    }

    /**
     *
     * @param pos
     * @return
     */
    public boolean deleteLine(int pos) {
        if (lines.get(pos).isReadonly()) {
            return false;
        }
        lines.remove(pos);
        if (lines.isEmpty()) {
            lines.add(new Line());
        }
        return true;
    }

    /**
     *
     * @param startline
     * @return
     */
    public final Line getLine(int startline) {
        return lines.get(startline);
    }

    /**
     *
     * @param line
     * @param pos
     * @return
     */
    public boolean breakLine(int line, int pos) {
        Line linetobreak = getLine(line);
        if (linetobreak.isReadonly()) {
            return false;
        }
        Line newline = linetobreak.breakMe(pos);
        addLine(line + 1, newline);
        return true;
    }

    /**
     *
     * @param pos
     * @return
     */
    public boolean connectLines(int pos) {
        Line nextline = getLine(pos + 1);
        if (nextline.isReadonly()) {
            return false;
        }
        Line line = getLine(pos);
        line.append(nextline);
        deleteLine(pos + 1);
        return true;
    }

    /**
     *
     * @param startline
     * @param start
     * @param endline
     * @param end
     * @return
     */
    public boolean removeRange(int startline, int start, int endline, int end) {
        Line sline = getLine(startline);
        Line eline = getLine(endline);
        if (sline.isReadonly()) {
            return false;
        }
        if (eline.isReadonly()) {
            return false;
        }

        List<Line> linestodelete = new ArrayList<>();
        for (int i = startline; i < endline; i++) {
            Line line = getLine(i);
            if (line.isReadonly()) {
                return false;
            }
            linestodelete.add(line);
        }

        sline.clearAfter(start);
        eline.clearUntil(end);
        sline.append(eline);
        lines.removeAll(linestodelete);

        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Line line : lines) {
            sb.append(line.toString());
            sb.append('\n');
        }
        return super.toString();
    }

    /**
     *
     * @return
     */
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        for (Line line : lines) {
            jo.accumulate("lines", line.toJSONObject());

        }
        return jo;
    }

    Map<Object, Line> users = new HashMap<>();

    /**
     *
     * @param linepos
     * @param o
     * @return
     */
    public Line lockLine(int linepos, Object o) {
        Line line = getLine(linepos);
        if (line.lock(o)) {
            users.put(o, line);
            return line;
        }
        return null;
    }

    /**
     *
     * @param u
     * @return
     */
    public Line getLineOf(Object u) {
        return users.get(u);
    }

    /**
     *
     * @param line
     * @return
     */
    public int indexOf(Line line) {
        return lines.indexOf(line);
    }

    /**
     *
     * @param u
     * @return
     */
    public Line removeUser(Object u) {
        Line line = users.remove(u);
        line.unlock(u);
        return line;
    }

}
