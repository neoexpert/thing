package core;

import core.controller.Controller;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Domains extends Node {

	static {
		addChildType(Domains.class, Domain.class);

	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Domains(Controller c, Document doc) {
		super(c, doc);
	}
}
