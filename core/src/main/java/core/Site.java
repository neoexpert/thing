package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Site extends Node {

	static {
		addChildType(Category.class, Site.class);

	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Site(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Site(Controller c,Document doc) {
		super(c,doc);
	}

}
