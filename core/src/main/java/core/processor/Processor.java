package core.processor;

import com.mongodb.MongoWriteException;
import com.neoexpert.Log;
import core.*;
import core.attribute.Attribute;
import core.controller.Controller;
import core.controller.Controller.NodeExistsException;
import core.controller.NodeIterator;
import core.id.ObjectID;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import templates.Template;
import types.Map;
import types.Primitive;

/**
 *
 * @author neoexpert
 */
public class Processor {

	/**
	 *
	 * @param pi
	 * @param c
	 * @param u
	 * @param lang
	 */
	public static void process(ProcessorInterface pi, Controller c, User u, Locale lang) {
		String cmd = pi.getCmd();
		if (cmd == null) {
			cmd = pi.getParameter("CMD");
		}
		if (cmd == null) {
			pi.error(new Exception("unknown command"));
			return;
		}

		try {
			if (!processUser(cmd, pi, c, u, lang)) {
				if (u.isAdmin()) {
					processAdmin(cmd, pi, c, u, lang);
				}
			}
		} catch (Exception e) {
			Log.error(e);
			pi.error(e);
		}

	}

	private static boolean processUser(String cmd, ProcessorInterface pi, Controller c, User u, Locale lang)
			throws Exception {
		switch (cmd) {
			case "refresh_template":
				refreshTemplate(pi, c, u, lang);
					return true;
			case "delNode":
				delNode(pi, c, u, lang);
				return true;
			case "getNode":
				getNode(pi, c, u, lang);
				return true;
			case "getChild":
				getChild(pi, c, u, lang);
				return true;
			case "addSession":
				addSession(pi, u, c, pi);
				return true;

			case "find":
				find(pi.toJSON(), u, c, pi);
				return true;
			case "next":
				next(pi.toJSON(), u, c, pi);
				return true;

			case "getUserAttrs":
				sendNodeAttrs(u, pi, lang, u, false, true);

				return true;

			case "setUserAttr":
				setUserAttr(pi, c, u);

				return true;
			case "setAttr":
				setAttr(c, pi, u, lang);

				return true;
			case "getChildrenCount":
				getChildrenCount(pi, c, u);

				return true;
			case "addNode":
				addNode(pi, c, u,lang);

				return true;

			case "addChild":
				String name = pi.getParameter("name");
				String path = pi.getParameter("path");
				if (path == null) {
					return false;
				}
				Node n = c.getNodeByPath(path);

				if (name == null || name.isEmpty()) {
					// response.sendError(response.SC_BAD_REQUEST,
					// "important_parameter needed");
					name = UUID.randomUUID().toString();
				}
				Node child = new Node(u.getID(), c);

				child.setName(name);

				try {

					String type = pi.getParameter("type");
					type = checkType(type);
					Class<?> cl = Class.forName(type);
					child = (Node) cl.getConstructor(Node.class).newInstance(child);
					JSONObject attrs = pi.get("child");
					if (attrs != null) {
						Set<String> keys = attrs.keySet();
						for (String key : keys) {
							if (!child.doc.containsKey(key)) {
								child.doc.put(key, attrs.get(key));
							}
						}
					}

					
					n.addChild(child, u);

				} catch (NodeExistsException e) {
					Log.error(e);
					pi.error(e);
					return false;
				} catch (Exception e) {
					pi.error(e);
					Log.error(e);
					return false;
				}

				return true;
			case "addChildToUser":
				addChildToUser(pi, c, u);

				return true;

			case "deleteChild":
			case "remove":
				 remove(pi, c, u);
				return true;
				 
			case "getChildrenTemplatesFromUser":
				getChildrenTemplatesFromUser(pi, c, u);

				return true;
			case "getRelatedTemplate":
				getRelatedTemplate(pi, c, u);

				return true;
			case "getChildTemplate":
				getChildTemplate(pi, c, u);
				return true;
			case "getChildrenTemplates":
				getChildrenTemplates(pi, c, u);

				return true;

			case "getChildrenTemplatesByUserID":
				getChildrenTemplatesByUserID(pi, c, u);

				return true;

			case "getChildrenMenu":
				getChildrenMenu(pi, c, lang);

				return true;
			case "getChildren":
				getChildren(pi, c, lang, u);

				return true;
			case "setLang":
				setLang(pi);
				return true;
			case "loggedin":
				if (!(u instanceof Zombie)) {
					pi.writeResult("true");
				} else {
					pi.writeResult("false");
				}

				return true;
			case "getAttrs":
				_sendAttrs(pi, c, lang, u);

				return true;
			case "getChildAttrs":
				getChildAttrs(pi, c, lang, u);

				return true;
			case "reset_password":
				resetpassword(pi, c, u);
				return true;
			case "password_forgoten":
				password_forgotten(pi, c, u);
				return true;
		}
		return false;
	}

	private static void password_forgotten(ProcessorInterface pi, Controller c, User u) throws Exception {
		String username = pi.getParameter("username");
		User user = (User) c.getNode("USERS").getChild(username);
		if (user == null) {
			return;
		}
		Attribute attr = user.getMyAttribute("lastpasswordforgotten");
		if (attr != null) {
			long lastPF = 0;
			try {
				lastPF = Long.parseLong(attr.getValue().toString());
			} catch (NumberFormatException e) {
			}
			if (lastPF > System.currentTimeMillis() - 60000) {
				pi.error(new Exception("already forgotten"));
				return;
			}
		}
		user.setAttribute(user, "lastpasswordforgotten", System.currentTimeMillis());
		String uuid = UUID.randomUUID().toString();
		user.setAttribute("passwordforgotten", uuid).commit(user);

		String uri = pi.getURL()
				+ // "8080"
				"/forgot"
				+ // "/people"
				"?"
				+ // "?"
				"username=" + user.getName() + "&uuid=" + uuid;
		user.sendMail("password reset", "password reset: " + uri, c.getMailSettings());
	}

	private static void resetpassword(ProcessorInterface pi, Controller c, User u) throws Exception {
		String uuid = pi.getParameter("uuid");
		if (uuid == null) {
			pi.error(new Exception("no uuid"));
			return;
		}
		String username = pi.getParameter("username");
		if (username == null) {
			pi.error(new Exception("no username"));
			return;
		}
		String password = pi.getParameter("password");
		User user = (User) c.getNode("USERS").getChild(username);
		if (password == null) {
			//pi.error(new Exception("no password"));
			if (uuid.equals(user.getAttribute("passwordforgotten").getValue())) {
				pi.renderSite("setpassword", c, u);
			} else {
				pi.writeResult("wrong uuid");
			}
			return;
		}
		try {
			if (uuid.equals(user.getAttribute("passwordforgotten").getValue())) {
				user.setPassword(password, user);
			} else {
				pi.error(new Exception("wrong uuid"));
			}
		} catch (Exception e) {
			pi.error(e);
		}
	}

	private static void setLang(ProcessorInterface pi) {
		// TODO Auto-generated method stub

	}

	private static void processAdmin(String cmd, ProcessorInterface pi, Controller c, User u, Locale lang)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		String type;
		Node n;
		ObjectID ID;
		switch (cmd) {
			case "getChild":
				getChild(pi, c, u, lang);
				break;
			case "getNode":
				getNode(pi, c, u, lang);
				break;
			case "addType":
				addType(pi, c, u);
				break;
			case "find":
				find(pi.toJSON(), u, c, pi);
				return;
			case "findnew":
				findnew(pi.toJSON(), u, c, pi);
				return;
			case "moveNode":
				ID = ObjectID.getDefault(pi.getParameter("ID"));

				String parent = pi.getParameter("parent");
				c.moveNode(u, ID, ObjectID.getDefault(parent));

				break;
			case "copyNode":
				ObjectID childID = ObjectID.getDefault(pi.getParameter("childID"));

				parent = pi.getParameter("parent");
				Node child_copy = c.getNode(childID);
				Node parentnode = c.getNode(ObjectID.getDefault(parent));

				try {

					n = c.copyNode(u, child_copy, parentnode);
					pi.writeResult(n.toJSON(lang).toString());
				} catch (NumberFormatException e) {
					Log.error(e);
					pi.error(e);
				} catch (MongoWriteException e) {
					int code = e.getCode();
					if (code == 11000) {
						pi.writeResult(child_copy.getName() + " already exists in " + parentnode.getName());
					}
					pi.error(e);
					Log.error(e);
				}
				break;

			case "addChild":
				String name = pi.getParameter("name");
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);

				if (name == null || name.isEmpty()) {
					// response.sendError(response.SC_BAD_REQUEST,
					// "important_parameter needed");
					name = UUID.randomUUID().toString();
				}

				type = pi.getParameter("type");
				type = checkType(type);
				Class<?> cl = Class.forName(type);
				Node child = (Node) cl.getConstructor(Controller.class, Document.class).newInstance(c, new Document());
				child.setName(name);

				JSONObject attrs = pi.get("child");
				if (attrs != null) {
					Set<String> keys = attrs.keySet();
					for (String key : keys) {
						if (!child.doc.containsKey(key)) {
							child.doc.put(key, attrs.get(key));
						}
					}
				}

				// Map<String, String[]> attrs = new
				// HashMap<>(request.getParameterMap());
				// attrs.remove("ID");
				// attrs.remove("type");
				// attrs.remove("cmd");
				// attrs.remove("name");
				// if (attrs.isEmpty())
				child = n.addChild(child, u);

				// c.addNode(cat);
				// if (cat instanceof Attribute)
				// return;
				// cat.setAttributes(u,null, request, lang);
				pi.writeResult(child.toJSON(lang).toString());

				return;
			case "incrementOrderID":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				n.incrementOrderID(u);

				pi.writeResult("ok");
				return;
			case "decrementOrderID":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				n.decrementOrderID(u);

				pi.writeResult("ok");

				return;
			case "setOrderID":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				String order_id = pi.getParameter("order_id");
				n.setOrderID(Integer.valueOf(order_id)).commit(u);

				pi.writeResult("ok");

				return;
			case "toggleFlag":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				String visible = pi.getParameter("visible");
				String scope = pi.getParameter("scope");

				if (scope != null) {
					switch (scope) {
						case "f":
							n.setVisibleInFrontend(visible.equals("true"));
							break;
						case "a":
							n.setVisibleForAdmin(visible.equals("true"));
							break;
						case "s":
							n.setVisibleInSearch(visible.equals("true"));
							break;
						case "m":
							n.setVisibleInMenu(visible.equals("true"));
							break;
					}
				} else {
					return;
				}

				n.commit(u);

				break;
			case "delete":
				childID = ObjectID.getDefault(pi.getParameter("childID"));

				int deleted = c.delete(childID, u);

				if (deleted > 0) {
					pi.writeResult("ok");
				}

				return;
			case "changeName":
				changeName(c, pi, u);
				return;
			case "changeID":
				changeID(c, pi, u);
				return;
			case "update":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				name = pi.getParameter("name");

				if (name != null) {
					n.setName(name);
				}

				n.commit(u);
				// cat.setAttributes(null, request, lang, u.getID());

				c.close();
				break;
			// case "import":
			//
			// Collection<Part> fileParts = request.getParts();
			//
			// for (Part filePart : fileParts) {
			// String fileName = getFileName(filePart);
			// String content_type = filePart.getContentType();
			// InputStream is = filePart.getInputStream();
			//
			// if (content_type != null)
			// switch (content_type) {
			// case "text/csv":
			// BufferedReader in = new BufferedReader(new InputStreamReader(is));
			// core.Import im = new core.Import(c);
			// // im.importUsers(in,u.getID());
			//
			// int count;
			// response.setContentType("text/html;charset=utf-8");
			// PrintWriter out = response.getWriter();
			// out.println(fileName + "<br>");
			//
			// // response.setStatus(200);
			// try {
			// count = im.importProducts(u, out, in, n, lang, request);
			// } catch (SQLException e) {
			// // TODO Auto-generated catch block
			// Log.error(e);
			// return;
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// Log.error(e);
			// out.println(e.toString());
			//
			// return;
			// }
			//
			// response.getWriter().append("<br>imported: " + count + "");
			// break;
			// case "image/jpeg":
			// core.Picture img = new core.Picture(u.getID(), c);
			// img.setName(fileName);
			//
			// img = (core.Picture) n.addChild(img, u);
			// java.io.File d = new
			// java.io.File(ControllerBuilder.getRootPath(request.getServerName())
			// + "/media/" + img.getID() + ".jpg");
			//
			// final byte[] buf = new byte[32768]; // 32k
			// int bytesRead;
			// FileOutputStream fout = new FileOutputStream(d);
			//
			// try {
			// while ((bytesRead = is.read(buf)) != -1)
			// fout.write(buf, 0, bytesRead);
			// fout.flush();
			// } finally {
			// fout.close();
			// }
			// break;
			// }
			// }
			// break;
			// case "export":
			// response.setContentType("text/csv");
			//
			// response.setHeader("Content-Disposition", "filename=" + n.getName() +
			// ".csv");
			//
			// PrintWriter out = response.getWriter();
			// Map<String, String[]> params = new
			// HashMap<>(request.getParameterMap());
			// params.remove("tab");
			// type = request.getParameter("type");
			// if ("all".equals(type))
			// type = null;
			//
			// LinkedHashMap<String, Attribute> nattrs = n.getAttributes(true);
			// Set<String> attrs = new HashSet<>();
			// if (params.containsKey("all")) {
			// attrs = new HashSet<>(nattrs.keySet());
			// attrs.add("ID");
			// attrs.add("type");
			//
			// } else
			// attrs = params.keySet();
			//
			// try {
			// for (String attr : attrs) {
			// out.print("\"");
			// out.print(attr);
			// out.print("\"");
			// out.print(",");
			// }
			// out.println();
			// exportCSV(n, out, attrs, lang, type);
			// } catch (JSONException e) {
			// // TODO Auto-generated catch block
			// Log.error(e);
			// }
			// break;
			case "addToSet":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				name = pi.getParameter("name");
				n = c.getNode(ID);
				String elementvalue = pi.getParameter("value");
				n.addToSet(name, elementvalue, u);
				break;
			case "removeFromSet":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				name = pi.getParameter("name");
				n = c.getNode(ID);

				elementvalue = pi.getParameter("value");
				n.removeFromSet(name, elementvalue, u);
				break;
			case "addToList":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				name = pi.getParameter("name");
				n = c.getNode(ID);

				elementvalue = pi.getParameter("value");
				n.addToList(name, elementvalue, u);
				break;
			case "removeAllFromList":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				name = pi.getParameter("name");
				n = c.getNode(ID);

				elementvalue = pi.getParameter("value");
				n.removeAllFromList(name, elementvalue, u);
				break;

			case "inheritFromParent":
				ID = ObjectID.getDefault(pi.getParameter("ID"));
				n = c.getNode(ID);
				name = pi.getParameter("name");
				n.removeAttr(name);
				break;

			case "getChildren":
				ID = ObjectID.getDefault(pi.getParameter("ID"));

				getChildren(c, ID, lang, pi);
				break;
			case "getAttributes":
				ID = ObjectID.getDefault(pi.getParameter("ID"));

				getAttributes(c, ID, lang, pi);
				break;
			//case "getChildTemplate":
				//ID = ObjectID.getDefault(pi.getParameter("ID"));

				//getChildTemplate(c, ID, lang, pi);

				//break;
			case "getTemplate":
				ID = ObjectID.getDefault(pi.getParameter("ID"));

				getTemplate(c, ID, lang, pi);

				break;
			case "getChildrenTemplates":
				ID = ObjectID.getDefault(pi.getParameter("ID"));

				getChildrenTemplates(c, ID, lang, pi);

				break;
			case "deleteAttr":
				deleteAttr(c, pi, u);
				break;
			case "addAttr":
				addAttr(c, pi, u);
				break;
			case "addToMap":
				addToMap(c, pi, u, lang);

				break;
			default:
				break;
		}
	}

	private static void addType(ProcessorInterface pi, Controller c, User u) {
		String name = pi.getParameter("name");
		Primitive p = new Primitive(u.getID(), c);
		p.setName(name);
		p = (Primitive) c.getNode("TYPES").addChild(p, u);
		pi.writeResult(p.getID().get() + "");
	}

	private static void changeName(Controller c, ProcessorInterface pi, User u) {
		String ID = pi.getParameter("ID");
		Node n = c.getNode(ObjectID.getDefault(ID));
		n.setName(pi.getParameter("value")).commit(u);
	}

	private static void addToMap(Controller c, ProcessorInterface pi, User u, Locale lang) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("ID"));
		String name = pi.getParameter("name");
		String key = pi.getParameter("key");

		String value = pi.getParameter("value");
		Attribute attr = c.getNode(ID).getMyAttribute(name);
		Primitive type = attr.getType();
		if (type instanceof Map) {
			((Map) type).addToMap(attr, key, value, lang);
		}
	}

	private static void setAttr(Controller c, ProcessorInterface pi, User u, Locale locale)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("ID"));
		String name = pi.getParameter("name");
		String value = pi.getParameter("value");
		if ("password".equals(name)) {
			value = PasswordSecurity.generateStorngPasswordHash(value);
		}
		Node n = c.getNode(ID);
		if (!n.mayAccess(u)) {
			throw new RuntimeException("access denied");
		}
		Attribute attr = n.getMyAttribute(name);
		attr.setValue(value, locale, u);
		while (n != null) {
			n.deleteAttr("filledtemplate", u);
			n = n.getParent();
		}
	}

	private static void addAttr(Controller c, ProcessorInterface pi, User u) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("ID"));
		String name = pi.getParameter("name");
		c.getNode(ID).addAttr(name, u);
	}

	private static void deleteAttr(Controller c, ProcessorInterface pi, User u) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("ID"));
		String name = pi.getParameter("name");
		c.getNode(ID).deleteAttr(name, u);
	}

	private static String checkType(String type) {
		switch (type) {
			case "usermsg":
				return "core.Message";
			default:
				return type;
		}
	}

	private static void getTemplate(Controller c, ObjectID ID, Locale lang, ProcessorInterface pi) {

		try {
			Node n = c.getNode(ID);
			String template = pi.getParameter("template");
			Template tt = n.getAdminTemplate(template);
			pi.writeResult(tt.getUnfilled(n));
		} catch (Exception e) {
			Log.error(e);
			pi.writeResult(e.toString());
		}
	}

	private static void getChildrenTemplates(Controller c, ObjectID ID, Locale lang, ProcessorInterface pi) {
		try {
			Node n = c.getNode(ID);
			NodeIterator<Node> it = n.getChildren();
			String type = pi.getParameter("type");
			String template = pi.getParameter("template");

			if (type != null) {
				it.addFieldToFilter("type", type);
			}
			it.build();

			while (it.hasNext()) {
				Node nn = it.next();
				Template tt = nn.getAdminTemplate(template);
				if (tt == null) {
					pi.writeResult("no admin template found");
					continue;
				}
				Attribute wa = tt.getAttribute("wrapper");
				String wrapper = "div";
				if (wa != null) {
					wrapper = wa.getValue().toString();
				}
				pi.writeResult("<" + wrapper + " data-id='" + nn.getID() + "' class='child unfilled "
						+ nn.getClass().getSimpleName().toLowerCase(Locale.ENGLISH) + "'>");
				pi.writeResult(tt.toString());
				pi.writeResult("</" + wrapper + ">");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
			pi.writeResult(e.toString());
		}

	}


	private static void getAttributes(Controller c, ObjectID ID, Locale lang, ProcessorInterface pi) {
		String[] attrsa = pi.getParameterValues("attribute");
		String[] user_attributes = pi.getParameterValues("user_attributes");
		Node cat = c.getNode(ID);

		if (attrsa == null) {
			getAllAttributes(c, cat, lang, pi);
		} else {
			Set<String> attrs = new HashSet<>(Arrays.asList(attrsa));
			attrs.add("type");
			JSONObject jo = getAttributes(c, attrs, cat, lang, pi);

			if (user_attributes != null) {
				User u = cat.getOwner();
				attrs = new HashSet<>(Arrays.asList(user_attributes));

				JSONObject userattrs = getAttributes(c, attrs, u, lang, pi);
				try {
					jo.put("user", userattrs);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
			}

			pi.writeResult(jo.toString());

		}
	}

	// private static JSONObject getAttributes(Controller c, Set<String> attr_names,
	// Node n, Locale lang,
	// ProcessorInterface pi) {
	// try {
	// JSONObject jo = new JSONObject();
	// JSONObject attrname = new JSONObject();
	//
	// attrname.put("htmlrow", "<tr></tr>");
	// String html = "";
	// html += "<input id=" + n.getID()
	// + " class='attribute_input' onkeydown='attrChanged(this)' type='text'
	// value='" + n.getName() + "'>";
	// html += "<button tabindex='-1' type='button' style='display:none'
	// onclick=\"changeName(" + n.getID()
	// + ",this)\">save</button>";
	// html += "<div class='error'></div>";
	//
	// attrname.put("html", html);
	//
	// attrname.put("caption", "<label for='name'>name</label>");
	// attrname.put("value", n.getName());
	// jo.put("name", attrname);
	// attr_names.remove("name");
	//
	// JSONObject attrID = new JSONObject();
	// attrID.put("caption", "<label for='ID'>ID</label>");
	// attrID.put("value", n.getID());
	// attrID.put("html", n.getID());
	//
	// jo.put("ID", attrID);
	// attr_names.remove("ID");
	//
	//
	// for (String attr_name : attr_names) {
	// JSONObject attrj = new JSONObject();
	// Object value = "";
	// switch (attr_name) {
	// case "request_count":
	// value = n.doc.get("requests");
	// attrj.put("html", value);
	//
	// break;
	// default:
	// Attribute attr = n.getAttribute(attr_name);
	// if (attr == null)
	// continue;
	// String htmlrow = attr.toHTMLROW(lang, attr.getName(), n.getID(),
	// attr.getParentID() == n.getID(),
	// true);
	// attrj.put("htmlrow", htmlrow);
	//
	// attrj.put("caption",
	// "<label for=" + attr.getID() + ">" + attr.getCaption(lang) + "</label>");
	// attrj.put("html", attr.toHTML(lang));
	// attrj.put("inheritCheckBox", attr.getInheritCheckBox(n.getID()));
	// attrj.put("order_id", attr.getOrderID());
	// value = attr.getValue(lang);
	// break;
	//
	// }
	// attrj.put("value", value);
	//
	// attrj.put("name", attr_name);
	//
	// jo.put(attr_name, attrj);
	// }
	// return jo;
	// } catch (NumberFormatException e) {
	// // TODO Auto-generated catch block
	// Log.error(e);
	// pi.writeResult(e.toString());
	//
	// } catch (Exception e) {
	// Log.error(e);
	// pi.writeResult(e.toString());
	// }
	// return null;
	// }
	private static JSONObject getAttributes(Controller c, Set<String> attr_names, Node n, Locale lang,
			ProcessorInterface pi) {
		try {
			JSONObject jo = new JSONObject();
			for (String attr_name : attr_names) {
				JSONObject attrj = new JSONObject();
				Object value;
				switch (attr_name) {
					case "request_count":
						value = n.doc.get("requests");
						attrj.put("html", value);
						break;
					default:
						Attribute attr = n.getAttribute(attr_name);
						if (attr == null) {
							continue;
						}
						attrj = attr.toJSON(lang);
						break;

				}
				jo.put(attr_name, attrj);
			}
			return jo;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			Log.error(e);
			pi.writeResult(e.toString());

		} catch (Exception e) {
			Log.error(e);
			pi.writeResult(e.toString());
		}
		return null;
	}

	private static void getAllAttributes(Controller c, Node cat, Locale lang, ProcessorInterface pi) {
		JSONObject jo = new JSONObject();

		String admin = pi.getParameter("admin");
		LinkedHashMap<String, Attribute> attrs = cat.getAttributes(true);
		Iterator<Attribute> it = attrs.values().iterator();
		if ("true".equals(admin)) {
			while (it.hasNext()) {
				Attribute attr = it.next();
				jo.put(attr.getName(), attr.toJsonAdmin(lang));
			}
		} else {
			while (it.hasNext()) {
				Attribute attr = it.next();
				jo.put(attr.getName(), attr.toJSON(lang));
			}
		}
		pi.writeResult(jo.toString());

	}

	private static void getChildren(Controller c, ObjectID ID, Locale lang, ProcessorInterface pi) {
		JSONArray ja = new JSONArray();
		Iterator<Node> it = c.getNode(ID).getChildren().build();
		while (it.hasNext()) {
			JSONObject jnode = new JSONObject();
			Node node = it.next();
			jnode.put("ID", node.getID());
			jnode.put("name", node.getName());
			jnode.put("link", node.getBackLink(lang));

			jnode.put("type", node.getClass().getName());

			ja.put(jnode);

		}
		pi.writeResult(ja.toString());

	}

	private static String[] getAllClassesExtendsClass(String classname) {
		switch (classname) {
			case "core.User":

				return new String[]{classname, "shop.Customer"};
			case "core.Node":

				return new String[]{classname, "shop.Order"};
			case "message":
				return new String[]{classname, "core.Node"};
			case "templates.Template":
				return new String[]{"templates.HtmlTemplate", "templates.StringTemplate"};
			default:
				break;
		}
		return new String[]{classname};
	}

	private static void next(JSONObject jo, User u, Controller c, ProcessorInterface pi) {
				pi.writeResult(pi.getClient().next());
	}
	private static void find(JSONObject jo, User u, Controller c, ProcessorInterface pi) {
		NodeIterator<Node> it;
		Node n = null;
		Log.log(jo.toString());

		String[] type = null;

		if (jo.has("filter")) {
			JSONObject filter = jo.getJSONObject("filter");
			if (filter.has("type")) {
				String t = filter.getString("type");
				if ("mychildren".equals(t) && filter.has("parent")) {
					ObjectID parent = ObjectID.getDefault(filter.getString("parent"));
					Set<Class<?>> types = c.getNode(parent).canContain();
					if (types == null) {
						pi.writeResult("{\"cmd\":\"last\"}");
						return;
					}
					Class<?>[] tarray = types.toArray(new Class[types.size()]);
					type = new String[tarray.length];
					for (int i = 0; i < tarray.length; i++) {
						type[i] = tarray[i].getName();
					}
				} else {
					type = getAllClassesExtendsClass(filter.getString("type"));
				}
				filter.remove("type");

			}
			int id;
			String nID = pi.getParameter("ID");
			if (nID != null && u.isAdmin()) {
				n = c.getNode(ObjectID.getDefault(nID));
			} else {
				String path = pi.getParameter("path");
				if (path != null) {
					n = c.getNodeByPath(path);
				}
			}
			if (filter.has("wrapper")) {
				filter.remove("wrapper");
			}
			String attribute;

			it = c.find();

			if (type != null) {
				it.addFieldToFilter("type", type);
			}
			Iterator<String> keys = filter.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				if (key.equals("parent") || key.equals("_id")) {
					it = it.addAttributeToFilter(key, ObjectID.getDefault(filter.get(key)).get(), false);
				} else {
					it = it.addAttributeToFilter(key, filter.get(key), false);
				}
			}
			if (jo.has("regex")) {
				JSONObject regex = jo.getJSONObject("regex");
				keys = regex.keys();
				while (keys.hasNext()) {
					String key = keys.next();
					it = it.addAttributeToFilter(key, regex.get(key).toString(), c.getLang(), true);
				}
			}
			if (n != null) {
				it.setPath(n.getPath());
			}

			if (jo.has("callback")) {
				it.setCallback(jo.getString("callback"));
			}
			// .addFieldToFilter("PARENT",
			// CategoryIterator.INTEGER, cat.getID())
			if (type != null) {
				it.addFieldToFilter("type", type);
			}

			if (!u.isAdmin()) {
				it.addAttributeToFilter("group", "user", false);
			}

			if (jo.has("sort")) {
				JSONObject sort = jo.getJSONObject("sort");
				it.sort(sort);
			}

			if (jo.has("attrs")) {
				JSONObject attrs = jo.getJSONObject("attrs");
				it.setAttrs(attrs.keySet());
			}
			if (jo.has("template")) {
				it.setTemplateName(jo.getString("template"));
			}

			it.build();
			pi.setIterator(it);
			// u.setIterator(it);

			JSONObject first = new JSONObject();

			first.put("cmd", "first");
			first.put("hasnext", it.hasNext());
			if (n != null) {
				ArrayList<String> fields = new ArrayList<>();
				Iterator<Attribute> fit = n.getAttributes(true).values().iterator();
				while (fit.hasNext()) {
					Attribute a = fit.next();
					fields.add(a.getName());
				}
				first.put("fields", fields);
			}

			pi.writeResult(first);
		}

	}

	/**
	 *
	 * @param it
	 * @param lang
	 * @return
	 */
	public static String next(NodeIterator<Node> it, Locale lang) {

		if (it == null) {
			return "null";
		}
		if (it.hasNext()) {
			Node cat = it.next();
			JSONObject jo = new JSONObject();

			jo.put("cmd", "entry");

			JSONObject n = new JSONObject();

			Set<String> keys = cat.doc.keySet();
			for (String key : keys) {
				n.put(key, cat.doc.get(key).toString());
			}

			n.put("htmlrow", cat.toHTMLROW(lang));

			jo.put("node", n);
			// jo.put("attrs", jattrs);

			return jo.toString();

		} else {
			return "{\"cmd\":\"last\"}";
		}
	}


	private static void sendNodeAttrs(Node n, ProcessorInterface pi, Locale lang, User u, boolean recursive,
			boolean html) throws IOException {

		HashMap<String, Attribute> attrs;
		attrs = n.getAttributes(recursive);

		Iterator<Attribute> it = attrs.values().iterator();
		JSONObject json = new JSONObject();
		json.put("name", new JSONObject().put("value", n.getName()));
		json.put("cid", new JSONObject().put("value", n.getCID()));

		json.put("link", n.getFrontLink(lang));

		while (it.hasNext()) {
			Attribute attr = it.next();
			JSONObject attrj = new JSONObject();

			// out.println(attr.getName());
			JSONObject cattr = new JSONObject().put("value", n.getCaption(lang));
			attrj.put("caption", cattr);

			Object value = attr.getValue(lang);

			// value = c.getTypeHTMLView(attr.getName(),
			// attr.getCustomValueType(), value);
			if (html) {
				value = attr.toHTML(lang);
			}

			if (value != null) {
				attrj.put("value", value.toString());
			}

			json.put(attr.getName(), attrj);

		}

		pi.writeResult(json.toString());
	}

	private static void sendConfigurableAttrs(Node cproduct, Node n, JSONObject confs, ProcessorInterface pi,
			Locale lang, User u, boolean recursive, boolean html) throws IOException {

		HashMap<String, Attribute> attrs;
		attrs = n.getAttributes(recursive);

		Iterator<Attribute> it = attrs.values().iterator();
		JSONObject json = new JSONObject();
		json.put("name", n.getName());

		json.put("link", n.getFrontLink(lang));

		json.put("ID", n.getID());

		while (it.hasNext()) {
			Attribute attr = it.next();
			JSONObject attrj = new JSONObject();

			Attribute cattr = cproduct.getAttribute(attr.getName());

			Object value = "";

			if (cattr.isConfigurable()) {
				if (confs.has(cattr.getName())) {
					continue;
				} else {
					attr = cattr;
				}
				(attr).setConfiguration(confs);
			}

			// value = c.getTypeHTMLView(attr.getName(),
			// attr.getCustomValueType(), value);
			if (html) {
				value = attr.toHTML(lang);
			}

			attrj.put("value", value.toString());

			json.put(attr.getName(), attrj);

		}

		pi.writeResult(json.toString());
	}

	private static synchronized void setUserAttr(ProcessorInterface pi, Controller c, User u) throws Exception {
		String name = pi.getParameter("name");
		String value = pi.getParameter("value");
		if ("password".equals(name)) {
			value = PasswordSecurity.generateStorngPasswordHash(value);
		}

		if (name == null || value == null) {
			return;
		}

		u.setAttribute(name, value).commit(u);
	}

	private static void getChildrenCount(ProcessorInterface pi, Controller c, User u) throws IOException {
		String child = pi.getParameter("child");
		Node ch = u.getChild(child);
		if (ch != null) {
			long count = ch.getChildrenCount();
			pi.writeResult(count + "");
		} else {
			pi.writeResult("0");
		}

	}

	private static boolean addNode(ProcessorInterface pi, Controller c, User u,Locale locale) throws Exception {
		JSONObject node=pi.getJSONObject("node");
		Node n = new Node(c,node);
		Node parent=c.getNode(ObjectID.getDefault(node.get("parent")));
		if (!parent.mayAccess(u))
			throw new RuntimeException("access denied");
		n=c.addNode(n,u);
		pi.writeResult(n.toJSON());

		
		return true;
	}
	private static boolean addChildToUser(ProcessorInterface pi, Controller c, User u) throws Exception {
		String parent_name = pi.getParameter("path");
		String parent_type = pi.getParameter("parent_type");
		Node parent = u.getChild(parent_name);
		if (parent == null) {
			Class<?> clazz = Class.forName(parent_type);
			Node n = (Node) clazz.getConstructor(ObjectID.class,Controller.class).newInstance(u.getID(), c);
			n.setName(parent_name);
			parent = u.addChild(n, u);
		}

		Set<String> keys = pi.getKeys();
		String name;
		if (keys.contains("name")) {
			name = pi.remove("name");
		} else {
			name = UUID.randomUUID().toString();
		}

		String type = pi.remove("type");
		Class<?> clazz = Class.forName(type);

		Node child = (Node) clazz.getConstructor(ObjectID.class, Controller.class).newInstance(u.getID(), c);
		child.setName(name);
		child = parent.addChild(child, u);
		for (String key : keys) {
			child.setAttribute(key, pi.getParameter(key)).commit(u);
		}
		return false;
	}

	private static void remove(ProcessorInterface pi, Controller c, User u) {
		ObjectID id = ObjectID.getDefault(pi.getParameter("_id"));
		Node n = c.getNode(id);
		if(!u.isAdmin() && !u.contains(n)) 
				return;
		c.remove(n, u);
	}

	private static void getChildrenTemplatesFromUser(ProcessorInterface pi, Controller c, User u) throws IOException {

		String nodename = pi.getParameter("name");
		Node n = u.getChild(nodename);
		if (n == null) {
			pi.error(new Exception("java.util.NoSuchElementException"));
			return;
		}
		StringBuilder sb = new StringBuilder();
		NodeIterator<Node> it;
		it = n.getChildren().sort("cid", false).build();

		while (it.hasNext()) {
			Node child = it.next();

			Template t;
			t = child.getTemplate();

			if (t == null) {
				continue;
			}
			String wt = t.getWrapperTag();

			sb.append("<").append(wt).append(" class='").append(child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH)).append(" unfilled' data-id='").append(child.getID()).append("'>");
			sb.append(t.toString());
			sb.append("</").append(wt).append(">");

		}
		pi.writeResult(sb.toString());

	}

	private static void getRelatedTemplate(ProcessorInterface pi, Controller c, User u) throws Exception {
		ObjectID parent = ObjectID.getDefault(pi.getParameter("parent"));
		String name = pi.getParameter("name");
		Node n = c.getNode(parent);

		n = n.getRelated(name);
		if (n == null) {
			throw new Exception("no such related: " + name);
		}

		Template t = n.getTemplate();
		String wt = t.getWrapperTag();

		pi.writeResult("<" + wt + " class='" + n.getClass().getSimpleName().toLowerCase(Locale.ENGLISH) + "' data-id='"
				+ n.getID() + "'>");
		pi.writeResult(t.toString());
		pi.writeResult("</" + wt + ">");

	}
	private static void getChildTemplate(ProcessorInterface pi, Controller c, User u) throws Exception {
		ObjectID parent = ObjectID.getDefault(pi.getParameter("parent"));
		String name = pi.getParameter("name");
		Node n = c.getNode(parent);

		n = n.getChild(name);
		if (n == null) {
			throw new Exception("no such child: " + name);
		}

		Template t = n.getTemplate();
		String wt = t.getWrapperTag();

		pi.writeResult("<" + wt + " class='" + n.getClass().getSimpleName().toLowerCase(Locale.ENGLISH) + " unfilled' data-id='"
				+ n.getID() + "'>");
		pi.writeResult(t.toString());
		pi.writeResult("</" + wt + ">");

	}

	private static void getChildrenTemplates(ProcessorInterface pi, Controller c, User u) throws IOException {
		ObjectID id = ObjectID.getDefault(pi.getParameter("ID"));
		Node n = c.getNode(id);

		
		if (!n.mayAccess(u) && !n.visibleInFrontEnd()) {
			return;
		}
		n.getChildrenTemplates(pi);

	}

	private static void getChildrenTemplatesByUserID(ProcessorInterface pi, Controller c, User u) throws IOException {
		NodeIterator<Node> it;
		it = c.find().addFieldToFilter("owner", u.getID().get()).addFieldToFilter("type", pi.getParameter("type"))
				.sort("created", true).build();

		StringBuilder sb = new StringBuilder();

		while (it.hasNext()) {
			Node child = it.next();

			Template t;
			t = child.getTemplate();

			if (t == null) {
				continue;
			}
			String wt = t.getWrapperTag();

			sb.append("<").append(wt).append(" class='").append(child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH)).append(" unfilled' data-id='").append(child.getID()).append("'>");
			sb.append(t.toString());
			sb.append("</").append(wt).append(">");

		}
		pi.writeResult(sb.toString());
	}

	private static void getChildrenMenu(ProcessorInterface pi, Controller c, Locale lang) throws IOException {
		JSONObject jo = new JSONObject();
		String ID = pi.getParameter("ID");
		NodeIterator<Node> it = c.getNode(ObjectID.getDefault(ID)).getChildren().sort("order_id", false).build();
		while (it.hasNext()) {
			Node node = it.next();
			if (!node.visibleInMenu()) {
				continue;
			}

			JSONObject jnode = new JSONObject();
			jnode.put("ID", node.getID());
			jnode.put("link", node.getFrontLink(lang));

			jnode.put("caption", node.getCaption(lang));

			Template tt = node.getThumbnailTemplate();
			if (tt != null) {
				jnode.put("template", tt.getValue());
			}

			jnode.put("type", node.getClass().getName());

			jo.put(node.getName(), jnode);

		}
		pi.writeResult(jo.toString());

	}

	private static void getChildren(ProcessorInterface pi, Controller c, Locale lang, User u) throws IOException {
		JSONObject jo = new JSONObject();
		String ID = pi.getParameter("ID");

		NodeIterator<Node> it = c.getNode(ObjectID.getDefault(ID)).getChildren().sort("order_id", false).build();
		while (it.hasNext()) {
			Node node = it.next();
			if (!node.visibleInFrontEnd() && !u.isAdmin()) {
				continue;
			}

			JSONObject jnode = new JSONObject();
			jnode.put("ID", node.getID());
			jnode.put("link", node.getFrontLink(lang));

			jnode.put("name", node.getName());

			jnode.put("caption", node.getCaption(lang));

			Template tt = node.getThumbnailTemplate();
			if (tt != null) {
				jnode.put("template", tt.getValue());
			}

			jnode.put("type", node.getClass().getName());

			jo.put(node.getName(), jnode);

		}
		pi.writeResult(jo.toString());

	}

	private static void getChildAttrs(ProcessorInterface pi, Controller c, Locale lang, User u){
		String _id = pi.getParameter("_id");
		String name = pi.getParameter("name");
		if (_id == null) {
			throw new RuntimeException("No ID passed");
		}
		Node n = c.getNode(ObjectID.getDefault(_id)).getChild(name);
		sendAttrs(n,pi, c, lang, u);
	}
	private static void _sendAttrs(ProcessorInterface pi, Controller c, Locale lang, User u) throws Exception {
		String ID = pi.getParameter("ID");
		if (ID == null) {
			throw new Exception("No ID passed");
		}
		Node n = c.getNode(ObjectID.getDefault(ID));
		sendAttrs(n,pi, c, lang, u);

	}

	private static void sendAttrs(Node n, ProcessorInterface pi, Controller c, Locale lang, User u) {
		if (!n.visibleInFrontEnd()) {
			if(!n.mayAccess(u))
				throw new RuntimeException("access denied");
		}
		String[] names = pi.getParameterValues("names");
		if(names==null)
			throw new RuntimeException("no names passed");

		Set<String> attrs = new HashSet<>(Arrays.asList(names));
		attrs.add("type");
		JSONObject jo = getAttributes(c, attrs, n, lang, pi);
		pi.writeResult(jo.toString());
	}

	private static void getChild(ProcessorInterface pi, Controller c, User u, Locale lang) {
		ObjectID parent = ObjectID.getDefault(pi.getParameter("parent"));
		String name = pi.getParameter("name");
		Node n=c.getNode(parent).getChild(name);
		if (!n.mayAccess(u))
			throw new RuntimeException("access denied");
		pi.writeResult(n.toJSON());
	}

	private static void refreshTemplate(ProcessorInterface pi, Controller c, User u, Locale lang) {
		if(!u.isAdmin())return;
		ObjectID _id = ObjectID.getDefault(pi.getParameter("_id"));
		Node n = c.getNode(_id);
		n.deleteAttr("filledtemplate", u);
	}

	private static void delNode(ProcessorInterface pi, Controller c, User u, Locale lang) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("_id"));
		Node n = c.getNode(ID);
		if (!n.mayAccess(u)) {
			throw new RuntimeException("access denied");
		}
		int deleted=c.delete(n,u);
		JSONObject r = new JSONObject();
		pi.writeResult(r.put("deleted",deleted));
	}

	private static void getNode(ProcessorInterface pi, Controller c, User u, Locale lang) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("_id"));
		Node n = c.getNode(ID);
		if (!n.mayAccess(u)) {
			throw new RuntimeException("access denied");
		}
		pi.writeResult(n.toJSON());
	}

	private static void addSession(ProcessorInterface pi, User u, Controller c, ProcessorInterface pi0) {
		ObjectID ID = ObjectID.getDefault(pi.getParameter("parent"));
		Node n = c.getNode(ID);
		Session s = new Session(u.getID(), c);
		s.setName(pi.getSessionID());
		n.addChild(s, u);
		JSONObject jo=new JSONObject()
				.put("session",s.toString());
		pi.writeResult(jo);
	}

	private static void changeID(Controller c, ProcessorInterface pi, User u) {
		String oldid = pi.getParameter("ID");
		String newid = pi.getParameter("newid");

		c.changeID(ObjectID.getDefault(oldid), ObjectID.getDefault(newid), u);
	}

	private static void findnew(JSONObject jo, User u, Controller c, ProcessorInterface pi) {
		String filter = jo.getString("filter");
		Log.log(filter);
		NodeIterator it = c.find(filter);
		it.build();
		pi.getClient().setIterator(it);
			JSONObject first = new JSONObject();

			first.put("cmd", "first");
			first.put("hasnext", it.hasNext());

			pi.writeResult(first);
	}
}
