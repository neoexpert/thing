package web;

import core.id.ObjectID;


/**
 *
 * @author neoexpert
 */
public class PageInfo {
	ObjectID rootNodeID;

    /**
     *
     * @return
     */
    public ObjectID getRootNodeID() {
		return rootNodeID;
	}

    /**
     *
     * @param rootNodeID
     */
    public void setRootNodeID(ObjectID rootNodeID) {
		this.rootNodeID = rootNodeID;
	}

    /**
     *
     * @return
     */
    public String getPath() {
		return path;
	}

    /**
     *
     * @param h
     */
    public void setPath(String h) {
		this.path = h;
	}

	String path="";

    /**
     *
     * @return
     */
    @Override
	public String toString() {
		return path;
	}
}
