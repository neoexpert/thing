package shop;

import core.Picture;
import core.attribute.Configuration;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.id.ObjectID;
import org.bson.Document;
import org.json.JSONObject;

/**
 * Konfigurierbares Produkt enthält mehrere einfache Produkte.
 *
 * @author neoexpert
 *
 */
public class Configurable_Product extends Product {

	static {
		addChildType(Configurable_Product.class, Product.class);
		addChildType(Configurable_Product.class, Configuration.class);

	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Configurable_Product(ObjectID userID, Controller con) {
		super(userID, con);
	}
/**
     *
     * @param c
	 * @param doc
     */
    public Configurable_Product(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 * @param confs
	 * @return
	 * @throws Exception Gibt ein einfaches Produkt entsprechend seiner
	 * Konfiguration zurück
	 */
	public NodeIterator<Product> getSimpleProductByConfiguration(JSONObject confs) throws Exception {
		return c.getNodeByConfiguration(this, confs);
	}
}
