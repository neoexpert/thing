package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;
import templates.Template;

/**
 *
 * @author neoexpert
 */
public class Users extends Node {

	static {
		addChildType(Users.class, User.class);
		addChildType(Users.class, Template.class);

	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Users(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param i
	 * @param c
	 */
	public Users(ObjectID i, Controller c) {
		super(i, c);
	}

}
