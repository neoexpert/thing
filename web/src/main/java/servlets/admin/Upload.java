package servlets.admin;

import com.neoexpert.Log;
import core.Node;
import core.Picture_OLD;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import core.id.ObjectID;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

/**
 * Servlet implementation class Gallery
 */
@WebServlet("/admin/upload")
public class Upload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Upload() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}

		HttpSession session = request.getSession();
		User u = c.getUser(session);
		request.setAttribute("username", u.getName());

		if (!u.isLoggedIn("admin")) {
			// RequestDispatcher rd =
			// request.getRequestDispatcher("/WEB-INF/login.jsp");

			// rd.forward(request, response);
			response.sendRedirect("/login");
			String uri = WebControllerBuilder.getFullURL(request);
			session.setAttribute("redirectTO", uri);

			return;
		}
		HttpController.getLang(request);

		String wrapper = request.getParameter("wrapper");
		RequestDispatcher rd;
		if (wrapper != null) {
			rd = request.getRequestDispatcher("/WEB-INF/Admin/Gallery.jsp");

		} else {

			request.setAttribute("entity", "GALLERY");
			rd = request.getRequestDispatcher("/WEB-INF/Admin/ControlBar.jsp");
		}

		// RequestDispatcher rd =
		// request.getRequestDispatcher("../WEB-INF/Admin/ControlBar.jsp");

		// RequestDispatcher rd =
		// request.getRequestDispatcher("../WEB-INF/Admin/Gallery.jsp");

		rd.forward(request, response);
		c.close();

	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		HttpSession session = request.getSession();
		User u = c.getUser(session);
		if (!u.isLoggedIn("admin")) {
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");

			rd.forward(request, response);
			String uri = WebControllerBuilder.getFullURL(request);
			session.setAttribute("redirectTO", uri);

			return;
		}
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
			doGet(request, response);
			return;
		}
		try {

			List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory())
					.parseRequest(new ServletRequestContext(request));
			Node gallery;
			for (FileItem item : items) {
				if (item.isFormField()) {
					// Process regular form field (input
					// type="text|radio|checkbox|etc", select, etc).
					String fieldName = item.getFieldName();
					// String fieldValue = item.getString();
					switch (fieldName) {
					case "name":
						// p.setName(new String(fieldValue.getBytes
						// ("iso-8859-1"), "UTF-8"));
						break;
					case "description":
						// p.setDescription(new String (fieldValue.getBytes
						// ("iso-8859-1"), "UTF-8"));
						break;
					case "ID":
						

						// p.setID(Integer.valueOf(fieldValue));
					}
					// ... (do your job here)
				} else {
					// Process form file field (input type="file").
					// String fieldName = item.getFieldName();
					String fileName = item.getName();
					// p.setFileName(fileName);
					InputStream in = item.getInputStream();
					
					Picture_OLD p = new Picture_OLD(u.getID(), c);
					p.setName(fileName);
					ObjectID ID = new ObjectID(request.getParameter("ID"));
					gallery = c.getNode(ID);
					if(gallery==null) return;
						gallery.addChild(p,u);

					File d=new File(ControllerBuilder.getRootPath(request.getServerName()) + "/media/" + gallery.getID());
					if(!d.exists())
					{
                                            boolean mkdirs = d.mkdirs();
					}
					File f = new File(d, fileName);

					if (f.exists()) {
						response.setStatus(409);
						response.getWriter().append(fileName);
						return;
					}
					FileOutputStream out = new FileOutputStream(f);
					byte[] buffer = new byte[1024];
					int len = in.read(buffer);
					while (len != -1) {
						out.write(buffer, 0, len);
						len = in.read(buffer);
					}
					out.close();
					response.setStatus(200);
					response.getWriter().append(fileName);
				}
			}
		} catch (FileUploadException e) {
			throw new ServletException("Cannot parse multipart request.", e);
		}
		// c.addPicture(p);
		catch (IOException e) {
			response.getWriter().append(e.toString());
			Log.error(e);
		}
		c.close();

		// response.sendRedirect("/admin/picture?ID="+p.getID());
	}

}
