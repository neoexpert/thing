package servlets.admin;

import com.neoexpert.Log;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import core.id.ObjectID;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DeleteCategory
 */
@WebServlet("/admin/deletePicture")
public class DeletePicture extends HttpServlet {
	private static final long serialVersionUID = 1L;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletePicture() {
        super();
    }

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		HttpSession session = request.getSession();
		User u=c.getUser(session);
		if(!u.isLoggedIn("admin")){
			//RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");

			//rd.forward(request, response);
			response.sendRedirect("/login");
			String uri=WebControllerBuilder.getFullURL(request);
			session.setAttribute("redirectTO",uri);
			
			return;
		}
		String ID = request.getParameter("ID");
		try {
			if(c.deletePicture(ControllerBuilder. getRootPath(request.getServerName()),new ObjectID(ID),u))
				response.getWriter().append("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		c.close();
	}

}
