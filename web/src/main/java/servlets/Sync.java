package servlets;

import com.neoexpert.Log;
import core.DFile;
import core.DFile.Line;
import core.User;
import core.controller.Controller;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
@ServerEndpoint(value = "/sync", configurator = core.GetHttpSessionConfigurator.class)
public class Sync {

    /**
     *
     */
    public static class Client {

        private final Session session;
        Controller c;
        private final Locale lang;
        private User u;
        private int version;

        /**
         *
         * @return
         */
        public int getVersion() {
            return version;
        }

        /**
         *
         * @param c
         * @param u
         * @param session
         * @param lang
         */
        public Client(Controller c, User u, Session session, Locale lang) {
            this.c = c;
            this.u = u;
            this.session = session;
            this.lang = lang;
        }

        /**
         *
         * @return
         */
        public Controller getController() {
            return c;
        }

        Session getSession() {
            return session;
        }

        /**
         *
         * @param obj
         * @return
         */
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Client) {
                return session.equals(((Client) obj).getSession());
            }
            return false;
        }

        /**
         *
         * @return
         */
        @Override
        public int hashCode() {
            return session.hashCode();
        }

        /**
         *
         */
        public void close() {
            c.close();
        }

        /**
         *
         * @param text
         */
        public void send(String text) {
            try {
                session.getBasicRemote().sendText(text);
            } catch (IOException e) {
                CLIENTS.remove(session);
                try {
                    session.close();
                } catch (IOException e1) {
                    Log.error(e1);
                }

                Log.error(e);
            }
        }

        /**
         *
         * @param u
         */
        public void setUser(User u) {
            this.u = u;
        }

        /**
         *
         * @return
         */
        public User getUser() {
            return u;
        }

        /**
         *
         * @param version
         */
        public void setVersion(int version) {
            this.version = version;
        }
    }

    private static final HashMap<Session, Client> CLIENTS = new HashMap<>();

    /**
     *
     * @return
     */
    public static int getClientsCount() {
        if (CLIENTS != null) {
            return CLIENTS.size();
        }
        return 0;
    }

    /**
     *
     * @param session
     * @param config
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        HttpSession httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
        String domain = (String) config.getUserProperties().get("domain");
        Locale lang = null;
        boolean su = false;

        if (httpSession != null) {
            lang = (Locale) httpSession.getAttribute("locale");
            su = httpSession.getAttribute("ROOT") != null;
        }
        if (lang == null) {
            lang = Locale.ENGLISH;
        }
        HttpController c;
        try {
            c = new WebControllerBuilder(domain, su, lang,httpSession.getServletContext()).getHttpController();
        } catch (Exception e) {
            Log.error(e);
            return;
        }

        // User u = null;
        // if (httpSession != null)
        // u = c.getUser(httpSession);
        Client client = new Client(c, null, session, lang);
        // if (u == null)
        // client.send("{cmd:\"login\"}");

        synchronized (CLIENTS) {
            CLIENTS.put(session, client);
        }
    }

    private static final DFile FILE;

    static {

        String s = "Hello World\n\n";
        FILE = new DFile(s);

        FILE.addLine(new Line("Diese TextArea ist in allen Browsern synchronisiert.", true));
        FILE.addLine(new Line());
        FILE.addLine(new Line("diese Zeile ist schreibgeschützt", true));
        FILE.addLine(new Line());
        FILE.addLine(new Line());

        FILE.addLine(new Line("Schreiben funktioniert zur zeit nur auf Desktop-Browser", true));
        FILE.addLine(new Line("TODO:", true));
        FILE.addLine(new Line("* einfügen funktioniert noch nicht", true));
        FILE.addLine(new Line("* unerstützung für mobile Browser", true));

    }

    private String getFile() {
        synchronized (FILE) {
            JSONObject jo = new JSONObject();
            JSONObject jofile = FILE.toJSONObject();

            jo.accumulate("cmd", "file").accumulate("file", jofile);
            return jo.toString();
        }
    }

    private boolean addToFile(JSONObject jo, Session session) {
        synchronized (FILE) {
            String c = jo.getString("char");
            int startline = jo.getInt("startline");
            int endline = jo.getInt("endline");
            int start = jo.getInt("start");
            int end = jo.getInt("end");
            if (startline != endline || start != end) {
                if (!deleteFromFile(jo, session)) {
                    return false;
                }
                jo.put("cmd", "add");
            }
            char ch = c.charAt(0);
            if (ch == '\r') {
                return false;
            }

            Line line = FILE.getLine(startline);
            if (ch == '\n') {
                if (!FILE.breakLine(startline, start)) {
                    return false;
                }
                freeline(FILE.getLine(startline));
                locklineForUser(startline + 1, session);
            } else if (!line.addChar(start, ch)) {
                return false;
            }

            sendToOthers(jo.toString(), session);
        }
        return true;
    }

    private String locklineForUser(int linepos, Session session) {
        Line previousline = FILE.getLineOf(session);

        Line line = FILE.lockLine(linepos, session);
        if (line == null) {
            return null;
        }

        JSONObject jo = new JSONObject();
        jo.put("cmd", "lockline");
        jo.put("line", linepos);

        if (previousline != null) {
            if (previousline.unlock(session) && !previousline.equals(line)) {
                freeline(previousline);
            }
        }
        sendToOthers(jo.toString(), session);
        jo.put("cmd", "yourline");
        return jo.toString();
    }

    private boolean deleteFromFile(JSONObject jo, Session session) {
        synchronized (FILE) {
            int startline = jo.getInt("startline");
            int endline = jo.getInt("endline");
            int start = jo.getInt("start");
            int end = jo.getInt("end");
            if (startline < endline && start == -1 && end == -1) {
                if (!FILE.deleteLine(startline)) {
                    return false;
                }
                JSONObject delline = new JSONObject().accumulate("cmd", "delline").accumulate("line", startline);

                sendToOthers(delline.toString(), session);

                return true;
            }

            if (startline == endline) {
                if (start == -1 && end == -1) {
                    if (!FILE.connectLines(startline - 1)) {
                        return false;
                    }
                    jo = new JSONObject().put("cmd", "connectline").put("line", startline - 1);
                    sendToOthers(jo.toString(), session);

                    return true;
                }
                Line line = FILE.getLine(startline);
                if (start == end) {
                    if (!line.removeChar(start)) {
                        return false;
                    }
                } else if (!line.removeChars(start, end)) {
                    return false;
                }
            } else {
                FILE.removeRange(startline, start, endline, end);
            }

            jo.put("cmd", "del");
            sendToOthers(jo.toString(), session);
        }
        return true;
    }

    /**
     *
     * @param message
     * @param session
     * @return
     */
    @OnMessage
    public String onMessage(String message, Session session) {
        JSONObject jo;
        try {
            jo = new JSONObject(message);
        } catch (JSONException e) {
            Log.error(e);
            return "error";
        }
        String cmd;
        try {
            if (jo.has("cmd")) {
                cmd = jo.getString("cmd");
            } else {
                cmd = jo.getString("type");
            }

        } catch (JSONException e) {
            Log.error(e);
            return "error";
        }
        if (FILE.contains('\n')) {
            getClass();
        }
        synchronized (CLIENTS) {
            final Client c = CLIENTS.get(session);

            switch (cmd) {
                case "getfile":
                    return getFile();
                case "add":
                    addToFile(jo, session);
                    break;
                case "del":
                    deleteFromFile(jo, session);
                    break;
                case "lockline":
                    int linepos = jo.getInt("line");
                    return locklineForUser(linepos, session);
            }
            return "{}";
        }

    }

    private void freeline(Line line) {
        JSONObject lineisfree = new JSONObject().put("cmd", "lineisfree").put("line", FILE.indexOf(line));
        sendToAll(lineisfree.toString());
    }

    /**
     *
     * @param t
     */
    @OnError
    public void onError(Throwable t) {
        Log.error(t);
    }

    /**
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session) {
        synchronized (CLIENTS) {
            CLIENTS.get(session).close();
            CLIENTS.remove(session);
            Line line = FILE.removeUser(session);
            freeline(line);
        }
    }

    /**
     *
     * @param message
     */
    public static void sendToAll(String message) {

        synchronized (CLIENTS) {
            for (Session s : CLIENTS.keySet()) {
                if (s.isOpen()) {
                    try {
                        s.getBasicRemote().sendText(message);
                    } catch (IOException ex) {
                        // TODO Auto-generated catch block
                        // ex.printStackTrace();
                    }
                }
            }

        }
    }

    /**
     *
     * @param message
     * @param me
     */
    public static void sendToOthers(String message, Session me) {

        synchronized (CLIENTS) {
            for (Session s : CLIENTS.keySet()) {
                if (s.equals(me)) {
                    continue;
                }
                if (s.isOpen()) {
                    try {
                        s.getBasicRemote().sendText(message);
                    } catch (IOException ex) {
                        // TODO Auto-generated catch block
                        // ex.printStackTrace();
                    }
                }
            }

        }
    }
}
