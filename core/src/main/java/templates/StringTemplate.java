package templates;

import core.Node;
import core.User;
import core.attribute.Attribute;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class StringTemplate extends Template {

	static {
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public StringTemplate(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public StringTemplate(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
	 *
	 * @param n
	 * @param locale
	 * @return
	 */
	@Override
	public String fill(Node n, Locale locale) {
		String s = getValue();
		Pattern pattern = Pattern.compile("\\{\\{(.*?)\\}\\}");
		Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			String group = matcher.group();
			group = group.substring(2, group.length() - 2);
			Attribute attr = getAttribute(group);
			if (attr != null) {
				s = s.replaceAll(Pattern.quote("{{" + group + "}}"), attr.toHTML(locale));
			}
		}

		return s;
	}

	public String getUnfilled(Node n){
		return getValue();
	}
	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		return false;
	}

	/**
	 *
	 * @param u
	 */
	@Override
	public void onCreate(User u) {
		addAttr("value", u);
	}

	/**
	 *
	 * @param v
	 * @return
	 */
	public Node setValue(String v) {
		doc.put("value", v);
		return this;
	}

}
