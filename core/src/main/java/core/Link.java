package core;

import core.controller.Controller;
import core.id.ObjectID;
import java.util.Locale;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Link extends Node {

    /**
     *
     * @param c
	 * @param doc
     */
    public Link(Controller c,Document doc) {
		super(c,doc);
	}

    @Override
    public String getFrontLink(Locale lang) {
	return getTarget().getFrontLink(lang);
    }

    @Override
    public String getBackLink(Locale lang) {
	Node cat = getTarget();

	if (cat == null) {
	    return super.getBackLink(lang) + " => undefined";
	} else {
	    return super.getBackLink(lang) + " => " + cat.getBackLink(lang);
	}
    }

    /**
     *
     * @return @throws NumberFormatException
     */
    public Node getTarget() throws NumberFormatException {
	return c.getNode(new ObjectID(doc.get("target")));
    }

    @Override
    public Node getChild(String name) {
	return getTarget().getChild(name);
    }

}
