package types;

import core.Node;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Types extends Node {

	static {
		addChildType(Types.class, Primitive.class);
		addChildType(Types.class, Collection.class);
		addChildType(Types.class, Map.class);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Types(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Types(ObjectID userID, Controller con) {
		super(userID, con);
	}

	public Primitive getType(String name) {
		Node type = getChild(name);
		if (type instanceof Primitive) {
			return (Primitive) type;
		}
		return null;
	}
}
