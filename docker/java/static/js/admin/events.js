function onError(evt) {
	console.log(evt);
	ws.close();
}

function onClose(evt) {
	console.log(evt);
	startws();
}

function onNodeCreated(node){
  var cc=$("[data-id="+node.parent+"] .children");
  cc.each(function(){
    var that=$(this);
	if(node.type!=that.attr("data-type"))return;

    var p=that;
    var pID=undefined;
    while(!pID){
    	p=p.parent();
      	pID=p.attr("data-id");
    }
    if(pID!=node.parent)return;
    
    getTemplate(node.ID,function(t){
      t=$(t);
      that.append(t);
      fillTemplate(t);
    });
  });
	var htmlnode=$(node.htmlrow);
	switch (node.type) {
		case "shop.Attribute":
			$(".attr_" + node.name).remove();
			$("#attrs_" + node.parent + " tr:first")
				.after(htmlnode);
			break;
        case "core.Template":
          $("#templates table.children tbody").append(
            htmlnode);
          break;
        case "core.Picture":
          $("#gallery table.children tbody").append(
            htmlnode);
          break;
        default:
          	if (node["type"].startsWith("core.attribute")) {} else
			{
				htmlnode.hide();
            	$("[data-id="+node.parent+"] .mychildren").append(htmlnode);
				htmlnode.slideDown();
			}
	}
}

function onNodeUpdated(node){
    $(".node_" + node.ID).effect("highlight");
    $("[data-id=" + node.ID+"]").effect("highlight");

    if (node.ID == catID) {
      alert("updated");
      if (typeof updated !== 'undefined')
        updated = true;
    }
}

function onNodeDeleted(node){
	$(".node_" + node.ID).remove();
  	$("[data-id=" + node.ID+"]").remove();
}



function processChild(e){
	var ws=e.currentTarget;
	try{
		var event = JSON.parse(e.data);
	}
	catch(err){
		console.log("parse error:"+e.data);
		return;
	}
	var node = event.node;
  	switch (event.cmd)
	{
        case "first":
			ws.send(JSON.stringify({
				cmd: "next"
			}));
		case "entry":
			if (node){
				var node_html=$(node.htmlrow);
				$(".mychildren").append(node_html);
				node_html.hide();

				node_html.slideDown();
			}
			if(typeof ws.nextCallback=="function")
				ws.nextCallback(ws);
			
			break;
	}
}

function processMyTemplate(e){
	var ws=e.currentTarget;
	try{
		var event = JSON.parse(e.data);
	}
	catch(err){
		console.log("parse error:"+e.data);
		return;
	}
	var node = event.node;
  	switch (event.cmd)
	{
        case "first":
			ws.send(JSON.stringify({
				cmd: "next"
			}));
		case "entry":
			if (node){
				var node_html=$(node.htmlrow);
				$("#templates").append(node_html);
				node_html.hide();

				node_html.slideDown();
			}
			ws.send(JSON.stringify({
				cmd : "next"
			}));
			
			break;
	}
}


function processPicture(e){}



function processComment(e){
	var ws=e.currentTarget;
	var event = JSON.parse(e.data);
	var node = event.node;
  	switch (event.cmd)
	{
        case "first":
			ws.send(JSON.stringify({
				cmd: "next"
			}));
		case "entry":
			
			if (node)
				$("#comments").append(node.htmlrow);
			if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100))
				ws.send(JSON.stringify({
					cmd: "next"
				}));
			break;
	}
}


function onMessage(e) {
  var ws=e.currentTarget;
	var event = JSON.parse(e.data);
	var node = event.node;
	switch (event.cmd) {
		case "event":
        	var ee=$("#lastevent");
			if (event.user) {
				if (event.user.image)
                  ee.find("img").attr("src",event.user.image);
              	else
                  ee.find("img").attr("src","/media/nobody.svg");
			}
			if (event.username)
            	ee.find(".username").html(event.username);
        
            ee.find(".event").html(event.value);
        	if(node)
              if(node["type"]=="core.Comment"){
              	ee.find(".event").html(event.value);
              }
			var diff_element=ee.find(".diff");
            diff_element.empty();
			if (event.type == "UPDATED")
				if (JsDiff && event.previousvalue && event.newvalue) {
                  	
                  	var t1=event.previousvalue.replace(/</g, "<").replace(/>/g, ">");
                  var t2=event.newvalue.replace(/</g, "<").replace(/>/g, ">");
					var changes = JsDiff["diffLines"](t1, t2);
					var l = changes.length;
					for (var i = 0; i < l; i++) {
						var change = changes[i];
						if (change.added)
							diff_element.append("<div class='added'>+ " + change.value + "</div>");
						if (change.removed)
							diff_element.append("<div class='removed'>- " + change.value + "</div>");
					}
				}
			//$("#lastevent").html(html);
			$("#lastevent").show();
			setTimeout(function() {
				$("#lastevent").fadeOut(5000);
			}, 5000);
			if (node.path.startsWith(path)) {
				var ID = node.path.replace(path, "").split("/")[1];
				$(".node_" + ID).effect("highlight");
			}
			switch (event.type) {
				case "DELETED":
                	onNodeDeleted(node);
					break;
				case "CREATED":
                	onNodeCreated(node);
					
					break;
				case "UPDATED":
					onNodeUpdated(node);
					break;
			}
		case "first":
			ws.send(JSON.stringify({
				cmd: "next"
			}));
		case "entry":
			switch (event.callback) {
				case "child":
					if (node)
						switch (node["TYPE"]) {
							case "core.Template":
								$("#templates table.children tbody").append(
									node.htmlrow);
								break;
							case "core.Picture":
								$("#gallery table.children tbody").append(
									node.htmlrow);
								break;
							default:
								if (node["TYPE"].startsWith("core.attribute")) {} else
									$("#" + node.parent).append(node.htmlrow);
						}
			}
			if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100))
				ws.send(JSON.stringify({
					cmd: "next"
				}));
			break;
	}
	//$("#container_"+node.parent).hide();
	//alert(node.htmlrow);
}
var ws;
var wsUri = "ws://" + document.location.host + "/events";
if (location.protocol == 'https:')
	wsUri = "wss://" + document.location.host + "/events";
function startws() {
	ws = new WebSocket(wsUri);
	ws.onerror = onError;
	ws.onclose = onClose;
	ws.onmessage = onMessage;
}
startws();

function loadMore() {
	ws.send(JSON.stringify({
		cmd: "next"
	}));
}

//$(window).scrol( function() {
	//if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100)) {
		//loadMore();
	//}
//});