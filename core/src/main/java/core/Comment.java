package core;

import core.controller.Controller;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Comment extends Node {
	static{
		addChildType(Comment.class,Comment.class);
	}

    /**
     *
     * @param c
	 * @param doc
     */
    public Comment(Controller c,Document doc) {
		super(c,doc);
	}
	
    /**
     *
     * @return
     */
    @Override
	public boolean canInherit() {
		return false;
	}
}
