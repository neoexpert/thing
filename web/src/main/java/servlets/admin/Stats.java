package servlets.admin;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servlets.EventSender;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/admin/stats")
public class Stats extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// Controller c = Controller.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Stats() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long MEGABYTE = 1024L * 1024L;

    /**
     *
     * @param bytes
     * @return
     */
    public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		// User u = c.getUser(session);
		// request.setAttribute("username", u.getName());
		// if (!u.isLoggedIn("admin")) {
		// RequestDispatcher rd =
		// request.getRequestDispatcher("/WEB-INF/login.jsp");

		// rd.forward(request, response);
		// Index i = new Index();
		// i.doGet(request, response);
		// String uri = c.getFullURL(request);
		// session.setAttribute("redirectTO", uri);
		// return;
		// }
		response.setContentType("text/html;charset=utf-8");

		response.getWriter().append("WebSocketClients: " + EventSender.getClientsCount() + "<br>\n");
		Runtime runtime = Runtime.getRuntime();
		// Run the garbage collector
		if (request.getParameter("gc") != null)
			runtime.gc();
		// Calculate the used memory
		long memory = runtime.totalMemory() - runtime.freeMemory();
		response.getWriter().append("Used memory is bytes: " + memory + "<br>\n");
		response.getWriter().append("Used memory is megabytes: " + bytesToMegabytes(memory) + "<br>\n");
		response.getWriter().append("ManagementFactory.getThreadMXBean().getThreadCount(): "
				+ ManagementFactory.getThreadMXBean().getThreadCount() + "<br>\n");
		response.getWriter().append("Thread.activeCount(): " + Thread.activeCount() + "<br>\n");
		//response.getWriter().append(Tools.dump(request, 0));
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
