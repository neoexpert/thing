package servlets;

import com.neoexpert.Log;
import core.controller.ControllerBuilder;
import web.*;
import core.id.ObjectID;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Picture
 */
@WebServlet("/picture")
public class Picture extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Picture() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}

		String ID = request.getParameter("ID");

		core.Picture_OLD pic = null;
		ObjectID id = new ObjectID(ID);
		try {
			pic = (core.Picture_OLD)c.getNode(id);
		} catch (Exception e) {
			response.getWriter().append(e.getMessage());
			Log.error(e);

			return;
		} finally {
			c.close();
                        

		}
		

		File f = new File(ControllerBuilder.getRootPath(request.getServerName()) + "/media/" + pic.getParentID() + "/"
				+ pic.getName());
		
		String content_type = getServletContext().getMimeType(f.getAbsolutePath());
		response.setContentType(content_type);
		
		switch (content_type) {
		case "image/jpeg":

			OutputStream out = response.getOutputStream();

			String w = request.getParameter("w");
			if (w != null) {
				BufferedImage sourceImage;
				try {
					sourceImage = ImageIO.read(f);
				} catch (IIOException e) {
					response.setStatus(404);
					return;
				}
				if (sourceImage == null) {
					c.close();
					return;
				}

				int width = Integer.parseInt(w);
				File thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
				if (!thumb.exists()) {
					if (width < sourceImage.getWidth()) {
						Image thumbnail = sourceImage.getScaledInstance(width, -1, Image.SCALE_SMOOTH);
						BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
								thumbnail.getHeight(null), BufferedImage.TYPE_INT_RGB);
						bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);

						ImageIO.write(bufferedThumbnail, "jpeg", thumb);
						ImageIO.write(bufferedThumbnail, "jpeg", out);

						c.close();
						thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
						// return;
					}
					else thumb=f;

				}
				
				Files.copy(thumb.toPath(), out);
				out.close();

				return;

			}
			Files.copy(f.toPath(), out);

			//ImageIO.write(sourceImage, "jpeg", out);

			out.close();
			c.close();
			break;
		case "image/png":

			 out = response.getOutputStream();

			 w = request.getParameter("w");
			if (w != null) {
				BufferedImage sourceImage;
				try {
					sourceImage = ImageIO.read(f);
				} catch (IIOException e) {
					response.setStatus(404);
					return;
				}
				if (sourceImage == null) {
					c.close();
					return;
				}

				int width = Integer.valueOf(w);
				File thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
				if (!thumb.exists()) {
					if (width < sourceImage.getWidth()) {
						Image thumbnail = sourceImage.getScaledInstance(width, -1, Image.SCALE_SMOOTH);
						BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
								thumbnail.getHeight(null), BufferedImage.TYPE_INT_ARGB);
						bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);

						ImageIO.write(bufferedThumbnail, "png", thumb);
						ImageIO.write(bufferedThumbnail, "png", out);

						c.close();
						thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
						// return;
					}
					else thumb=f;

				}
				
				Files.copy(thumb.toPath(), out);
				out.close();

				return;

			}
			Files.copy(f.toPath(), out);

			//ImageIO.write(sourceImage, "jpeg", out);

			out.close();
			c.close();
			break;
		case "image/gif":

			 out = response.getOutputStream();

			 w = request.getParameter("w");
			if (w != null) {
				BufferedImage sourceImage;
				try {
					sourceImage = ImageIO.read(f);
				} catch (IIOException e) {
					response.setStatus(404);
					return;
				}
				if (sourceImage == null) {
					c.close();
					return;
				}

				int width = Integer.valueOf(w);
				File thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
				if (!thumb.exists()) {
					if (width < sourceImage.getWidth()) {
						Image thumbnail = sourceImage.getScaledInstance(width, -1, Image.SCALE_SMOOTH);
						BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
								thumbnail.getHeight(null), BufferedImage.TYPE_INT_ARGB);
						bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);

						ImageIO.write(bufferedThumbnail, "gif", thumb);
						ImageIO.write(bufferedThumbnail, "gif", out);

						c.close();
						thumb = new File(f.getParent() + "/_" + width + "_" + f.getName());
						// return;
					}
					else thumb=f;

				}
				
				Files.copy(thumb.toPath(), out);
				out.close();

				return;

			}
			Files.copy(f.toPath(), out);

			//ImageIO.write(sourceImage, "jpeg", out);

			out.close();
			c.close();
			break;

		default:
			Files.copy(f.toPath(), response.getOutputStream());

		}

	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

    /**
     *
     * @param req
     * @return
     */
    @Override
	protected long getLastModified(HttpServletRequest req) {
		return super.getLastModified(req);
	}
}
