package shop;


import core.Node;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Supplier extends Node {
	
    /**
     *
     * @param c
	 * @param doc
     */
    public Supplier(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @param id
     * @param c
     */
    public Supplier(ObjectID id, Controller c) {
		super(id,c);
	}
}
