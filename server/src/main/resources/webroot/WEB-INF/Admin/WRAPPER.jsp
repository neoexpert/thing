<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<%@ page import="core.*"%>
<%@ page import="web.*"%>
<%@ page import="core.attribute.*"%>

<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="core.controller.*"%>
<%@ page import="templates.*"%>

<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%>

<h2 class="path">
	<%
		Node n = (Node) request.getAttribute("node");
		Locale lang = (Locale) request.getAttribute("locale");
		HttpController c = (HttpController) request.getAttribute("controller");

		PageInfo pi = c.getPathHTML(n, lang);
		request.setAttribute("rootCategoryID", pi.getRootNodeID());
		out.println(pi.getPath());
	%>
</h2>
<script>
	var canContain = {}
<%
java.util.Set<Class<?>> cis = n.canContain();
			if (cis != null)
				for (Class<?> ci : cis) {
					String name = ci.getSimpleName().toUpperCase();
					out.println("canContain['" + name + "']='" + ci.getName() + "';");
				}
			out.println("var lang='" + lang.getLanguage() + "';");
			out.println("var ID='" + n.getID() + "';");
			out.println("var path='" + n.getPath() + "';");


			%>
</script>
<div id="node_wrapper" data-id="<%=n.getID()%>">
	<%
		HtmlTemplate tt = (HtmlTemplate)n.getAdminTemplate(null);
		if (request.getParameter("template") != null) {
			String template = request.getParameter("template");
			if (!"default".equals(template))
				tt = (HtmlTemplate)n.getTemplate(template);
			else tt=null;
		}

		if (tt == null){
			ServletContext context = getServletContext();
			InputStream is=HttpController.getWebRootResourceUri(this,"/default.html").toURL().openStream();
				try (java.io.BufferedReader br = new java.io.BufferedReader(new InputStreamReader(is, "UTF-8"))) {
				    String line;
				    while ((line = br.readLine()) != null) {
				    	out.println(line);
				    }
				}
				
			//}
		}
		else
			out.print(tt.getValue());
	%>
</div>
<script>
	fillTemplate($("#node_wrapper"));
</script>

