package core;

import core.controller.Controller;
import core.id.ObjectID;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.bson.Document;

/**
 * @author neoexpert
 *
 */
public class Picture_OLD extends Node {

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Picture_OLD(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Picture_OLD(ObjectID userID, Controller con) {
		super(userID, con);
	}

	@Override
	public String toHTMLROW(Locale locale) {
		String type = getClass().getSimpleName().toLowerCase(Locale.ENGLISH);
		String out;
		out = ("<div class='table_row " + type + " node_" + getID() + "' id='" + getID() + "'>");

		out += ("<div class='table_cell'>");
		out += (getID());
		out += ("</div>");

		out += ("<div class='table_cell'>");
		out += (getClass().getSimpleName());
		out += ("</div>");

		out += ("<div class='table_cell'>");
		out += ("<img src='/picture?ID=" + getID() + "&w=300'><br>");

		out += getBackLink(locale);

		out += ("</div>");

		out += ("<div class='table_cell narrowColumn'>");
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy hh:mm", locale);

		// DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM,
		// locale);
		String formattedDate = df.format(new Date(getUpdated()));
		out += formattedDate;
		out += " ";

		out += "(";
		out += getAge(locale);
		out += ")";
		out += ("</div>");
		out += "<div class='table_cell'>";
		out += flagsToHtml();
		out += "</div>";

		out += ("<div class='table_cell narrowColumn'>");
		out += ("<button value='" + getID() + "' onclick='deleteChild(this)'>delete</button>");
		// out.println("<a href='/admin/category?ID=" + id + "'>edit</a>");

		// out.println("<form action='/admin/product'>");
		// out.println("<input type='hidden' name='ID' value='" + id + "'/>");
		// out.println("<input type='submit' value='edit'/>");
		// out.println("</form>");
		out += ("</div>");
		out += ("</div>");
		return out;
	}

}
