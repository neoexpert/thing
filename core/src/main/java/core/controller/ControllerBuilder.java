package core.controller;

import core.controller.mongo.MongoController2;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import core.User;
import java.util.Locale;
//import javax.servlet.http.HttpServletRequest;
import org.slf4j.*;
import ch.qos.logback.classic.*;

//import javax.servlet.ServletContext;

/**
 *
 * @author neoexpert
 */
public class ControllerBuilder {

	static ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
			.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

	static {
		root.setLevel(Level.WARN);
	}
	protected static MongoClient mongo;
	// private static MongoClient mongo;
	protected static String MONGO_HOST;
	protected static String MONGO_PORT;
	protected static String MONGO_USER;
	protected static String MONGO_PW;
	protected static String AUTH_DB;
	protected static String connection_string;
	protected static String STATIC_FILES;
	public static String MONGO_DB;
	public static String MONGO_NODES = "nodes";
	
	static {
		MONGO_DB = System.getenv("MONGO_DB");
		if (MONGO_DB == null) {
			MONGO_DB = "thing";
		}
		MONGO_NODES = System.getenv("MONGO_NODES");
		if (MONGO_NODES == null) {
			MONGO_NODES = "nodes";
		}
	}

	static {
		MONGO_HOST = System.getenv("MONGO_HOST");
		if (MONGO_HOST == null) {
			MONGO_HOST = "localhost";
		}
		MONGO_PORT = System.getenv("MONGO_PORT");
		if (MONGO_PORT == null) {
			MONGO_PORT = "27017";
		}
		MONGO_USER = System.getenv("MONGO_USER");
		MONGO_PW = System.getenv("MONGO_PW");
		AUTH_DB = System.getenv("AUTH_DB");
		if (AUTH_DB == null) {
			AUTH_DB = "admin";
		}

		STATIC_FILES = System.getenv("STATIC_FILES");
		if (STATIC_FILES == null) {
			STATIC_FILES = "/var/www/thing/";
		}

	}
	private String domain;
	private Locale locale;

	private static void initMongoConnection() {
		if (MONGO_USER != null) {
			connection_string = "mongodb://" + MONGO_USER + ":" + MONGO_PW + "@" + MONGO_HOST + ":" + MONGO_PORT + "/" + AUTH_DB;
		} else {
			connection_string = "mongodb://" + MONGO_HOST + ":" + MONGO_PORT;
		}

		mongo = new MongoClient(new MongoClientURI(connection_string));
	}

	/*
	private static void initMongoConnection(ServletContext ctx) {

		String _MONGO_USER = ctx.getInitParameter("MONGO_USER");
		if(_MONGO_USER!=null)
			MONGO_USER=_MONGO_USER;
		String _MONGO_PW = ctx.getInitParameter("MONGO_PW");
		if(_MONGO_PW!=null)
			MONGO_PW=_MONGO_PW;
		String _MONGO_HOST = ctx.getInitParameter("MONGO_HOST");
		if(_MONGO_HOST!=null)
			MONGO_HOST=_MONGO_HOST;
		String _MONGO_PORT = ctx.getInitParameter("MONGO_HOST");
		if(_MONGO_PORT!=null)
			MONGO_PORT=_MONGO_PORT;
		String _MONGO_AUTH_DB = ctx.getInitParameter("MONGO_AUTH_DB");
		if(_MONGO_AUTH_DB!=null)
			AUTH_DB=_MONGO_AUTH_DB;
		String _MONGO_DB = ctx.getInitParameter("MONGO_DB");
		if(_MONGO_DB!=null)
			MONGO_DB=_MONGO_DB;
		if (MONGO_USER != null) {
			connection_string = "mongodb://" + MONGO_USER + ":" + MONGO_PW + "@" + MONGO_HOST + ":" + MONGO_PORT + "/" + AUTH_DB;
		} else {
			connection_string = "mongodb://" + MONGO_HOST + ":" + MONGO_PORT;
		}
		Log.log(connection_string);	
		mongo = new MongoClient(new MongoClientURI(connection_string));
	}*/

	/**
	 *
	 * @param request
	 * @param ctx
	 * @throws Exception
	 */
	/*
	public ControllerBuilder(HttpServletRequest request) throws Exception {
		locale = HttpController.getLang(request);

		this.domain = request.getServerName();
		if (null == mongo) {
			initMongoConnection(request.getServletContext());
		}

		build();
	}
	*/

	public ControllerBuilder() {
		if (null == mongo) {
			initMongoConnection();
		}
		build();
	}

	/**
	 *
	 * @param from
	 * @param to
	 * @param u
	 * @throws Exception
	 */
	public void copyDB(Controller from, Controller to, User u) throws Exception {
		to.getRoot().addAllChildrenFrom(from.getRoot(), u);
	}

	// public MyController(Connection con, HttpServletRequest request, long ts)
	// throws Exception {
	// UserController uc = new SQLUserController(con, request);
	// c = new SQLArchiveController(con, request,
	// uc,Controller.getLang(request), ts);
	// }
	/**
	 *
	 * @param domain
	 * @param root
	 * @param locale
	 * @throws Exception
	 */
	/*
	public ControllerBuilder(String domain, boolean root, Locale locale, ServletContext ctx) throws Exception {
		this.domain = domain;
		this.locale = locale;
		if (null == mongo) {
			initMongoConnection(ctx);
		}

		build();
	}
	*/

	/**
	 *
	 * @param ctx
	 */
	/*
	public ControllerBuilder(ServletContext ctx) {
		if (null == mongo) {
			initMongoConnection(ctx);
		}

		build();
	}
	*/

	// public MyController(Connection con, String domain, boolean root,String
	// lang, long ts) throws Exception {
	// UserController uc = new SQLUserController(con, domain, root);
	// c = new SQLArchiveController(con, domain, root, uc,lang, ts);
	// }
	//private HttpController hc;

	/**
	 *
	 * @param domain
	 * @return
	 */
	public static String getRootPath(String domain) {

		return STATIC_FILES + domain;
	}

	/**
	 *
	 * @param request
	 * @return
	 */
	/*
	public static String getFullURL(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}
*/
	/**
	 *
	 * @return
	 */
	/*public HttpController getHttpController() {
		return hc;
	}*/

	public Controller getController() {
		return new MongoController2(mongo, domain, locale);
	}

	/**
	 *
	 * @return
	 */
	public Controller build() {
		//hc = new HttpController(mongo, domain, locale);
		return null;
	}

	/**
	 *
	 * @param domain
	 * @return
	 */
	public ControllerBuilder setDomain(String domain) {
		this.domain = domain;
		return this;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	public ControllerBuilder setLocale(Locale locale) {
		this.locale = locale;
		return this;
	}

	/**
	 *
	 * @param string
	 * @return
	 */
	public ControllerBuilder setDB(String string) {
		return this;
	}
}
