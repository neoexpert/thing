# thing

is a tool for managing things in thing called `Nodes`.

## Prerequisites

To run thing you need the follogwing additional software:

* docker-compose

## Installation

```
git clone https://github.com/neo-expert/thing/
cd thing
./install.sh
```

## Database organisation

The database is organized hierarchically (like a filesystem). The simplest data record is a `Node`. A `Node` may have any number of Attributes. Some attribute names are used to define a `Node` and may not be used. These are:

* `_id` - an unique id of the `Node`
* `name` - an unique name within the parent `Node` (like filename)
* `parent` - `_id` of the parent node
* `type` - type of the `Node`.
* `cid` - unique id within the parent `Node` (increments every time a child `Node` is added)
* `last_cid` - used to save the last `cid`
* `created` - timestamp
* `updated` - timestamp
* `owner` - `_id` of the owner
* `involved` - list of _id's from each user that has edited the `Node`
* `group` user group that may update that `Node`

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing


## Authors

## License



