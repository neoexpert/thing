/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.id;

import org.bson.types.ObjectId;

/**
 *
 * @author neoexpert
 */
public class MongoObjectID extends ObjectID {

	public MongoObjectID(String oid) {
		super(oid);
		try {
			id = new ObjectId(oid);
		} catch (Exception e) {
		}
	}
}
