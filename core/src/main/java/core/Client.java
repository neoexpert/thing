/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import com.neoexpert.Log;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.events.Event;
import core.events.EventListener;
import java.io.IOException;
import java.util.Locale;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
/**
 *
 */
public abstract class Client implements EventListener {

    NodeIterator<Node> it;
    Controller c;
    private final Locale lang;
    private User u;
    private int version;

    /**
     *
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     *
     * @param c
     * @param u
     * @param lang
     */
    public Client(Controller c, User u, Locale lang) {
        this.c = c;
        this.u = u;
        this.lang = lang;
    }
    public void send(String text) throws IOException {}

    public abstract String getSessionID();

    /**
     *
     * @return
     */
    public Controller getController() {
        return c;
    }


    /**
     *
     * @return
     */
    public JSONObject next() {
        JSONObject jo = new JSONObject();
        jo.put("cmd", "entry");
		if(!it.hasNext())
		{
        	jo.put("hasnext", false);
			Log.log(u,"iterator: hasnext is false for "+u.getName());
			return jo;
		}

        Node n = it.next();


        JSONObject njo = new JSONObject();
        Set<String> attrs = it.getAttrs();
        if (attrs != null) {
            for (String key : attrs) {
                njo.put(key, n.doc.get(key));
            }
        }

        njo.put("name", n.getName());
        njo.put("_id", n.getID().get());
        njo.put("parent", n.getParentID().get());
        User o = n.getOwner();
        if (o != null) {
            Object nick = o.doc.get("nick");
            if (nick == null) {
                njo.put("owner_name", o.getName());
            } else {
                njo.put("owner_name", nick.toString());
            }
            njo.put("owner_groups", o.getGroups());
        }
        String templatename = it.getTemplateName();
        if (templatename != null) {
            switch (templatename) {
                case "htmlrow":
                    njo.put("template", n.toHTMLROW(lang));
                    break;
                case "default":
                    njo.put("template", n.getTemplate().getValue());
                    break;
                default:
                    njo.put("template", n.getTemplate(templatename).getValue());
                    break;

            }
        }

        jo.put("hasnext", it.hasNext());

        jo.put("node", njo);

        return jo;
    }

    /**
     *
     * @param it
     */
    public void setIterator(NodeIterator<Node> it) {
        this.it = it;
    }



    /**
     *
     * @param u
     */
    public void setUser(User u) {
        this.u = u;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return u;
    }

    /**
     *
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public void onEvent(Event e) {

    }
    public void close() throws IOException{
        c.close();
    }
}
