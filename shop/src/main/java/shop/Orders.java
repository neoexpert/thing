package shop;


import core.Domain;
import core.Node;
import core.controller.Controller;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Orders extends Node {
	static{
		addChildType(Orders.class,Order.class);
		addChildType(Domain.class, Orders.class);
	}

    /**
     *
     * @param c
	 * @param doc
     */
    public Orders(Controller c,Document doc) {
		super(c,doc);
	}
	
}
