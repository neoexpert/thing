package shop;

import core.User;
import core.Users;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Customer extends User {

	static {
		addChildType(Users.class, Customer.class);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Customer(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Customer(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @return @throws Exception
	 */
	public Cart getCart() throws Exception {
		return (Cart) getChild("CART");
	}


}
