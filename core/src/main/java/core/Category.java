package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Category extends Node {

	static {
		addChildType(Category.class, Category.class);
		addChildType(Category.class, Site.class);
		addChildType(Category.class, File.class);

	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Category(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param i
	 * @param c
	 */
	public Category(ObjectID i, Controller c) {
		super(i, c);
	}
}
