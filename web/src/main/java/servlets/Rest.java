package servlets;

import com.neoexpert.Log;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import core.processor.Processor;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NodeREST
 */
@WebServlet("/rest")
public class Rest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			
			Log.error(e);
			return;
		}
		response.setContentType("text/html");

		response.setCharacterEncoding("UTF-8");
		Locale lang = HttpController.getLang(request);
		User u=c.getUser(request.getSession());
		
		HttpParams hp = new HttpParams(request,response);

		Processor.process(hp,c,u,lang);
	}

}
