<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="core.*"%>
<%@ page import="web.*"%>
<%@ page import="templates.*"%>

<%@ page import="java.util.*"%>
<%@ page import="core.controller.*"%>
<%@ page trimDirectiveWhitespaces="true"%>

<%
	response.setCharacterEncoding("UTF-8");

	Node n = (Node) request.getAttribute("node");
	request.setAttribute("entity", n.getClass().getSimpleName());
	HttpController c = (HttpController) request.getAttribute("controller");

	Locale locale = (Locale) request.getAttribute("locale");

	PageInfo pi = c.getFrontPageInfo(n, locale);
	request.setAttribute("rootCategoryID", pi.getRootNodeID());
	HtmlTemplate htmltemplate = (HtmlTemplate) n.getTemplate();

	String template = null;
	boolean dynamic_template = false;
	if (htmltemplate != null) {
		dynamic_template = htmltemplate.isDynamic();
		if (dynamic_template) {
			template = htmltemplate.getValue();
		} else {
			template = n.getFilledTemplate(htmltemplate, locale);
		}
	}

%>
<div id="wrapper" data-id="<%=n.getID()%>">

	<script>

		<%out.print("var ID='" + n.getID() + "';");%>

	</script>
	<div class="wrapper" id="wrapper_<%=n.getName()%>">


		<%
			if (template != null) {
				out.print(template);
			} else {
				out.print("template not found");
			}
		%>

	</div>

</div>
<script>
	<%
		if (dynamic_template) {
			out.println("fillTemplate($(\"#wrapper\"));");
		} else {
			out.println("initTemplate($(\"#wrapper\"));");
		}

	%>
</script>
