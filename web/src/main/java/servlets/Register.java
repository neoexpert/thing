package servlets;

import com.neoexpert.Log;
import core.Address;
import core.Addresses;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddUser
 */
@WebServlet("/reg")
public class Register extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");

        rd.forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            out.append(e.toString());
            Log.error(e);
            return;
        }

        // String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (password == null) {
            out.append("wrong password");
            return;
        }
        String email = request.getParameter("email");
        if (email == null) {
            out.append("wrong email");
            return;
        }
        User u;

        try {
            u = c.addUser(email, password);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.error(e);
            out.append(e.toString());
            return;
        }
        try {
            Addresses addresses = (Addresses) u.getChild("ADDRESSES");
            Address a = new Address(u.getID(), c);
            a.setName(UUID.randomUUID().toString());

            a = (Address) addresses.addChild(a, u);

            Map<String, String[]> attrs = new HashMap<>(request.getParameterMap());
            attrs.remove(password);
            attrs.remove(email);

            Set<Map.Entry<String, String[]>> keys = attrs.entrySet();
            for (Map.Entry<String, String[]> entry : keys) {
                a.setAttribute(entry.getKey(), entry.getValue()[0]).commit(u);
                u.setAttribute("billing_" + entry.getKey(), entry.getValue()[0]).commit(u);
				u.setAttribute("shipping_" + entry.getKey(), entry.getValue()[0]).commit(u);


            }
            // u.setAttributes(u,null, myreq, null);
        } catch (Exception e) {
            Log.error(e);
            out.append(e.toString());

            return;
        }
        String uuid = UUID.randomUUID().toString();

        //Locale lang = HttpController.getLang(request);
        
        u.setAttribute("email_confirmed", false)
                .setAttribute("email_confirmation_uuid", uuid)
                .commit(u);
        String servername=c.getSesstings().getServerName();
        if(servername==null)
            servername=request.getServerName();
        StringBuilder uri = new StringBuilder();
        uri.append(request.getScheme())
                .append("://")
                .append(servername)
                .append(":")
                .append(request.getServerPort())
                .append("/confirm_email")
                .append("?")
                .append("email=")
                .append(u.getEmail())
                .append("&uuid=")
                .append(uuid);

        String emailcontent = "Vielen Dank für Ihre Registrierung. Bitte bestätigen Sie Ihre Registrierung über den folgenden Link "
                + uri
                + " oder kopieren Sie den Link in die Adresszeile Ihres Browsers. Der Link bleibt 24 Std. gültig.";
        

        try {
            u.sendMail("Registration", emailcontent, c.getMailSettings());
        } catch (Exception e) {
            Log.error(e);
            out.print(e.toString());
        }

        c.close();
        out.print("ok");

    }

}
