package shop;

import core.Category;
import core.Node;
import static core.Node.addChildType;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Product extends Node {

	static {
		addChildType(Category.class, Product.class);

	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Product(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public Product(ObjectID userID, Controller c) {
		super(userID, c);
	}
}
