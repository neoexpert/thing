package servlets;

import com.neoexpert.Log;
import core.Node;
import core.SendEmail;
import core.User;
import web.*;
import core.controller.NodeIterator;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import shop.Cart;
import shop.Item;
import shop.Order;
import templates.HtmlTemplate;

/**
 * Servlet implementation class CategoryView
 */
@WebServlet("/checkout")
public class Checkout extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Checkout() {
		super();
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		response.setContentType("text/html");
		RequestDispatcher rd;
		// rd= request.getRequestDispatcher("/WEB-INF/Checkout.jsp");
		try {
			Node cat = c.getNode("CATEGORIES");
			cat = cat.getChild("checkout");
			// cat.setID(cat.getID());
			request.setAttribute("ID", cat.getID());
			request.setAttribute("entity", "SITE");
			String wrapper = request.getParameter("wrapper");
			if (wrapper == null) {
				rd = request.getRequestDispatcher("/WEB-INF/ControlBar.jsp");
			} else {
				rd = request.getRequestDispatcher("/WEB-INF/Site.jsp");
			}

			rd.forward(request, response);
			return;
		} catch (ServletException | IOException e) {
			response.getWriter().append(e.toString());
			Log.error(e);
		}
		// rd.forward(request, response);
		c.close();
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		User u = c.getUser(session);
		// if (!u.isLoggedIn("USER") && !u.isLoggedIn("admin")) {
		// RequestDispatcher rd =
		// request.getRequestDispatcher("/WEB-INF/login.jsp");

		// rd.forward(request, response);
		// response.sendRedirect("/login");
		// session.setAttribute("redirectTO", "/admin/product");
		// return;
		// }
		Locale lang = HttpController.getLang(request);
		try {
			Node orders = c.getNode("ORDERS");

			Order order = new Order(u.getID(), c);
			order.setName("ORDER " + UUID.randomUUID());
			try {
				order = (Order) orders.addChild(order, u);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.error(e);

			}
			Cart cart = (Cart) u.getChild("CART");
			c.moveNode(cart, order, u);
			// order.setCart(cart,customer);
			order.setCustomer(u).commit(u);

			Iterator<Item> items = cart.getItems();
			ArrayList<String> filesb64 = new ArrayList<>();
			while (items.hasNext()) {
				Item item = items.next();
				Object file = item.doc.get("img_upload");
				if (file instanceof String && file != null) {
					filesb64.add((String) file);
				}
			}

			/*
			 * HashMap<String, Attribute> attrs; try { attrs =
			 * order.getAttributesWithTypes(true); } catch (Exception e1) { //
			 * TODO Auto-generated catch block e1.printStackTrace(); return; }
			 * 
			 * for (Category a : attrs.values()) { String value =
			 * request.getParameter(a.getName()); if (value != null) { try {
			 * order.setAttribute(a.getName(), value,null); } catch
			 * (SQLException e) { // TODO Auto-generated catch block
			 * Log.error(e); } catch (Exception e) { // TODO
			 * Auto-generated catch block Log.error(e); } } }
			 */
			// String mail = order.getFilledTemplate("MAIL",lang);
			// String subject = order.getFilledTemplate("MAIL_SUBJECT",lang);
			String mail = ((HtmlTemplate) order.getTemplate("MAIL")).fill(order, lang);
			String subject = order.getFilledNTemplate("MAIL_SUBJECT", lang);

			if (filesb64.isEmpty()) {
				u.sendMail(subject, mail, c.getMailSettings());

			} else {
				u.sendMail(subject, mail, c.getMailSettings(),filesb64);
			}

			mail = ((HtmlTemplate) order.getTemplate("ADMIN_MAIL")).fill(order, lang);
			subject = order.getFilledNTemplate("ADMIN_MAIL_SUBJECT", lang);

			NodeIterator<Node> it = c.getNode("SETTINGS").getChild("MAILS").getChildren().build();
			SendEmail sm = new SendEmail(c.getMailSettings());
			while (it.hasNext()) {
				String email = it.next().getName();
				if (validate(email)) {
					if (filesb64.isEmpty()) {
						sm.sendEmail(email, subject, mail);
					} else {
						sm.sendEmail(email, subject, mail, filesb64);
					}
				} else {
					throw new RuntimeException("mail adress validation failed");
				}
			}
			/*
			SendEmail.sendEmail("neoexpert@gmail.com", subject, mail);
			SendEmail.sendEmail("wundermuli@googlemail.com", subject, mail);
			SendEmail.sendEmail("ulf@fischbeck.info", subject, mail);
			 */

			order.processSuplier(u);

		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			response.getWriter().append(e.toString());
			response.getWriter().append(errors.toString());

			Log.error(e);
			return;
		}
		c.close();
		response.getWriter().append("ok");

	}

	/**
	 *
	 */
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX
			= Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	/**
	 *
	 * @param emailStr
	 * @return
	 */
	public static boolean validate(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

}
