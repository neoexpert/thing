package core.controller;

import com.neoexpert.Log;
import core.*;
import core.attribute.Attribute;
import core.events.Event;
import core.id.ObjectID;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import templates.Template;
import types.Types;


/**
 *
 * @author neoexpert
 */
public abstract class Controller {
	protected void init(String domain, boolean root) {
		if (root) {
			rootID = null;
			return;
		}
		Node rootcat = getNode(rootID, "SITES");
		if (rootcat == null) {
			return;
		}
		Node newroot = rootcat.getChild(domain);
		if (newroot == null) {
			return;
		}
		rootcat = newroot;
		rootID = rootcat.getID();
	}

	protected ObjectID rootID = new ObjectID("ROOT");

	public Users getUsers() {
		return (Users) getRoot().getChild("USERS");
	}
	public Node getEvents() {
		return getRoot().getChild("EVENTS");
	}
	public Category getRootCategory() {
		return (Category) getRoot().getChild("CATEGORIES");
	}

	public abstract NodeIterator find(String query);

	/**
	 *
	 */
	public static class NodeExistsException extends RuntimeException {

		private final Exception e;

		/**
		 *
		 * @param e
		 */
		public NodeExistsException(Exception e) {
			this.e = e;
		}

		/**
		 *
		 * @return
		 */
		public Exception getReason() {
			return e;
		}

	}

	/**
	 *
	 * @return
	 */
	public Types getTypes() {
		Node types;
		types = getNode("TYPES");
		if (types instanceof Types) {
			return (Types) types;
		}
		return null;
		//return (Types) getNode(0).getChild("TYPES");
	}

	public Settings getSesstings() {
		return (Settings) getNode("SETTINGS");
	}


	/**
	 *
	 * @return
	 */
	public Node getMailSettings() {
		return getNode("SETTINGS").getChild("EMAIL");
	}

	/**
	 *
	 * @param cat
	 * @param u
	 * @return
	 */
	public abstract Node addNode(Node cat, User u);

	/**
	 *
	 * @param cat
	 * @param container
	 * @param u
	 */
	public void integrate(Node cat, Node container, User u) {
		container.addAllChildren(cat, u);
	}

	/**
	 *
	 * @param e
	 * @param u
	 * @return
	 */
	public Event addEvent(Event e, User u) {
		Node events = getNode("EVENTS");
		if (events == null) {
			return null;
		}

		e.setParent(events);
		e.setName(UUID.randomUUID().toString());
		e = (Event) addNode(e, u);
		//EventSender.sendAll(e, u);
		return e;
	}

	/**
	 *
	 * @param oldid
	 * @param newid
	 * @param u
	 */
	public abstract void changeID(ObjectID oldid,ObjectID newid,User u);
	
	/**
	 *
	 * @param e
	 * @param n
	 * @param u
	 * @return 
	 */
	public Event addEvent(String e, Node n, User u) {
		Log.log(u,n.getType()+" "+e);
		try {
			Node events = getNode("EVENTS");
			if (events == null) {
				return null;
			}
			String pvalue;
			pvalue = n.getPreviousValue();
			String newvalue;

			newvalue = n.getNewValue();

			Event event = new Event(n, e, this, pvalue, newvalue);
			event.setParent(events);
			event.setName(UUID.randomUUID().toString());

			addNode(event, u);

			//EventSender.sendAll(event, u);
			return event;
		} catch (RuntimeException ex) {
			Log.log("can not add event to root node (ID=" + rootID + ")");
			Log.error(ex);
		}
		return null;
	}

	/**
	 *
	 * @param cat
	 */
	public abstract void addToArchive(Node cat);

	/**
	 *
	 * @param contaienr
	 * @param id
	 */
	public abstract void addToArchive(String contaienr, int id);

	/**
	 *
	 * @param name
	 * @param password
	 * @return
	 */
	public abstract User addUser(String name, String password);

	/**
	 *
	 */
	public abstract void close();
	public abstract boolean isConnected();

	/**
	 *
	 * @param cat
	 * @param container
	 * @param u
	 */
	public abstract void copy(Node cat, Node container, User u);


	/**
	 *
	 * @param n
	 * @param u
	 */
	public abstract void decrementOrderID(Node n, User u);

	/**
	 *
	 * @param childID
	 * @param u
	 * @return
	 */
	public abstract int delete(ObjectID childID, User u);

	/**
	 *
	 * @param path
	 * @param id
	 * @param u
	 * @return
	 */
	public abstract boolean deletePicture(String path, ObjectID id, User u);

	/**
	 *
	 * @param contaienr
	 * @param id
	 * @return
	 */
	public abstract Iterator<Node> getArchive(String contaienr, int id);

	/**
	 *
	 * @param parent
	 * @param attr_type
	 * @return
	 */
	public abstract LinkedHashMap<String, Attribute> getAttributes(ObjectID parent, String attr_type);

	/**
	 *
	 * @param id
	 * @return
	 */
	public abstract LinkedHashMap<String, Attribute> getAttributes(int id);

	/**
	 *
	 * @param id
	 * @param type
	 * @return
	 */
	public abstract long getChildrenCount(ObjectID id, Class<?> type);

	/**
	 *
	 * @param iD
	 * @return
	 */
	public abstract long getChildrenCount(ObjectID iD);

	/**
	 *
	 * @param id
	 * @return
	 */
	public abstract Node getNode(ObjectID id);

	/**
	 *
	 * @param parentID
	 * @param name
	 * @return
	 */
	public abstract Node getNode(ObjectID parentID, String name);

	/**
	 *
	 * @param parentID
	 * @param name
	 * @param type
	 * @return
	 */
	public abstract Node getNode(ObjectID parentID, String name, Class<?> type);

	/**
	 *
	 * @param parentID
	 * @param type
	 * @param name
	 * @return
	 */
	public abstract Node getNode(ObjectID parentID, Class<?> type, String name);

	/**
	 *
	 * @param name
	 * @return
	 */
	public abstract Node getNode(String name);

	/**
	 *
	 * @param parent
	 * @param id
	 * @return
	 */
	public abstract Node getNode(ObjectID parent, ObjectID id);

	/**
	 *
	 * @param root
	 * @param caption
	 * @param lang
	 * @return
	 */
	public abstract Node getNodeByUrl(Node root, String caption, Locale lang);

	/**
	 *
	 * @param node
	 * @param confs
	 * @return
	 * @throws JSONException
	 */
	public abstract NodeIterator getNodeByConfiguration(Node node, JSONObject confs) throws JSONException;

	/**
	 *
	 * @param id
	 * @param type
	 * @param attrname
	 * @param lang
	 * @return
	 */
	public abstract Node getNodeLang(ObjectID id, Class<?> type, String attrname, Locale lang);


	/**
	 *
	 * @return
	 */
	public abstract Node getRoot();

	/**
	 *
	 * @return
	 */
	public abstract ObjectID getRootID();

	/**
	 *
	 * @param n
	 * @param u
	 */
	public abstract void incrementOrderID(Node n, User u);

	/**
	 *
	 * @param u
	 * @param id
	 * @param parent
	 * @return
	 */
	public abstract Node moveNode(User u, ObjectID id, ObjectID parent);

	/**
	 *
	 * @param cat
	 * @param container
	 * @param u
	 */
	public abstract void moveNode(Node cat, Node container, User u);

	/**
	 *
	 * @param id
	 */
	public abstract void refreshUpdated(ObjectID id);

	/**
	 *
	 * @param u
	 */
	public abstract void setup(User u);

	/**
	 *
	 * @param u
	 * @param cat
	 * @return
	 */
	public abstract Node updateNode(User u, Node cat);

	/**
	 *
	 * @param username
	 * @param password
	 * @return
	 */
	public abstract User userLogin(String username, String password);

	/**
	 *
	 * @return
	 */
	public abstract boolean isSuperRoot();

	/**
	 *
	 * @return
	 */
	public abstract Locale getLang();

	/**
	 *
	 * @param n
	 * @param u
	 * @return
	 */
	public abstract int delete(Node n, User u);
	
	/**
	 *
	 * @param n
	 * @param u
	 * @return
	 */
	public void remove(Node n, User u){
		n.setName(n.getName()+" "+UUID.randomUUID().toString());
		n.setParent(new ObjectID("TRASH"))
				.commit(u);
	}


	/**
	 *
	 * @param node
	 * @param name
	 * @return
	 */
	public abstract NodeIterator getAllChildren(Node node, String name);

	/**
	 *
	 * @param u
	 * @param n
	 * @param newparent
	 * @return
	 */
	public abstract Node copyNode(User u, Node n, ObjectID newparent);

	/**
	 *
	 * @param name
	 * @return
	 */
	public abstract User getUser(String name);

	/**
	 *
	 * @param node
	 * @return
	 */
	public abstract int getNextCID(Node node);

	/**
	 *
	 * @param u
	 * @param child_copy
	 * @param parentnode
	 * @return
	 */
	public abstract Node copyNode(User u, Node child_copy, Node parentnode);

	/**
	 *
	 * @return
	 */
	public abstract NodeIterator find();

	/**
	 *
	 * @param attribute
	 */
	public abstract void deleteAttr(Attribute attribute);

	/**
	 *
	 * @param parent
	 * @param name
	 * @return
	 */
	public abstract boolean nodeExists(ObjectID parent, String name);

	/**
	 *
	 * @param node
	 */
	public abstract void accessNode(Node node);

	/**
	 *
	 * @param path
	 * @return
	 */
	public Node getNodeByPath(String path) {
		String[] names = path.split("/");
		Node n = getNode("CATEGORIES");
		for (String name : names) {
			n = n.getChild(name);
		}
		return n;
	}

	/**
	 *
	 * @return
	 */
	protected abstract Node createRoot();

	/**
	 *
	 * @return
	 */
	public Node init() {
		Node root = getRoot();
		if (root == null) {
			root = createRoot();
		}
		Node users = root.getChild("USERS");
		if (users == null) {
			users = new Users(null, this);
			users.setName("USERS");
			users = root.addChild(users, null);
			Log.logGreen("node USERS created");
		}
		User u = (User) users.getChild("root");

		if (u == null) {
			u = new User(null, this);
			u.setName("root");
			u = (User) users.addChild(u, null);
			u.setOwner(u).save(u);
			u.setAttribute("email_confirmed", true).commit(u);
			u.addToGroup("admin", u);
			u.setPassword("huhu", u);
			Log.logGreen("User 'root' created");
		}
		Node events = getEvents();
		if (events == null) {
			events = new Node(u.getID(), this);
			events.setName("EVENTS");
			root.addChild(events, u);
			Log.logGreen("NODE EVENTS created");
		}
		Node types = getTypes();
		if (types == null) {
			types = new Types(u.getID(), this);
			types.setName("TYPES");
			root.addChild(types, u);
			Log.logGreen("NODE TYPES created");
		}
		Node zombies = root.getChild("ZOMBIES");
		if (zombies == null) {
			zombies = new Node(u.getID(), this);
			zombies.setName("ZOMBIES");
			root.addChild(zombies, u);
			Log.logGreen("NODE ZOMBIES created");
		}
		Node cats = root.getChild("CATEGORIES");
		if (cats == null) {
			cats = new Category(u.getID(), this);
			cats.setName("CATEGORIES");
			root.addChild(cats, u);
			Log.logGreen("NODE CATEGORIES created");
		}
		Node design = root.getChild("DESIGN");
		if (design == null) {
			design = new Node(u.getID(), this);
			design.setName("DESIGN");
			root.addChild(design, u);
			Log.logGreen("NODE DESIGN created");
		}



		return root;
	}

	/**
	 *
	 * @param node
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	public abstract Node addToSet(Node node, String name, String value, User u);

	/**
	 *
	 * @param node
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	public abstract Node removeFromSet(Node node, String name, String value, User u);

	/**
	 *
	 * @param node
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	public abstract Node addToList(Node node, String name, String value, User u);

	/**
	 *
	 * @param node
	 * @param name
	 * @param value
	 * @param u
	 * @return
	 */
	public abstract Node removeAllFromList(Node node, String name, String value, User u);

	/**
	 *
	 * @param node
	 * @param name
	 * @param u
	 */
	public abstract void deleteAttr(Node node, String name, User u);

	/**
	 *
	 * @param node
	 * @param name
	 * @param u
	 */
	public abstract void addAttr(Node node, String name, User u);

	/**
	 *
	 * @param parent
	 * @param attr
	 * @param key
	 * @param value
	 * @param locale
	 */
	public abstract void addToMap(Node parent, Attribute attr, String key, String value, Locale locale);

	/**
	 *
	 * @return
	 */
	public abstract Object getConnection();

	public abstract void deleteSession(String id);

}
