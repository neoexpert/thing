package core;
import core.controller.Controller;
import core.events.*;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Session extends Node {

    public Session( ObjectID userID, Controller c) {
	    super(userID,c);
	}
    /**
     *
     * @param c
	 * @param doc
     */
    public Session(Controller c,Document doc) {
		super(c,doc);
	}
    public void sendEvent(Event e){
	//EventSender.sendEvent(getName(),e);	
    }
	
    /**
     *
     * @return
     */
    @Override
	public boolean canInherit() {
		return false;
	}
	
}
