package core.processor;

import core.User;
import core.controller.Controller;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class JSONParams extends ProcessorInterface {

    private final JSONObject jo;
    private final ResultWriter rw;

    /**
     *
     * @param jo
     * @param rw
     */
    public JSONParams(JSONObject jo, ResultWriter rw) {
        this.jo = jo;
        this.rw = rw;
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public String getParameter(String key) {
        if (jo.has(key)) {
            return jo.get(key) + "";
        }
        return null;
    }
		@Override
    public JSONObject getJSONObject(String key){
			return jo.getJSONObject(key);	
		}

    /**
     *
     * @param result
     */
    @Override
    public void writeResult(String result) {
        rw.write(result);
    }

    @Override
    public void writeResult(JSONObject result) {
        rw.write(result);
    }

    /**
     *
     * @param string
     * @return
     */
    @Override
    public String[] getParameterValues(String string) {
        return new String[]{};
    }

    /**
     *
     * @return
     */
    @Override
    public Set<String> getKeys() {
        return jo.keySet();
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public String remove(String key) {
        return jo.remove(key).toString();
    }

    /**
     *
     * @param key
     * @return
     */
    @Override
    public JSONObject get(String key) {
        if (jo.has(key)) {
            return jo.getJSONObject(key);
        }
        return null;
    }

    /**
     *
     * @param key
     * @param jo
     */
    @Override
    public void put(String key, JSONObject jo) {

    }

    

    

    /**
     *
     * @return
     */
    @Override
    public JSONObject toJSON() {
        return jo;
    }

    /**
     *
     * @param string
     */
    @Override
    public void setCmd(String string) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @return
     */
    @Override
    public String getCmd() {
        return getParameter("cmd");
    }

    /**
     *
     * @param e
     */
    @Override
    public void error(Exception e) {
				JSONObject jo=new JSONObject();
				jo.put("cmd","error");
				jo.put("error",e.toString());

        rw.write(jo);
    }

    /**
     *
     * @return
     */
    @Override
    public String getURL() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     *
     * @param name
     * @param c
     * @param u
     */
    @Override
    public void renderSite(String name, Controller c, User u) {
        // TODO Auto-generated method stub

    }

}
