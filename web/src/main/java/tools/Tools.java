package tools;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

/**
 *
 * @author neoexpert
 */
public class Tools {
	private static final long SECONDS_IN_A_DAY = 60 * 60 * 24;

    /**
     *
     * @param millis
     * @return
     */
    public static String getDuration(long millis) {
		String t;
		long diffSec = millis / 1000;
		// if(!ChatService.connected)
		// if(lastReconnectTry<System.currentTimeMillis()-15000){
		// Intent i=new Intent("com.neoexpert.scdown.CHAT_SERVICE");
		//
		//
		// i.putExtra("type", "please_connect");
		//
		// sendBroadcast(i);
		// lastReconnectTry=System.currentTimeMillis();
		// }
		long days = diffSec / SECONDS_IN_A_DAY;
		long secondsDay = diffSec % SECONDS_IN_A_DAY;
		long seconds = secondsDay % 60;

		long minutes = (secondsDay / 60) % 60;
		long hours = (secondsDay / 3600); // % 24 not needed
		if (days == 0)
			if (hours == 0)
				if (minutes == 0) {
					t = String.format("%d s", seconds);
				} else
					t = String.format("%d m", minutes);

			else
				t = String.format("%d h", hours);

		else {
			t = String.format("%d d", days);

			// getStatus();

		}
		return t;
	}

    /**
     *
     * @param o
     * @param callCount
     * @return
     */
    public static String dump(Object o, int callCount) {
		callCount++;
		StringBuffer tabs = new StringBuffer();
		for (int k = 0; k < callCount; k++) {
			tabs.append("\t");
		}
		StringBuffer buffer = new StringBuffer();
		Class<?> oClass = o.getClass();
		if (oClass.isArray()) {
			buffer.append("\n");
			buffer.append(tabs.toString());
			buffer.append("[");
			for (int i = 0; i < Array.getLength(o); i++) {
				if (i < 0)
					buffer.append(",");
				Object value = Array.get(o, i);
				if (value.getClass().isPrimitive() || value.getClass() == java.lang.Long.class
						|| value.getClass() == java.lang.String.class || value.getClass() == java.lang.Integer.class
						|| value.getClass() == java.lang.Boolean.class) {
					buffer.append(value);
				} else {
					buffer.append(dump(value, callCount));
				}
			}
			buffer.append(tabs.toString());
			buffer.append("]\n");
		} else {
			buffer.append("\n");
			buffer.append(tabs.toString());
			buffer.append("{\n");
			while (oClass != null) {
				Field[] fields = oClass.getDeclaredFields();
				for (int i = 0; i < fields.length; i++) {
					buffer.append(tabs.toString());
					fields[i].setAccessible(true);
					buffer.append(fields[i].getName());
					buffer.append("=");
					try {
						Object value = fields[i].get(o);
						if (value != null) {
							if (value.getClass().isPrimitive() || value.getClass() == java.lang.Long.class
									|| value.getClass() == java.lang.String.class
									|| value.getClass() == java.lang.Integer.class
									|| value.getClass() == java.lang.Boolean.class) {
								buffer.append(value);
							} else {
								buffer.append(dump(value, callCount));
							}
						}
					} catch (IllegalAccessException e) {
						buffer.append(e.getMessage());
					}
					buffer.append("\n");
				}
				oClass = oClass.getSuperclass();
			}
			buffer.append(tabs.toString());
			buffer.append("}\n");
		}
		return buffer.toString();
	}
}
