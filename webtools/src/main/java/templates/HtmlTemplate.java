package templates;

import core.File;
import core.Node;
import core.User;
import core.attribute.Attribute;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.id.ObjectID;
import java.util.Locale;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

/**
 *
 * @author neoexpert
 */
public class HtmlTemplate extends Template {

	static {
		addChildType(HtmlTemplate.class, File.class);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public HtmlTemplate(Controller c,org.bson.Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public HtmlTemplate(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
	 *
	 * @param n
	 * @param locale
	 * @return
	 */
	@Override
	public String fill(Node n, Locale locale) {
		String html = getValue();
		Document d;
		if (xmlParser()) {
			d = Jsoup.parse(html, "", Parser.xmlParser());
		} else {
			d = Jsoup.parse(html, "", Parser.htmlParser());
		}

		d.outputSettings().prettyPrint(false);

		Element tag = d;


		Elements elements = tag.getElementsByClass("attribute");

		for (Element e : elements) {

			String attrName = e.attr("data-name");
			String value;
			switch (attrName) {
				case "created_timestamp":
					value = String.valueOf(n.getCreatedString(locale));
					break;
				case "updated_timestamp":
					value = String.valueOf(n.getUpdatedString(locale));
					break;
				case "cid":
					value = String.valueOf(n.getCID());
					break;
				case "name":
					value = n.getName();
					break;
				default:
					Attribute a = n.getAttribute(attrName);

					if (a == null) {
						continue;
					}
					e.attr("data-parent", a.getParentID() + "");
					value = a.getValue(locale).toString();
					break;

			}
			String data_attr = e.attr("data-attr");
			if (data_attr.isEmpty()) {
				String data_load = e.attr("data-load");
				if (data_load.isEmpty()) {
					e.html(value);
				} else {
					e.attr("data-value", value);
					e.addClass("loadme");
				}

			} else {
				e.attr(data_attr, value);
			}

		}
		Elements children_unfilled = tag.select(".child.unfilled");

		for (Element e : children_unfilled) {
			String childName = e.attr("data-name");
			Node child = n.getChild(childName);
			if (child == null) {
				e.html("no such child");
				continue;
			}

			Template t = child.getTemplate();
			if (t == null || !(t instanceof HtmlTemplate)) {
				continue;
			}
			String wt = t.getWrapperTag();

			StringBuilder sb = new StringBuilder();
			sb.append("<");
			sb.append(wt);
			sb.append(" class='");
			sb.append(child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH));
			sb.append("' data-id='");
			sb.append(child.getID());
			sb.append("' data-name='");
			sb.append(child.getName());
			sb.append("'>");
			sb.append(t.fill(child, locale));
			sb.append("</");
			sb.append(wt);
			sb.append(">");

			e.html(sb.toString());

		}
		Elements children = tag.select(".children");
		String sortby = children.attr("data-sort");
		String templateprefix = children.attr("data-templateprefix");

		for (Element e : children) {
			if (e.hasClass("filled")) {
				continue;
			}

			NodeIterator<Node> it = n.getChildren();
			if (sortby.isEmpty()) {
				it.sort("cid", false).build();
			} else {
				it.sort(sortby, false).build();
			}

			StringBuilder html2 = new StringBuilder();
			int i = 1;
			while (it.hasNext()) {

				Node child = it.next();
				Template t;
				if (templateprefix.isEmpty()) {
					t = child.getTemplate();
				} else {
					t = child.getThumbnailTemplate();
				}
				if (t == null || !(t instanceof HtmlTemplate)) {
					continue;
				}
				String wt = t.getWrapperTag();
				html2.append("<").append(wt).append(" class='").append(child.getClass().getSimpleName().toLowerCase())
						.append(" unfilled' data-id='").append(child.getID())
						.append("' data-name='").append(child.getName()).append("'>");
				html2.append(((HtmlTemplate) t).fill(child, locale));
				html2.append("</").append(wt).append(">");
			}
			e.html(html2.toString());
			e.addClass("filled");

		}
		//if(d.body()!=null)
		//	return d.body().html();
		//else 
		return d.toString();
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		return false;
	}

	/**
	 *
	 * @return
	 */
	public String getWrapper() {
		Object wrapper = doc.get("wrapper");
		if (wrapper != null) {
			if (wrapper instanceof String) {
				return (String) wrapper;
			}
		}
		return "div";
	}

	/**
	 *
	 * @param u
	 */
	@Override
	public void onCreate(User u) {
		addAttr("value", u);
		addAttr("dynamic", u);
		addAttr("xmlparser", u);

	}

	/**
	 *
	 * @param v
	 * @return
	 */
	public HtmlTemplate setValue(String v) {
		doc.put("value", v);
		return this;
	}

	/**
	 *
	 * @return
	 */
	public boolean isDynamic() {
		if (doc.containsKey("dynamic")) {
			Object dynamic = doc.get("dynamic");
			if (dynamic instanceof Boolean) {
				return (Boolean) dynamic;
			}
		}
		return false;
	}

	public boolean xmlParser() {
		if (doc.containsKey("xmlparser")) {
			Object xmlparser = doc.get("xmlparser");
			if (xmlparser instanceof Boolean) {
				return (Boolean) xmlparser;
			}
		}
		return false;
	}
	@Override
	public String getUnfilled(Node n){
		StringBuilder sb=new StringBuilder();
		String wrapper = getWrapper();
		sb
			.append("<")
			.append(wrapper)
			.append("data-id='")
			.append(n.getID())
			.append("' class='")
			.append(n.getClass().getSimpleName().toLowerCase(Locale.ENGLISH))
			.append("'>")
			.append(super.getValue())
			.append("</")
			.append(wrapper)
			.append(">");
		return sb.toString();
	}

}
