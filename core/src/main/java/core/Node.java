package core;

import com.neoexpert.Log;
import core.attribute.Attribute;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.events.*;
import core.id.ObjectID;
import core.processor.ProcessorInterface;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bson.Document;
import org.json.JSONObject;
import templates.Template;
import tools.Tools;

/**
 *
 * Die Klasse “Node” ist die wichtigste Klasse in unserem System. Sie ermöglicht
 * die Hierarchie durch ihre “Kinder” die wiederum selbst Nodes sind. Ein Objekt
 * von dem Typ Node kann beliebig viele Kinder haben. Alle Entitäten (z.B. Node,
 * Product, User, Item usw.) erben von dieser Klasse. Die Nodes werden durch
 * Attribute beschrieben. Ein Attribut ist auch ein Node.
 *
 * @author neoexpert
 */
public class Node {

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public Node(ObjectID userID, Controller c) {
		doc = new Document();
		doc.put("owner", userID);
		this.c = c;
	}

	public Node(Controller c, Document doc) {
		this.doc = doc;
		this.c = c;
		this.parent = new ObjectID(doc.get("parent"));
		if (doc.containsKey("flags")) {
			flags = doc.getInteger("flags");
		}
	}

	public Node(Controller c, JSONObject jo) {
		this.doc = new Document();
		Set<String> keys = jo.keySet();
		for (String key : keys) {
			doc.put(key, jo.get(key));
		}
		this.c = c;
		this.parent = new ObjectID(doc.get("parent"));
		if (doc.containsKey("flags")) {
			flags = (int) doc.get("flags");
		}
	}

	public boolean mayAccess(User u) {
		if (getOwnerID().equals(u.getID())) {
			return true;
		}
		if (u.isAdmin()) {
			return true;
		}
		return u.contains(this);
	}
	/**
	 *
	 */
	protected ObjectID parent;

	/**
	 *
	 */
	protected String path;

	/**
	 *
	 */
	public Map<String,Object> doc;

	/**
	 *
	 */
	protected int flags;

	/**
	 *
	 */
	protected Controller c;

	// timestamp
	// ---------------- setter and getter for the Node-Paths ----------------//
	/**
	 *
	 * @return
	 */
	public String getPath() {
		path = (String) doc.get("path");
		// if (path == null)
		refreshPath();
		return path + "/";
	}

	// ---------------------------------------------------------------------//
	/**
	 *
	 * @return
	 */
	public String refreshPath() {
		Node p = getParent();
		if (p == null) {
			return "";
		}
		path = "/" + parent.get().toString();

		path = p.refreshPath() + path;
		return path;
	}

	// ---------------- setter and getter for the Flags --------------------//
	/**
	 *
	 * @param flags
	 */
	public void setFlags(int flags) {
		this.flags = flags;
	}

	/**
	 *
	 * @return
	 */
	public int getFlags() {
		return flags;
	}
	// ---------------------------------------------------------------------//

	/**
	 *
	 * @return
	 */
	public long getCreated() {
		return (long) doc.get("created");
	}

	/**
	 *
	 * @return
	 */
	public long getUpdated() {
		if (doc.containsKey("updated")) {
			return (long) doc.get("updated");
		}
		return getCreated();
	}

	/**
	 *
	 * @return
	 */
	public ObjectID getParentID() {
		return parent;
	}
	private Node cachedParent = null;

	/**
	 * Sets the parent of a Node.
	 *
	 * @param p Node with given Values (will be the future parent of the Node on
	 * whom this method got called)
	 * @return
	 */
	public Node setParent(Node p) {
		cachedParent = p;
		parent = p.getID();
		return this;
	}

	/**
	 *
	 * @return
	 */
	public int getNextCID() {
		return c.getNextCID(this);
	}

	/**
	 * Sets a blank parent with only a type
	 *
	 * @param parent
	 * @return
	 */
	public Node setParent(ObjectID parent) {
		this.parent = parent;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public ObjectID getID() {
		return new ObjectID(doc.get("_id"));
	}

	/**
	 *
	 * @return
	 */
	public int getOrderID() {
		Object order_id = doc.get("order_id");
		if(order_id instanceof Integer && order_id !=null)
			return (int) order_id;
		return 0;
	}

	/**
	 *
	 * @param orderID
	 * @return
	 */
	public Node setOrderID(int orderID) {
		doc.put("order_id", orderID);
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return (String) doc.get("name");
	}

	/**
	 *
	 * @return
	 */
	public Iterator<Attribute> getMyAttributes() {
		return null;
	}

	/**
	 *
	 * @param attr_type type of the attribute
	 * @param recursive is it recursive?
	 * @return a LinkedHashMap with attributes and their types
	 */
	public LinkedHashMap<String, Attribute> getAttributes(String attr_type, boolean recursive) {
		// Warum so verschachtelt?
		LinkedHashMap<String, Attribute> attrs = c.getAttributes(getID(), attr_type);

		if (parent == null || !recursive || !canInherit()) {
			/*
			 * HashMap<String, Attribute> parentattrs = c.getAttributesWithTypes(parent);
			 * for (String key : parentattrs.keySet()) if (!attrs.containsKey(key))
			 * attrs.put(key, parentattrs.get(key)); else
			 * attrs.get(key).setCustomValueType(parentattrs.get(key).
			 * getCustomValueType());
			 */
			return attrs;
		}

		Node p = getParent();
		LinkedHashMap<String, Attribute> parentattrs = p.getAttributes(attr_type, recursive);
		/*
		 * for (String key : parentattrs.keySet()) if (!attrs.containsKey(key))
		 * attrs.put(key, parentattrs.get(key)); else
		 * attrs.get(key).setCustomValueType(parentattrs.get(key).
		 * getCustomValueType());
		 */
		Set<Entry<String, Attribute>> entryset = attrs.entrySet();
		entryset.stream().forEach((entry) -> {
			Attribute a = entry.getValue();

			parentattrs.put(entry.getKey(), a);
		});

		// for (String key : attrs.keySet()) {
		// Attribute a = attrs.get(key);
		// if (parentattrs.containsKey(key)) {
		// Attribute pa = parentattrs.get(key);
		// a.setValueType(pa.getValueType());
		// a.setOrderID(pa.getOrderID());
		// }
		// parentattrs.put(key, a);
		// }
		return parentattrs;

	}

	// ist diese Methode nötig? Macht die nich im Prinzip das gleiche wie die
	// getAttributesWithTypes()?
	private final static Set<String> SATTRS = new HashSet<>();

	static {
		SATTRS.add("_id");
		// sattrs.add("type");
		SATTRS.add("owner");
		SATTRS.add("cid");
		SATTRS.add("parent");
		SATTRS.add("created");
		SATTRS.add("updated");
		SATTRS.add("involved");
		SATTRS.add("path");
		SATTRS.add("last_cid");
		SATTRS.add("group");
		SATTRS.add("rights");
		SATTRS.add("flags");

	}

	/**
	 *
	 * @param recursive
	 * @return
	 */
	public LinkedHashMap<String, Attribute> getAttributes(boolean recursive) {
		LinkedHashMap<String, Attribute> attrs = new LinkedHashMap<>();
		Set<String> keys = doc.keySet();
		keys.stream().filter((key) -> !(SATTRS.contains(key))).forEach((key) -> {
			attrs.put(key, new Attribute(key, this, c));
		});

		return attrs;
	}

	/**
	 *
	 * @return the parent of the node
	 */
	public Node getParent() {
		if (parent == null) {
			return null;
		}
		return c.getNode(parent);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return getClass().getName() + ":" + getName() + " (" + getID() + ")";
	}

	/**
	 *
	 * @param n future Child-Node
	 * @param u User
	 * @return Child-Node
	 */
	// Eine der beiden Methoden reicht doch, oder?
	public Node addChild(Node n, User u) {
		n.setParent(this);
		Node child = c.addNode(n, u);
		return child;
	}

	// public Node addOrUpdateChild(User u, Node cat) {
	// if (childExists(cat.getName())) {
	// return getChild(cat.getName()).setValue(cat.getValue(),u);
	// }
	// cat.setParent(this);
	// Node child = c.addNode(cat, u);
	// if (cat instanceof Attribute)
	// ((Attribute) child).setValue(((Attribute) cat).getValue());
	// return child;
	// }
	// -------------------------------------//
	private static HashMap<Class<?>, Set<Class<?>>> cc = new HashMap<>();

	static {
		addChildType(Node.class, Node.class);
		addChildType(Node.class, Site.class);
		addChildType(Node.class, File.class);
		addChildType(Node.class, Link.class);

	}

	/**
	 *
	 * @param container
	 * @param child
	 */
	public static void addChildType(Class<?> container, Class<?> child) {
		Set<Class<?>> canContain = cc.get(container);
		if (canContain == null) {
			canContain = new HashSet<>();
			cc.put(container, canContain);
		}
		canContain.add(child);
	}

	/**
	 *
	 * @return
	 */
	public final Set<Class<?>> canContain() {
		return cc.get(getClass());
	}

	/**
	 *
	 * @param cat ???
	 * @param u User
	 *
	 */
	// was passiert hier? // -------------------------------------////
	// -------------------------------------////
	// -------------------------------------//
	public void addAllChildren(Node cat, User u) {
		NodeIterator<Node> it;
		it = cat.getChildren().build();

		// cat.setParent(this);
		try {
			cat = addChild(cat, u);
		} catch (Exception e) {
			Log.error(e);
		}

		while (it.hasNext()) {
			Node n = it.next();
			if (n == null) {
				continue;
			}

			cat.addAllChildren(n, u);
		}

	}

	// -------------------------------------////
	// -------------------------------------////
	// -------------------------------------//
	/**
	 *
	 * @param cat Node we get the children from
	 * @param u User
	 */
	public void addAllChildrenFrom(Node cat, User u) {
		Iterator<Node> it = cat.getChildren().build();
		while (it.hasNext()) {
			Node n = it.next();
			addAllChildren(n, u);
		}
	}

	/**
	 *
	 * @param type Type of Child-Node
	 * @return quantity of all Child-Nodes from this Node
	 */
	public long childCount(Class<?> type) {
		return c.getChildrenCount(getID(), type);
	}

	/**
	 *
	 * @return every child of this Node as an NodeIterator Object
	 */
	public NodeIterator getChildren() {
		return c.find().addFieldToFilter("parent", getID().get());
	}

	/**
	 *
	 * @param type Type of Node
	 * @return every child of this Node (with the given type) as an NodeIterator
	 * Object
	 */
	public NodeIterator getChildren(Class<?> type) {
		NodeIterator it = c.find().addFieldToFilter("parent", getID().get()).addFieldToFilter("type", type);
		return it.build();
	}

	/**
	 *
	 * @param type
	 * @param orderby
	 * @param desc
	 * @return every child of this Node (with the given type) in the specified
	 * order as an NodeIterator Object
	 */
	// Unfertig?
	public Iterator<Node> getChildren(Class<?> type, String orderby, boolean desc) {
		NodeIterator it = c.find();
		it.addFieldToFilter("parent", getID());
		it.addFieldToFilter("type", type);
		it.sort(orderby, desc);
		return it.build();
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Attribute getAttribute(String name) {
		if (doc.get(name) == null) {
			Node p = getParent();
			if (p == null) {
				return null;
			}
			Attribute a = p.getAttribute(name);
			if (a != null) {
				return a.setInherited();
			} else {
				return null;
			}
		}
		return new Attribute(name, this, c);
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Attribute getMyAttribute(String name) {
		if (doc.containsKey(name)) {
			return new Attribute(name, this, c);
		}
		return null;
	}

	/**
	 *
	 * @param u User
	 * @param name Name of the Attribute
	 * @param value Values of the Node Attribute
	 * @return Node with the new Attribute set
	 */
	public Node setAttribute(User u, String name, Object value) {
		doc.put(name, value);
		return this;
	}

	/**
	 *
	 * @param name Name of the Attribute
	 * @param locale
	 * @param value Value of the Attribute Type of the Attribute
	 * @return the Attribute with a new Attribute set
	 */
	public Node setAttribute(String name, Object value, Locale locale) {
		Object attr = doc.get(name);
		if (attr instanceof Document) {
			((Document) attr).put(locale.getLanguage(), value);
		} else {
			doc.put(name, new Document(locale.getLanguage(), value));
		}
		return this;

	}

	/**
	 * TODO
	 *
	 * @param name Name of the Child
	 * @param type
	 * @return
	 * @throws Exception
	 */
	private Node getChild(String name, Class<?> type) {
		return c.getNode(getID(), name, type);
	}

	/**
	 *
	 * @param name Name of the Child
	 * @return desired Child
	 */
	public Node getChild(String name) {
		return c.getNode(getID(), name);
	}

	public Node getRelated(String name) {
		Node n = this;
		while (n != null) {
			Node child = n.getChild(name);
			if (child != null) {
				return child;
			}
			n = n.getParent();
		}
		return null;
	}

	public Node getRelated(String name, Class<?> type) {
		Node n = this;
		while (n != null) {
			Node child = n.getChild(name, type);
			if (child != null) {
				return child;
			}
			n = n.getParent();
		}
		return null;
	}

	/**
	 *
	 * @param name Name of the Child
	 * @return true if there is a child with this name, otherwise false
	 */
	protected boolean childExists(String name) {
		try {
			return c.nodeExists(getID(), name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		return false;
	}

	/**
	 *
	 * @param name Name of the Template we want to get
	 * @return the desired Template
	 */
	public Template getTemplate(String name) {
		return (Template) getRelated(name, Template.class);
	}

	/**
	 *
	 * @return
	 */
	public Template getTemplate() {
		Object template = doc.get("template");
		if (template == null) {
			template = getType();
		}
		return getTemplate(template.toString());
	}

	/**
	 *
	 * @return
	 */
	public Template getThumbnailTemplate() {

		return getTemplate("thumbnail." + getClass().getName());

	}

	/**
	 *
	 */
	protected static final Map<ObjectID, ObjectID> ADMINCACHE = new HashMap<>();

	/**
	 *
	 * @param template
	 * @return
	 */
	public Template getAdminTemplate(String template) {
		if (template == null) {
			ObjectID tID = ADMINCACHE.get(getID());
			if (tID != null) {
				System.out.println("template form cache");

				return (Template) c.getNode(tID);
			}
			Template tNode = getTemplate("admin." + getClass().getName());
			if (tNode == null) {
				tNode = getTemplate("admin.core.Node");
			}
			if (tNode != null) {
				ADMINCACHE.put(getID(), tNode.getID());
			}

			return tNode;
		} else {
			return getTemplate(template + ".admin." + getClass().getName());
		}
	}

	// was passiert mit dem Parameter u?
	// onCreate ist ein Erreignis. Diese funktion wird aufgerufen wenn ein Node
	// erzeugt wird.
	// u ist dann der Ersteller
	/**
	 *
	 * @param u
	 */
	public void onCreate(User u) {
		refreshUpdated();
		Event e = c.addEvent("CREATED", this, u);
		if (cachedParent != null) {
			cachedParent.processEvent(e);
		}
	}

	/**
	 * refreshes the parentnode to apply changes in this one.
	 *
	 */
	protected void refreshUpdated() {
		c.refreshUpdated(getID());
		Node p;
		try {
			p = getParent();
		} catch (Exception e) {
			Log.error(e);
			return;
		}
		if (p != null) {
			p.refreshUpdated();
		}
	}

	// redundant?
	// diese funktion wird aufgerugen wenn ein Node aktualisiert wird
	// das kann praktisch sein. man kann ja jede Methode überschreiben
	// Überschreibt man onUpdate z.B. beim User, kann man gewisse aktionen hier
	// ausführen, wenn sich der Use aktualisiert
	/**
	 *
	 * @param u
	 */
	public void onUpdate(User u) {
		refreshUpdated();
		Event e = c.addEvent("UPDATED", this, u);
		processEvent(e);
	}

	/**
	 *
	 * @param u
	 */
	public void onMove(User u) {
		ADMINCACHE.remove(getID());
		refreshUpdated();
	}

	/**
	 *
	 * @param u
	 */
	public void onDelete(User u) {
		Event e = c.addEvent("DELETED", this, u);
		if (cachedParent != null) {
			cachedParent.processEvent(e);
		} else {
			getParent().processEvent(e);
		}
		refreshUpdated();
	}

	/**
	 *
	 * @return
	 */
	public String flagsToHtml() {
		String out = "";
		if (visibleInFrontEnd()) {
			out += "<label><input type='checkbox' value='" + getID()
					+ "' onclick=\"toggleFlag(this,'f')\" checked>f</label>";
		} else {
			out += "<label><input type='checkbox' value='" + getID() + "' onclick=\"toggleFlag(this,'f')\">f</label>";
		}

		if (visibleForAdmin()) {
			out += "<label><input type='checkbox' value='" + getID()
					+ "' onclick=\"toggleFlag(this,'a')\" checked>a</label>";
		} else {
			out += "<label><input type='checkbox' value='" + getID() + "' onclick=\"toggleFlag(this,'a')\">a</label>";
		}

		if (visibleInSearch()) {
			out += "<label><input type='checkbox' value='" + getID()
					+ "' onclick=\"toggleFlag(this,'s')\" checked>s</label>";
		} else {
			out += "<label><input type='checkbox' value='" + getID() + "' onclick=\"toggleFlag(this,'s')\" >s</label>";
		}
		if (visibleInMenu()) {
			out += "<label><input type='checkbox' value='" + getID()
					+ "' onclick=\"toggleFlag(this,'m')\" checked>m</label>";
		} else {
			out += "<label><input type='checkbox' value='" + getID() + "' onclick=\"toggleFlag(this,'m')\" >m</label>";
		}
		return out;
	}

	/**
	 * ToDo
	 *
	 * @param locale
	 * @return a String with HTML Code to generate a row for a table
	 */
	public String toHTMLROW(Locale locale) {
		String type = getClass().getSimpleName().toLowerCase(Locale.ENGLISH);
		String out;
		out = "<div class='table_row node " + type + "' data-id=" + getID() + ">";

		out += ("<div class='table_cell'>");
		out += (getID());
		out += ("</div>");

		out += ("<div class='table_cell'>");
		out += (getClass().getSimpleName());
		out += ("</div>");

		out += ("<div class='table_cell'>");

		out += getBackLink(locale);

		out += ("</div>");

		out += ("<div class='narrowColumn table_cell'>");
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yy hh:mm", locale);

		// DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM,
		// locale);
		String formattedDate = df.format(new Date(getUpdated()));
		out += formattedDate;
		out += " ";

		out += "(";
		out += getAge(locale);
		out += ")";
		out += ("</div>");
		out += "<div>";
		out += flagsToHtml();
		out += "</div>";

		out += ("<div class='narrowColumn table_cell'>");
		out += ("<button value='" + getID() + "' onclick='removeParent(this)'>remove</button>");
		// out.println("<a href='/admin/category?ID=" + id + "'>edit</a>");

		// out.println("<form action='/admin/product'>");
		// out.println("<input type='hidden' name='ID' value='" + id + "'/>");
		// out.println("<input type='submit' value='edit'/>");
		// out.println("</form>");
		out += ("</div>");
		out += ("</div>");
		return out;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	protected String getAge(Locale locale) {
		return (Tools.getDuration(System.currentTimeMillis() - getUpdated()));
	}

	/**
	 * adds the children of the Node to the table (one by one through the
	 * iterator)
	 *
	 * @param out JSP Writer
	 * @param lang Language
	 * @param ctype Classtype
	 * @throws IOException
	 * @throws Exception
	 */
	public void childrenToHTML(Writer out, Locale lang, Class<?> ctype) throws IOException, Exception {
		Iterator<Node> it = getChildren(ctype).sort("order_id", false).build();

		// iterate through the java resultset
		while (it.hasNext()) {
			Node ccat = it.next();
			// String type = ccat.getType();

			out.write(ccat.toHTMLROW(lang));

		}
	}

	/**
	 *
	 * adds the children of the Node to the table (one by one through the
	 * iterator)
	 *
	 * @param out JSP Writer
	 * @param lang Language
	 * @param ctype Classtype
	 * @param orderby Order by
	 * @throws IOException
	 * @throws Exception
	 */
	public void childrenToHTML(Writer out, Locale lang, Class<?> ctype, String orderby) throws IOException, Exception {

		Iterator<Node> it = getChildren(ctype).sort(orderby, false).build();

		// iterate through the java resultset
		while (it.hasNext()) {
			Node ccat = it.next();
			// String type = ccat.getType();
			out.write(ccat.toHTMLROW(lang));

		}
	}

	/**
	 *
	 * @return UserId
	 */
	public ObjectID getOwnerID() {
		return new ObjectID(doc.get("owner"));
	}

	// redundant?
	/**
	 *
	 * @param u
	 */
	public void incrementOrderID(User u) {
		c.incrementOrderID(this, u);

	}

	// redundant?
	/**
	 *
	 * @param u
	 */
	public void decrementOrderID(User u) {
		c.decrementOrderID(this, u);

	}

	/**
	 *
	 * @param lang Language
	 * @return URL-Path from the parent (downwards)
	 */
	public String getFrontPath(Locale lang) {
		if (parent == null || parent.equals(c.getRootID())) {
			return "/" + lang.getLanguage().toLowerCase(Locale.ENGLISH);
		}
		return getParent().getFrontPath(lang) + "/" + getUrl(lang);

	}

	/**
	 *
	 * @param lang Language
	 * @return the full Url of this Node
	 */
	public String getUrl(Locale lang) {
		Attribute attr = getAttribute("URL");
		if (attr == null) {
			return getName();
		}
		Object v = attr.getValue(lang);
		if (v == null) {
			return getName();
		}

		return v.toString();
	}

	/**
	 *
	 * @return the URL-Path from the parent (downwards)
	 */
	public String getFrontPath() {
		if (parent == null || "CATEGORIES".equals(getName())) {
			return "";
		}
		try {
			return getParent().getFrontPath() + "/" + getName();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);

			return getName();
		}

	}

	/**
	 * ??
	 *
	 * @return
	 */
	public String getBackURL() {
		return "/admin/" + getClass().getSimpleName().toLowerCase(Locale.ENGLISH) + "?ID=" + getID();
	}

	/**
	 * ??
	 *
	 * @param lang
	 * @return
	 */
	public String getBackLink(Locale lang) {
		return ("<a tabindex='-1' href='" + getBackURL() + "'>" + getName() + "</a>");
	}

	/**
	 * ToDo
	 *
	 * @param lang Language (unused)
	 * @return the caption from a Node as a String
	 */
	public String getCaption(Locale lang) {
		Attribute caption = getMyAttribute("caption");
		if (caption == null) {
			return getName();
		}
		Object v = caption.getValue(lang);
		if (v == null) {
			return getName();
		}
		return v.toString();
	}

	/**
	 * Sets a caption to a Node
	 *
	 * @param caption
	 * @param locale
	 * @param u User
	 * @return
	 */
	public Node setCaption(String caption, Locale locale, User u) {
		doc.put("caption", new Document(locale.getLanguage(), caption));
		return this;
	}



	/**
	 * ToDo
	 *
	 * @param name
	 * @param u User
	 * @return
	 */
	public int removeChild(String name, User u) {
		return c.delete(getChild(name), u);
	}

	boolean loaded = false;

	/**
	 *
	 *
	 * @param u User
	 * @return The edited Node
	 */
	public Node commit(User u) {
		c.updateNode(u, this);
		return this;

	}

	// versteh ich nicht //-----------------------------------------------//
	/**
	 *
	 * @return
	 */
	public boolean visibleInSearch() {
		return (flags & (1 << 1)) != 0;
	}

	/**
	 *
	 * @param v
	 */
	public void setVisibleInSearch(boolean v) {
		if (v) {
			flags = flags | (1 << 1);
		} else {
			flags = flags & ~(1 << 1);
		}
	}

	/**
	 *
	 * @return
	 */
	public boolean visibleInFrontEnd() {
		return (flags & (1 << 2)) != 0;
	}

	/**
	 *
	 * @param v
	 */
	public void setVisibleInFrontend(boolean v) {
		if (v) {
			flags = flags | (1 << 2);
		} else {
			flags = flags & ~(1 << 2);
		}
	}

	/**
	 *
	 * @return
	 */
	public boolean visibleForAdmin() {
		boolean vfa = (flags & (1 << 3)) != 0;
		return vfa;
	}

	/**
	 *
	 * @param v
	 */
	public void setVisibleForAdmin(boolean v) {
		if (v) {
			flags = flags | (1 << 3);
		} else {
			flags = flags & ~(1 << 3);
		}
	}

	/**
	 *
	 * @return
	 */
	public boolean visibleInMenu() {
		boolean vfa = (flags & (1 << 4)) != 0;
		return vfa;
	}

	/**
	 *
	 * @param v
	 */
	public void setVisibleInMenu(boolean v) {
		if (v) {
			flags = flags | (1 << 4);
		} else {
			flags = flags & ~(1 << 4);
		}
	}
	// -------------------------------------------------------------------//

	/**
	 *
	 * @param lang Language
	 * @return a JSON Object with every information of the Node
	 */
	public JSONObject toJSON(Locale lang) {
		JSONObject jo = new JSONObject();
		Set<String> keys = doc.keySet();
		for (String key : keys) {
			Object value = doc.get(key);
			if (value instanceof Document) {
				jo.put(key, ((Document) value).get(lang.getLanguage()));
			} else {
				jo.put(key, value);
			}
		}
		return jo;
	}

	/**
	 *
	 * @return
	 */
	public JSONObject toJSON() {
		JSONObject jo = new JSONObject();
		jo.put("_id", doc.get("_id"));
		jo.put("parent", doc.get("parent"));
		jo.put("name", doc.get("name"));
		jo.put("type", doc.get("type"));
		jo.put("owner", doc.get("owner"));
		return jo;
	}

	/**
	 *
	 * @param type Class-type
	 * @return the sum of all Children of this Node (with the given Type)
	 */
	public long getChildrenCount(Class<?> type) {
		return c.getChildrenCount(getID(), type);
	}

	/**
	 *
	 * @return the sum of all Children of this Node
	 */
	public long getChildrenCount() {
		return c.getChildrenCount(getID());
	}

	/**
	 *
	 * @return
	 */
	public int getCID() {
		Integer cid = (Integer) doc.get("cid");
		if (cid == null) {
			return 1;
		}
		return cid;
	}

	// -----------------/
	// -----ToDO------//
	/**
	 *
	 * @param field
	 * @param value
	 * @return
	 */
	public Iterator<Node> findChild(String field, String value) {
		return null;
	}

	/**
	 *
	 * @param type
	 * @param field
	 * @param value
	 * @return
	 */
	public Iterator<Node> findChild(String type, String field, String value) {
		return null;
	}

	/**
	 *
	 * @param field
	 * @param value
	 * @return
	 */
	public static Iterator<Node> find(String field, String value) {
		return null;
	}

	/**
	 *
	 * @param type
	 * @param field
	 * @param value
	 * @return
	 */
	public static Iterator<Node> find(String type, String field, String value) {
		return null;
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static Node find(int id) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return null;

	}
	// --------------//

	// ----------------------------------------------------------//
	/**
	 *
	 * @param name Name we want to set
	 * @return the Node with the new Name
	 */
	public Node setName(String name) {
		doc.put("name", name);
		return this;
	}

	/**
	 *
	 * @return the involved User of this Node
	 */
	public User getOwner() {
		Node n = c.getNode(getOwnerID());
		if (n instanceof User) {
			return (User) n;
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public int hashCode() {
		return getID().hashCode();
	}

	/**
	 *
	 * @param obj
	 * @return
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node) {
			return ((Node) obj).getID().equals(getID());
		}
		return super.equals(obj);
	}

	/**
	 *
	 * @param locale
	 * @return the Front-URL of this Node
	 */
	// (Was ist der Front Link?)
	// Front-link ist die URL für das Frontend
	public String getFrontLink(Locale locale) {
		if ("CATEGORIES".equals(getName())) {
			return "/" + locale.getLanguage();
		}
		Node p = getParent();
		if (p == null) {
			return "/" + locale;
		}
		return getParent().getFrontLink(locale) + "/" + getUrl(locale);
	}

	/**
	 *
	 * @param name Name of the Children we want to get
	 * @return an NodeIterator Object with every Child of this Node
	 */
	public NodeIterator getAllChildren(String name) {
		return c.getAllChildren(this, name);
	}

	// // ToDo?
	// public void generateAdminTemplate(User u) {
	// Node t = null;
	// try {
	// t = getChild("admin." + getClass().getName());
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// Log.error(e);
	// }
	// if (t != null)
	// throw new RuntimeException("templateexists");
	// LinkedHashMap<String, Attribute> attrs = getAttributes(true);
	// Iterator<Attribute> it = attrs.values().iterator();
	// HashMap<String, String> groups = new HashMap<>();
	// groups.put("default", "");
	// while (it.hasNext()) {
	// Attribute a = it.next();
	// String name = a.getName();
	// String group = "default";
	// if (name.contains("_")) {
	// group = name.substring(0, name.indexOf("_"));
	// }
	// String groupvalue = groups.get(group);
	// if (groupvalue == null)
	// groupvalue = "";
	// groupvalue += "\t<div class='" + name + "'></div>\n";
	// groups.put(group, groupvalue);
	// }
	// String tmp = "";
	//
	// Iterator<String> groupsit = groups.keySet().iterator();
	// while (groupsit.hasNext()) {
	// String key = groupsit.next();
	// String group = groups.get(key);
	// tmp += "<div class='" + key + "'>\n";
	// tmp += group;
	// tmp += "</div>\n";
	// }
	// Template template = new Template(u.getID(), c);
	// template.setValue(tmp);
	// template.setName("admin." + getClass().getName());
	// addChild(template, u);
	//
	// }
	/**
	 *
	 * @return
	 */
	public boolean canInherit() {
		return true;
	}

	/**
	 *
	 * @param n
	 * @return
	 */
	public boolean contains(Node n) {
		while (n != null) {
			n = n.getParent();
			if (n == null) {
				break;
			}
			if (n.equals(this)) {
				return true;
			}

		}
		return false;
	}

	/**
	 *
	 * @param name
	 * @param value
	 * @return
	 */
	public Node setAttribute(String name, Object value) {
		doc.put(name, value);
		return this;
	}

	/**
	 *
	 * @param template
	 * @return
	 */
	public String getRelativeName(String template) {
		return getName();
	}

	/**
	 *
	 * @param template
	 * @param locale
	 * @return
	 */
	public String getFilledNTemplate(String template, Locale locale) {
		Template tcat = getTemplate(template);
		template = tcat.getValue();
		Pattern pattern = Pattern.compile("\\{\\{(.*?)\\}\\}");
		Matcher matcher = pattern.matcher(template);
		while (matcher.find()) {
			String group = matcher.group();
			group = group.substring(2, group.length() - 2);
			Attribute attr = getAttribute(group);
			if (attr != null) {
				template = template.replaceAll(Pattern.quote("{{" + group + "}}"), attr.toHTML(locale));
			}
		}

		// try {
		// LinkedHashMap<String, Attribute> attrs = getAttributes(false);
		// Iterator<String> it = attrs.keySet().iterator();
		// while (it.hasNext()) {
		// Attribute attr = attrs.get(it.next());
		// template = template.replaceAll(Pattern.quote("{{" + attr.getName() + "}}"),
		// attr.toHTML(locale));
		// }
		//
		// DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, locale);
		// String formattedDate = df.format(new Date(getCreated()));
		//
		// template = template.replaceAll(Pattern.quote("{{created_timestamp}}"),
		// formattedDate);
		// template = template.replaceAll(Pattern.quote("{{name}}"), getName());
		// Matcher m = Pattern.compile("\\{(.*?)\\}").matcher(template);
		// if (template.contains("{children}")) {
		// StringBuilder children = new StringBuilder();
		// NodeIterator itc = getChildren().build();
		// while (itc.hasNext()) {
		// Node child = itc.next();
		// String childtemplate = child.getFilledTemplate(locale);
		// if (childtemplate != null)
		// children.append(childtemplate);
		// }
		// template = template.replace("{children}", children);
		// }
		//
		// while (m.find()) {
		// String s = "{" + m.group(1) + "}";
		// String child;
		// String childtemplate;
		// try {
		// JSONObject jo = new JSONObject(s);
		// child = jo.getString("child");
		// childtemplate = jo.getString("template");
		//
		// } catch (JSONException e) {
		// continue;
		// }
		// Node n = getChild(child);
		// String ft = n.getFilledTemplate(childtemplate, locale);
		// template = template.replaceAll(Pattern.quote(s), ft);
		// }
		//
		// } catch (Exception e) {
		// Log.error(e);
		// template += e;
		// }
		return template;
	}

	/**
	 *
	 * @param template
	 * @param locale
	 * @return
	 */
	public String getFilledTemplate(Template template, Locale locale) {
		if (doc.containsKey("filledtemplate")) {
			Document d= (Document) this.doc.get("filledtemplate");
			if (d.containsKey(locale.getLanguage())) {
				return d.getString(locale.getLanguage());
			}
		}
		String filledtemplate = template.fill(this, locale);
		setAttribute("filledtemplate", filledtemplate, locale).commit(null);
		return filledtemplate;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	public String getFilledNTemplate(Locale locale) {
		if (doc.containsKey("filledtemplate")) {
			return ((Document) doc.get("filledtemplate")).getString(locale.getLanguage());
		}
		String filledtemplate = getFilledNTemplate(getClass().getName(), locale);
		setAttribute("filledtemplate", filledtemplate, locale).commit(null);
		return filledtemplate;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	public String getCreatedString(Locale locale) {
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, locale);
		String formattedDate = df.format(new Date(getCreated()));

		return formattedDate;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	public String getUpdatedString(Locale locale) {
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, locale);
		String formattedDate = df.format(new Date(getCreated()));

		return formattedDate;
	}

	/**
	 *
	 * @return
	 */
	public boolean isEmpty() {
		return getChildrenCount() == 0;
	}

	/**
	 *
	 * @return
	 */
	public String getType() {
		return getClass().getName();
	}

	/**
	 *
	 * @param u
	 */
	public void saveMe(User u) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 * @param u
	 */
	public void save(User u) {
		c.updateNode(u, this);
	}

	/**
	 *
	 * @param u
	 * @return
	 */
	public Node setOwner(User u) {
		doc.put("owner", u.getID().get());
		return this;
	}

	/**
	 *
	 * @param u
	 * @return
	 */
	public boolean canUpdate(User u) {
		if (u == null) {
			return true;
		}
		if (u.isInGroup(getGroup())) {
			return true;
		} else if (u.isAdmin()) {
			return true;
		}
		if (doc.containsKey("owner")) {
			return u.getID().get().equals(doc.get("owner"));
		}
		return false;
	}

	private List getGroup() {
		return (List) doc.get("group");
	}

	/**
	 *
	 */
	public void onAccess() {
		c.accessNode(this);
	}

	/**
	 *
	 * @return
	 */
	public String getPreviousValue() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @return
	 */
	public String getNewValue() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *
	 * @param name
	 * @param value
	 * @param u
	 */
	public void addToSet(String name, String value, User u) {
		c.addToSet(this, name, value, u);
	}

	/**
	 *
	 * @param name
	 * @param value
	 * @param u
	 */
	public void removeFromSet(String name, String value, User u) {
		c.removeFromSet(this, name, value, u);
	}

	/**
	 *
	 * @param name
	 * @param value
	 * @param u
	 */
	public void addToList(String name, String value, User u) {
		c.addToList(this, name, value, u);
	}

	/**
	 *
	 * @param name
	 * @param value
	 * @param u
	 */
	public void removeAllFromList(String name, String value, User u) {
		c.removeAllFromList(this, name, value, u);
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Node removeAttr(String name) {
		doc.remove(name);
		return this;
	}

	/**
	 *
	 * @param name
	 * @param u
	 */
	public void deleteAttr(String name, User u) {
		c.deleteAttr(this, name, u);
	}

	/**
	 *
	 * @param name
	 * @param u
	 * @return
	 */
	public Attribute addAttr(String name, User u) {
		if (doc.containsKey(name)) {
			return getMyAttribute(name);
		}
		c.addAttr(this, name, u);
		doc.put(name, "");
		return getMyAttribute(name);
	}

	/**
	 *
	 * @param pi
	 */
	public void getChildrenTemplates(ProcessorInterface pi) {
		String templatepredix = pi.getParameter("template_prefix");

		NodeIterator<Node> it = getChildren().sort("cid", false).build();
		while (it.hasNext()) {
			Node child = it.next();
			Template t;
			if (templatepredix == null) {
				t = child.getTemplate();
			} else {
				t = child.getThumbnailTemplate();
			}

			if (t == null) {
				continue;
			}
			String wt = t.getWrapperTag();

			pi.writeResult("<" + wt + " class='" + child.getClass().getSimpleName().toLowerCase(Locale.ENGLISH)
					+ " unfilled' data-id='" + child.getID() + "' data-name='" + child.getName() + "'>");
			pi.writeResult(t.toString());
			pi.writeResult("</" + wt + ">");
		}
	}

	protected final void processEvent(Event e) {
		Iterator<Session> it = getSessions();
		while (it.hasNext()) {
			Session s = it.next();
			s.sendEvent(e);
		}
	}

	public Iterator<Session> getSessions() {
		return getChildren(Session.class);
	}

}
