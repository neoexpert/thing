package web;
import java.io.IOException;
import java.io.Writer;

import com.mongodb.MongoClient;
import com.neoexpert.Log;
import core.Node;
import core.User;
import core.Zombie;
import core.controller.mongo.MongoController2;
import core.id.ObjectID;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import templates.*;

/**
 *
 * @author neoexpert
 */
public class HttpController extends MongoController2 {

	public static URI getWebRootResourceUri(Object o,String path) {
		Log.log(path);
		URL indexUri = o.getClass().getResource("/webroot" + path);
		if (indexUri == null) {
			return null;
		}
		Log.log(indexUri.toString());
		// Points to wherever /webroot/ (the resource) is
		try {
			return indexUri.toURI();
		} catch (URISyntaxException e) {
			Log.error(e);
		}
		return null;
	}
	/**
	 *
	 * @param name
	 * @param out
	 * @param wrapper
	 * @throws IOException
	 */
	public void printDesignBlock(String name, Writer out, boolean wrapper) throws IOException {
		Template t = (Template) getNode("DESIGN").getChild(name);
		if (t != null) {
			if (wrapper) {
				out.write("<div data-id='" + t.getID() + "' class='template unfilled'>");
			}
			out.write(t.toString());
			if (wrapper) {
				out.write("</div>");
			}

		}

	}
	/**
	 *
	 * @param cat
	 * @param lang
	 * @return
	 */
	public PageInfo getFrontPageInfo(Node cat, Locale lang) {
		PageInfo pi = new PageInfo();

		Node categories = getNode("CATEGORIES");

		Node parent = null;
		pi.setRootNodeID(cat.getID());

		ObjectID parentID = cat.getParentID();
		if (parentID != null) {
			parent = getNode(parentID);
		}

		String caption = cat.getCaption(lang);

		String h = "<div class='path'>" + caption + "</div>";
		StringBuilder path = new StringBuilder("../");

		while (parent != null) {
			if (parentID == categories.getID()) {
				break;
			}
			caption = parent.getCaption(lang);

			h = "<span class='path'><a href='" + path + "'>" + caption + "</a> <span id='arrow'>▶</span></span> " + h;
			pi.setRootNodeID(parent.getID());

			parentID = parent.getParentID();
			if (parentID == null) {
				break;
			}

			parent = getNode(parentID);

			path.append(path);
		}
		pi.setPath(h);
		return pi;
	}

	/**
	 *
	 * @param n
	 * @param lang
	 * @return
	 */
	public PageInfo getPathHTML(Node n, Locale lang) {
		PageInfo pi = new PageInfo();

		Node parent;
		if (n == null) {
			throw new RuntimeException("n was null");
		}
		pi.setRootNodeID(n.getID());

		parent = n.getParent();

		String h = "<span class='category_link myname' contentEditable='true'>" + n.getName() + "</span>";
		h += "<button hidden>save</button>";
		h += "<div class='error'></div>";

		if (n.getID() != rootID) {
			while (parent != null && parent.getID() != rootID) {

				h = "<span class='category_link'>" + parent.getBackLink(lang) + " <span id='arrow'>▶</span></span> "
						+ h;
				pi.setRootNodeID(parent.getID());

				parent = parent.getParent();
				if (parent == null) {
					break;
				} else if (parent.getID() == rootID) {
					break;
				}
			}
		}
		pi.setPath(h);
		return pi;
	}


    /**
     *
     * @param mongo
     * @param domain
     * @param lang
     */
    public HttpController(MongoClient mongo, String domain, Locale lang) {
        super(mongo, domain, lang);
    }

    /**
     *
     * @param mongo
     * @param domain
     * @param root
     * @param lang
     */
    public HttpController(MongoClient mongo, String domain, boolean root, Locale lang) {
        super(mongo, domain, root, lang);
    }

    /**
     *
     * @param request
     * @return
     */
    public static Locale getLang(HttpServletRequest request) {
        Locale locale = request.getLocale();
        Object langattr = request.getSession().getAttribute("locale");
        if (langattr != null) {
            locale = (Locale) langattr;
        } else {
            request.getSession().setAttribute("lang", locale);
        }
        if (!langs.contains(locale.getLanguage())) {
            locale = Locale.GERMANY;
        }
        request.setAttribute("locale", locale);
        return locale;
    }

    /**
     *
     * @param session
     * @return
     */
    public static Locale getLang(HttpSession session) {
        Locale langattr = (Locale) session.getAttribute("locale");
        if (langattr != null) {
            if (langs.contains(langattr.getLanguage())) {
                return langattr;
            }
        }
        return Locale.ENGLISH;
    }
    static Set<String> langs = new HashSet<String>() {
        {
            add("de");
            add("en");
            add("pl");
        }
    };

    /**
     *
     * @param request
     * @param lang
     */
    public static void setLang(HttpServletRequest request, Locale lang) {
        if (langs.contains(lang.getLanguage())) {
            request.getSession().setAttribute("locale", lang);
        }
    }

    /**
     *
     * @param session
     * @return
     */
    public User addZombie(HttpSession session) {
        Zombie z = new Zombie(null, this);
        z.setName(session.getId());
        Node zombies = getNode("ZOMBIES");
        if (zombies == null) {
            zombies = new Node(null, this);
            zombies.setName("ZOMBIES");
            addNode(zombies, null);
        }
        
        z.setParent(zombies);
        z = (Zombie) addNode(z, null);

       

        session.setAttribute("GROUP", "ZOMBIE");
        session.setAttribute("UserID", z.getID());

        return z;
    }

    /**
     *
     * @param session
     * @return
     */
    public User getUser(HttpSession session) {
        if (session == null) {
            return null;
        }
        try {
            Object uID = session.getAttribute("UserID");
            Object group = session.getAttribute("GROUP");

            if (group == null || uID == null) {
                return addZombie(session);
            }
            if (group.equals("ZOMBIE")) {
                User z = getZombie(session.getId());
                if (z == null) {
                    return addZombie(session);
                } else {
                    return z;
                }
            }
            return (User) getNode((ObjectID) uID);
        } catch (RuntimeException e1) {
            try {
                return addZombie(session);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.error(e);
            }
        }
        return null;
    }

    private User getZombie(String id) {
        Node zombies = getNode("ZOMBIES");
        return (User) zombies.getChild(id);

    }


	public Node init() {
		Node root = super.init();
		HtmlTemplate htmltemplate = (HtmlTemplate) root.getChild("admin.templates.HtmlTemplate");
		if (htmltemplate == null) {
			htmltemplate = new HtmlTemplate(null, this);
			htmltemplate.setName("admin.templates.HtmlTemplate");
			htmltemplate.setValue("<div class=\"attribute\" data-name=\"value\" data-load=\"edit_code\"></div>");
			root.addChild(htmltemplate, null);
			Log.logGreen("template admin.templatea.HtmlTemplate created");
		}
		return root;
	}
}
