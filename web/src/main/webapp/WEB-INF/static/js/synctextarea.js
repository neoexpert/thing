"use strict"
class DTextArea extends HTMLTextAreaElement {
	constructor()
	{
		super();
	}
  createdCallback() {
	this.cursorpos=0;
    this.tabIndex = 1;
    this.onkeydown = function(e) {
      this.monkeydown(e);
    }
    this.onkeypress = function(e) {
      this.monkeypress(e);
    }
	this.contentEditable=true;

  }
  
  
	setFile(file){
		this.file=file;
		this.innerHTML="";

		var lines=file.lines;
		var l = lines.length;
		for(var i = 0; i < l; i++){
			var div = document.createElement('div');
			div.onclick=this.onlineclick;

			lines[i].htmlElement=div;
			div.innerHTML = lines[i].v;
			if(lines[i].readonly){
				div.style.background="#FF9";
				div.contentEditable=false;
			}
			if(lines[i].locked){
				div.style.background="#FCC";
				div.contentEditable=false;
			}
	    	this.appendChild(div);
	    	// this.appendChild(document.createElement('br'));
		}
		
	}
	currentline(linenr){
		this.getLine(linenr,function(line){
			line.style.background="#CFC";
			line.contentEditable=true;
		});
	}
	freeline(linenr){
		this.getLine(linenr,function(line){
			line.style.background="#FFF";
			line.contentEditable=false;
		});
	}
	lockline(linenr){
		this.getLine(linenr,function(line){
			line.style.background="#FCC";
			line.contentEditable=false;
		});
	}
	
	getLine(linenr,callback){
		var children = this.childNodes;
		var i=0;
		children.forEach(function(child){
			if(i==linenr){
				callback(child);
			}
			i++;
		});
	}
	
	removeLine(line){
		this.removeChild(line);
	}
	
	connectLines(linenr){
		var that=this;
		this.getLine(linenr,function(line){
			var nextline=line.nextSibling;
			line.innerHTML+=nextline.innerHTML;
			that.removeLine(nextline);
		});
	}
	delline(linenr){
		var that=this;
		this.getLine(linenr,function(line){
			that.removeLine(line);
		});
	}
	del(e){
		var startline=e.startline;
		var endline=e.endline;
		var start=e.start;
		var end=e.end;
		if (startline == endline) {
			this.getLine(startline,function(line){
				if(start==end)e.end++;
				line.innerHTML=line.innerHTML.splice(e.start, e.start-e.end, "");
				var textNode = line.firstChild;
				var caret = e.start; // insert caret after the 10th character
										// say
				var range = document.createRange();
				range.setStart(textNode, caret);
				range.setEnd(textNode, caret);
				var sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			});
		} else {
			
			this.getLine(startline,function(line){
				line.innerHTML=line.innerHTML.splice(start, start-line.innerHTML.length, "")
			});
			this.getLine(endline,function(line){
				line.innerHTML=line.innerHTML.splice(0, end, "");
			});
			this.connectLines(startline);
			this.getLine(startline,function(line){
				var textNode = line.firstChild;
				var caret = e.start+1; // insert caret after the 10th character
										// say
				var range = document.createRange();
				range.setStart(textNode, caret);
				range.setEnd(textNode, caret);
				var sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			})
			
			
		}
	}
	onlineclick(e){
		var line=this.parentNode.getLinePosition(e.currentTarget);
		var lockline={};
		lockline.line=line;
		lockline.cmd="lockline";
		ws.send(JSON.stringify(lockline));
	}
	add(e){
		var linenr=e.startline;
		var c=e["char"];
		var that=this;
		this.getLine(linenr,function(line){
			if(c=="\n"){
				var div = document.createElement('div');
				div.onclick=that.onlineclick;
				that.insertBefore(div, line.nextSibling);
				var html=line.innerHTML;
				line.innerHTML=html.splice(e.start,line.innerHTML.length , "");
				div.innerHTML=html.splice(0,e.start , "");
				
				return;
			}
		   
			line.innerHTML=line.innerHTML.splice(e.start, 0, c);
			
		});
	}
	getValue(){
		var value="";
		var children = this.childNodes;
		var that=this;
		children.forEach(function(child){
				if(child.nodeName=="BR"){
					value+="\n";
					return;
				}
				if(child===that.cursor)return;
				value+=child.innerHTML;
		});
		
		return value;
	}

	setWebSocket(ws){
		this.ws=ws;
	}
  attachedCallback() {}
  monkeydown(e) {
	var KeyID = event.keyCode;

  switch (KeyID) {
    case 8:// backspace
	     var sel = document.getSelection();
		 var del={};
		 del.startline=this.getLinePosition(sel.anchorNode);
		 del.endline=this.getLinePosition(sel.extentNode);

		 del.cmd="del";
		 del.line=this.getLinePosition(e.target);
		 del.start=sel.baseOffset;
		 del.end=sel.extentOffset;
		 if(del.start==del.end){
			 del.start--;
			 del.end=del.start;
		 }

		 this.ws.send(JSON.stringify(del));


      break;
    case 46:
    	 // e.preventDefault();
	     var sel = document.getSelection();
         
         
		 var del={};
		 del.startline=this.getLinePosition(sel.anchorNode);
		 del.endline=this.getLinePosition(sel.extentNode);

		 del.cmd="del";
		 del.line=this.getLinePosition(e.target);
		 del.start=sel.baseOffset;
		 del.end=sel.extentOffset;
		 if(del.start==del.end){
			 del.start;
			 del.end=del.start;
		 }

		 this.ws.send(JSON.stringify(del));


      break;
			
    case 13:
	    // e.preventDefault();
	    // var span = document.createElement('span');
		// span.onclick=this.characterClick;
	    var c=String.fromCharCode(e.which);
	    // span.innerHTML = c;

	    var add={};
	    var sel = document.getSelection();
	  	add.startline=this.getLinePosition(sel.anchorNode);
		add.endline=this.getLinePosition(sel.extentNode);
		add.start=sel.baseOffset;
		add.end=sel.extentOffset;
	    add.cmd="add";
	    add["char"]="\n";
	    this.ws.send(JSON.stringify(add));
	    
      // this.cursor.before(br);
      break;

    default:
      break;
  }
	}
  getCursorPosition(){
	  var i=0;
	  var child=this.cursor;
	  while( (child = child.previousSibling) != null ) 
		   i++;
	  return i;
  }
  getLinePosition(line){
	  if(line.nodeType==Node.TEXT_NODE)
		  line=line.parentNode;
	  if(line===this)
		  return -1;
	  var i=0;
	  while( (line = line.previousSibling) != null ) 
		   i++;
	  return i;
  }
	characterClick(e){
		 this.parentNode.cursor.parentNode.insertBefore(this.parentNode.cursor, e.target);
	}
  monkeypress(e) {
    // var span = document.createElement('span');
	// span.onclick=this.characterClick;
    var c=String.fromCharCode(e.which);
    // span.innerHTML = c;

    var add={};
    var sel = document.getSelection();
  	add.startline=this.getLinePosition(sel.anchorNode);
	add.endline=this.getLinePosition(sel.extentNode);
	add.start=sel.baseOffset;
	add.end=sel.extentOffset;
    add.cmd="add";
    add["char"]=c;
    this.ws.send(JSON.stringify(add));
    // this.cursor.parentNode.insertBefore(span, this.cursor);
  }
	monclick(e){
var target = e.target; // Clicked element
    while (target && target.parentNode !== this) {
        target = target.parentNode; // If the clicked element isn't a direct
									// child
        if(!target) { return; } // If element doesn't exist
    }
    if (target.tagName === 'SPAN'){
        alert(target.id); // Check if the element is a LI
    }
	}
}