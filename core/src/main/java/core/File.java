package core;

import com.neoexpert.Log;
import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import core.controller.Controller;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import org.bson.Document;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

/**
 *
 * @author neoexpert
 */
public class File extends Node implements ErrorReporter {

    /**
     *
     * @param c
	 * @param doc
     */
    public File(Controller c,Document doc) {
		super(c,doc);
	}
    /**
     *
     * @return
     */
    @Override
	public boolean canInherit() {
		// TODO Auto-generated method stub
		return false;
	}
	
    /**
     *
     * @param u
     */
    public void minify(User u){
		if(getName().endsWith(".js")){
			Reader in = new StringReader((String) doc.get("value"));
			try {
				JavaScriptCompressor cc=new JavaScriptCompressor(in, this);
				StringWriter out = new StringWriter();
				cc.compress(out, 0, true, true, true, false);
				out.flush();
				setAttribute("minified", out.toString()).commit(u);
				
			} catch (Exception e) {
				removeChild("minified",u);
				Log.error(e);
			}
		}
		
		if(getName().endsWith(".css")){
			Reader in = new StringReader((String) doc.get("value"));
			try {
				CssCompressor cc=new CssCompressor(in);
				StringWriter out = new StringWriter();
				cc.compress(out,0);
				out.flush();
				setAttribute("minified", out.toString()).commit(u);
				//doc.put("minified", out.toString())
				out.toString();
			} catch (Exception e) {
				System.out.println("minified failed:"+e.toString() );
				removeChild("minified",u);
				Log.error(e);
			}
		}
	}
	
    /**
     *
     * @param u
     */
    @Override
	public void onUpdate(User u) {
		

		super.onUpdate(u);
	}
	
    /**
     *
     * @return
     */
    public String getContent() {
		Object m = doc.get("minified");
		if(m==null)
		return (String) doc.get("value");
		else
			return m.toString();
	}
	
    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @param arg4
     */
    @Override
	public void error(String arg0, String arg1, int arg2, String arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @param arg4
     * @return
     */
    @Override
	public EvaluatorException runtimeError(String arg0, String arg1, int arg2, String arg3, int arg4) {
		// TODO Auto-generated method stub
		return null;
	}

    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @param arg4
     */
    @Override
	public void warning(String arg0, String arg1, int arg2, String arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}
}
