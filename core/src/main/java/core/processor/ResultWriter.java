package core.processor;

import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public interface ResultWriter {

    /**
     *
     * @param result
     */
    void write(String result);

    /**
     *
     * @param result
     */
    void write(JSONObject result);

}
