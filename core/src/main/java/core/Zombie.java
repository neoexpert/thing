package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Zombie extends User {

	/**
     *
     * @param c
	 * @param doc
     */
    public Zombie(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Zombie(ObjectID userID, Controller con) {
		super(userID, con);
	}

}
