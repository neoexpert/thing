package types;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;
import org.json.JSONObject;
import templates.Template;

/**
 *
 * @author neoexpert
 */
public class Collection extends Primitive {

	/**
	 *
	 */
	public static final int TYPE_COLLECTION = 4;

	/**
     *
     * @param c
	 * @param doc
     */
    public Collection(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Collection(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public int getValueType() {
		return TYPE_COLLECTION;
	}

	/**
	 *
	 * @return
	 */
	public int getElementValueType() {
		if (doc.containsKey("value_type")) {
			return (int) doc.get("value_type");
		}
		return TYPE_STRING;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public JSONObject getInfo() {
		JSONObject jo = new JSONObject();
		Template t = (Template) getChild("template");
		if (t != null) {
			jo.put("template", t.getValue());
		}
		t = (Template) getChild("element_template");
		if (t != null) {
			jo.put("element_template", t.getValue());
		}

		jo.put("multilang", isMultilang());
		jo.put("value_type", getValueType());
		return jo;
	}
}
