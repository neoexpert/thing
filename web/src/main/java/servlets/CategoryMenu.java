package servlets;

import com.neoexpert.Log;
import core.Node;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CategoryView
 */
@WebServlet("/category-menu")
public class CategoryMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CategoryMenu() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		response.setCharacterEncoding("UTF-8");

		request.setAttribute("controller", c);
		HttpSession session = request.getSession();
		
		Locale locale = HttpController.getLang(request);
		String lang = locale.getLanguage();
		try {
			response.setContentType("text/html");

			PrintWriter out = response.getWriter();
			Node cat = c.getNode("CATEGORIES");
			Iterator<Node> it = cat.getChildren().sort("order_id", false).build();

			String active = "";
			// iterate through the java resultset
			while (it.hasNext()) {
				Node ccat = it.next();
				if (!ccat.visibleInMenu())
					continue;
				

				String url = cat.getName();
				try {
					url = ccat.getUrl(locale);
				} catch (Exception e) {
					Log.error(e);
				}
				String name;
				try {
					name = (String) ccat.getCaption(locale);
				} catch (Exception e) {
					name = url;
				}
				out.println("<div class='node' level=0 id='" + ccat.getName() + "'" + active + " value=" + ccat.getID()
						+ "><a href='/" + lang + "/" + url + "/' onclick=\"loadArea('/" + lang + "/" + url
						+ "/',this);return false;\">" + name + "</a></div>");

				/*
				 * if (Controller.locURL) out.println("<li id='" +
				 * ccat.getName() + "'" + active + "><a href='/" + lang + "/" +
				 * url + "/'>" + name + "</a></li>"); else
				 * out.println("<li id='" + ccat.getName() + "'" + active +
				 * "><a href='/" + url + "/'>" + name + "</a></li>");
				 */

			}
			out.println("<script>initMenu();</script>");

			// }

		} catch (IOException | RuntimeException e) {
			Log.error(e);
		}

	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
