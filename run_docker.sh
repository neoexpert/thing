if ! command -v git >/dev/null; then
	echo "git is not installed"
	exit 1
fi
echo "git pull ..."
git pull &&
if ! command -v mvn >/dev/null; then
	echo "maven is not installed"
	exit 2
fi
mvn clean install -DskipTests=true  &&
#cp web/target/thing.war docker/data/tomcat/ROOT.war &&
if ! command -v docker-compose > /dev/null; then
	echo "docker-compose is not installed"
	exit 3
fi
cd docker/java &&
sudo docker-compose up -d &&
if command -v xgd-open >/dev/null; then
xdg-open http://localhost:8080
fi
