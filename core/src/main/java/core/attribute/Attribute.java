package core.attribute;

import core.Node;
import core.User;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Locale;
import org.bson.Document;
import org.json.JSONObject;
import types.Primitive;
import types.Types;

/**
 *
 * @author neoexpert
 */
public class Attribute {

    private final Node parent;
    private final String name;
    private Object value;
    private final Controller c;
    private boolean inherited;

    /**
     *
     * @param name
     * @param parent
     * @param c
     */
    public Attribute(String name, Node parent, Controller c) {
        this.name = name;
        this.parent = parent;
        value = parent.doc.get(name);
        this.c = c;
    }

    /**
     *
     * @return
     */
    public Object getValue() {
        return value;
    }

    /**
     *
     * @param lang
     * @return
     */
    public Object getValue(Locale lang) {
        if (lang == null) {
            return getValue();
        }
        if (value instanceof Document) {
            Object v = ((Document) value).get(lang.getLanguage());
            if (v == null) {
                v = ((Document) value).get("de");
            }
            if (v == null) {
                return "";
            }
            return v;
        }
        if (value == null) {
            return "";
        }
        return value;
    }

    /**
     *
     * @param lang
     * @return
     */
    public JSONObject toJSON(Locale lang) {
        JSONObject jo = new JSONObject();
        jo.put("parent", parent.getID());
        jo.put("name", name);

        Types types;
        types = (Types) c.getTypes();
        Primitive type = types.getType(name);
        if (type == null) {
            jo.put("value", getValue());
        } else {
            jo.put("value", type.valueOf(this, lang));

            jo.put("type", type.getInfo().put("caption", type.getCaption(lang)));
        }
        return jo;
    }

    /**
     *
     * @param lang
     * @return
     */
    public JSONObject toJsonAdmin(Locale lang) {
        JSONObject jo = new JSONObject();
        jo.put("parent", parent.getID());
        jo.put("name", name);
        jo.put("inherited", inherited);

        if (name.equals("URL")) {
            getClass();
        }

        Primitive type = getType();

        if (type == null) {
            jo.put("value", getValue());
        } else {
            jo.put("value", type.valueOf(this, lang));

            jo.put("type", type.getAdminInfo().put("caption", type.getCaption(lang)));
        }
        return jo;
    }

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked")

    public Primitive getType() {
        Types types = c.getTypes();
        if (name.startsWith("_")) {
            return (Primitive) types.getChild(name.substring(0, name.indexOf('_')));
        } else {
            return types.getType(name);
        }
    }

    /**
     *
     * @return
     */
    public Attribute setInherited() {
        inherited = true;
        return this;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param locale
     * @return
     */
    public String toHTML(Locale locale) {
        return getValue(locale).toString();
    }

    /**
     *
     * @return
     */
    public boolean multiLang() {
        return false;
    }

    /**
     *
     * @return
     */
    public boolean hasParent() {
        return false;
    }

    /**
     *
     * @return
     */
    public int getOrderID() {
        return 0;
    }

    /**
     *
     * @param lang
     * @return
     */
    public String getCaption(Locale lang) {
        return name;
    }

    /**
     *
     * @return
     */
    public ObjectID getParentID() {
        return parent.getID();
    }

    /**
     *
     * @return
     */
    public boolean isConfigurable() {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     *
     * @param confs
     */
    public void setConfiguration(JSONObject confs) {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     *
     * @param value
     * @param locale
     * @param u
     */
    public void setValue(String value, Locale locale, User u) {
        Primitive type = getType();
        if (type == null) {
            parent.doc.put(name, value);
            parent.commit(u);
        } else {
            type.setValueOf(this, value, locale, u);
        }
    }

    /**
     *
     * @return
     */
    public Node getParent() {
        return parent;
    }

    public void setValue(Object v, User u) {
				if(isMultilang())
					throw new RuntimeException("name is a multilangual attribute");
        parent.doc.put(name, v);
        parent.commit(u);
				this.value=v;
    }

    /**
     *
     * @return
     */
    public boolean isMultilang() {
				Primitive type=getType();
        return type==null?false:type.isMultilang();
    }

}
