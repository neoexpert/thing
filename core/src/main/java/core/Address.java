package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Address extends Node {

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Address(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public Address(ObjectID userID, Controller c) {
		super(userID, c);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		return false;
	}
}
