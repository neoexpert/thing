
import com.sun.org.apache.bcel.internal.generic.LSTORE;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author neoexpert
 */
public class WS extends WebSocketAdapter {

	private final HttpSession httpsession;

	private Integer id;
	private static int last_id = 1;
	private static final Map<String, Integer> sessionid = new HashMap<>();
	private static final ArrayList<String> lastmessages = new ArrayList<>();
	private final int color;

	public WS(HttpSession session) {
		String sessid = session.getId();
		this.httpsession = session;
		id = sessionid.get(sessid);
		if (id == null) {
			id = last_id;
			sessionid.put(sessid, id);
			last_id++;
		}
		this.color=new Random(sessid.hashCode()).nextInt();
	}
	public Session session;
	public static List<WS> clients = new ArrayList<>();

	@Override
	public void onWebSocketConnect(Session sess) {
		super.onWebSocketConnect(sess);

		System.out.println("Socket Connected: " + sess);
		this.session = sess;
		clients.add(this);
		try {
			for (WS ws : clients) {
				JSONObject jo = new JSONObject();
				jo.put("online", clients.size());

				ws.session.getRemote().sendString(jo.toString());
			}
			for (String m : lastmessages) {
				session.getRemote().sendString(m);
			}

		} catch (IOException ex) {
			Logger.getLogger(WS.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void onWebSocketText(String message) {
		if (lastmessages.size() > 1000) {
			if (lastmessages.size() > 1000) {
				lastmessages.remove(0);
			}
		}
		super.onWebSocketText(message);
		JSONObject jo = new JSONObject();
		jo.put("message", message);
		jo.put("client", id);
		jo.put("color", color);

		try {

			String m = jo.toString();
			lastmessages.add(m);
			for (int i = 0; i < clients.size(); i++) {
				WS ws = clients.get(i);
				ws.session.getRemote().sendString(m);
			}
		} catch (IOException ex) {
			Logger.getLogger(WS.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void onWebSocketClose(int statusCode, String reason) {
		super.onWebSocketClose(statusCode, reason);
		System.out.println("Socket Closed: [" + statusCode + "] " + reason);
		clients.remove(this);
	}

	@Override
	public void onWebSocketError(Throwable cause) {
		super.onWebSocketError(cause);
		cause.printStackTrace(System.err);
	}
}
