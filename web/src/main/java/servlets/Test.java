package servlets;

import com.neoexpert.Log;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import shop.Order;
import templates.HtmlTemplate;

/**
 * Servlet implementation class Path
 */
@WebServlet("/test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		PrintWriter out = response.getWriter();
		out.println("<a href='https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&runame=Alexander_Tyury-Alexande-Test-P-hgtzx&oauthparams=%26state%3Dnull%26client_id%3DAlexande-Test-PRD-f2466ad41-daaad47d%26redirect_uri%3DAlexander_Tyury-Alexande-Test-P-hgtzx%26response_type%3Dcode%26device_id%3Dnull%26display%3Dnull%26scope%3Dhttps%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.marketing.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.marketing+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.inventory.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.inventory+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.account.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.account+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.fulfillment.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.fulfillment+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.analytics.readonly%26tt%3D1'>login</a>");
		//new EbayClient("","",out).testIt(out);
		Order n;
		try {
			n = (Order) c.getNode("ORDERS").getChild("test_order");
			HtmlTemplate temp = (HtmlTemplate) n.getTemplate("MAIL");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().append(temp.fill(n,request.getLocale()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		
		
//		MongoClient mongo = new MongoClient("localhost", 27017);
//		MongoDatabase shop = mongo.getDatabase("shop");
//		MongoCollection<Document> nodes = shop.getCollection("nodes");
//		FindIterable<Document> huhu = nodes.find().limit(20);
//		for(Document h:huhu){
//			response.getWriter().append(h.toString());
//		}
//		
//		mongo.close();
		// Create a File object on the root of the directory containing the class file
		java.io.File file = new java.io.File("c:\\myclasses\\");

		try {
		    // Convert File to a URL
		    URL url = file.toURI().toURL();        // file:/c:/myclasses/
		    URL[] urls = new URL[]{url};

		    // Create a new class loader with the directory
		    ClassLoader cl = new URLClassLoader(urls);
		    Class classToLoad = Class.forName ("com.MyClass", true, cl);
		    Method method = classToLoad.getDeclaredMethod ("myMethod");
		    Object instance = classToLoad.newInstance ();
		    Object result = method.invoke (instance);

		    // Load in the class; MyClass.class should be located in
		    // the directory file:/c:/myclasses/com/mycompany
		    Class cls = cl.loadClass("com.mycompany.MyClass");
		} catch (MalformedURLException e) {
		} catch (ClassNotFoundException e) {
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
	}

}
