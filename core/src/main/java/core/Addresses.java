package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Addresses extends Node {

	static {
		addChildType(Addresses.class, Address.class);
	}

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public Addresses(ObjectID userID, Controller c) {
		super(userID, c);
	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Addresses(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		return false;
	}
}
