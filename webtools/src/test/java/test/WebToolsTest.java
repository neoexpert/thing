package test;

import core.*;
import com.neoexpert.Log;
import core.attribute.Attribute;
import core.controller.*;
import java.util.Locale;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.*;
import org.junit.runners.MethodSorters;
import templates.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebToolsTest {
	public interface Logger{
		public void log(String s);
		public void info(String s);
		public void ok();
		public void error(String s);
		public void error(Throwable t);
	}
	private static Logger logger;
	public static void setLogger(Logger trl){
		logger=trl;	
	}	
	public static void createConsoleLogger(){
		logger=new Logger(){
			@Override
			public void log(String s){
				System.out.println(s);
			}
			@Override
			public void info(String s){
				System.out.println("[ " + b(s) + " ]");
			}
			@Override
			public void ok(){
				System.out.println("[ " + g("OK") + " ]");
			}
			@Override
			public void error(String s){
				System.out.print("[ " + r("ERROR") + " ] ");
				System.out.println(s);
			}
			@Override
			public void error(Throwable t){
				System.out.print("[ " + r("ERROR") + " ] ");
				t.printStackTrace(System.out);
			}
		};	
	}
	private static void _fail(String s){
		logger.error(s);
		fail(s);
	}

	@Test
	public void test7_templates() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		logger.info("TEMPLATES TEST");
		logger.log("getting test Node...");
		Node test=c.getRoot().getChild("test");		
		assertNotNull("test node not found",test);
		test.setOwner(u).commit(u);
		logger.ok();
		Attribute a=test.addAttr("test_attribute",u);
		
		test.deleteAttr("test_attribute",u);
		assertNotNull("addAttr returned null",a);
		a.setValue("test_value",u);
		HtmlTemplate t=new HtmlTemplate(u.getID(),c);
		t.setValue("<div class=\"attribute\" data-name=\"test_attribute\"></div>");
		t.setName("core.Node");
		test.addChild(t,u);
		String ft=test.getFilledTemplate(test.getTemplate(),Locale.ENGLISH);
		Log.logGreen(ft);
		assertTrue(ft.contains("test_value"));
		assertTrue(ft.contains("data-parent"));
		assertTrue(ft.contains(test.getID().get().toString()));

	}

	private static Controller c;
	private static User u;

	private static long starttime;
	@BeforeClass
	public static void getController() {
		starttime=System.currentTimeMillis();
		if(logger==null)
			createConsoleLogger();
		logger.info("THING TEST");
		logger.log("getController...");
		try{
		c= new ControllerBuilder()
				.setDB("test")
				.setDomain("test")
				.setLocale(Locale.ENGLISH)
				.getController();
		}
		catch(RuntimeException e){
			logger.error(e);
		//	assumeTrue(false);
		}
		//assumeTrue(c.isConnected());
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}

		logger.info("INIT DB");
		c.init();
		logger.ok();

		logger.log("check if test user.exists...");
		u = c.getUser("test");
		if (u != null) {
			logger.log("test user exists! removing...");

			c.remove(u, u);
			logger.ok();
		}
		else logger.ok();
		logger.log("adding User...");
		u = c.addUser("test", "test");
		System.err.println("here we are");
		if (u == null) {
			_fail("user not exits");
		} else {
			logger.ok();
		}
		u.setAttribute("email_confirmed", true).commit(u);
		assertTrue((Boolean)u.getAttribute("email_confirmed").getValue());
		logger.log("adding test node to ROOT...");
		Node test = new Node(u.getID(), c);
		test.setName("test");
		c.getRoot().addChild(test, u);
		logger.ok();
		
	}

	@AfterClass
	public static void a_delete_test_Nodes() {
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
		
		logger.log("delete test node recursively... ");
		int deleted=c.delete(c.getRoot().getChild("test"), u);
		logger.log("deleted nodes: "+deleted);
		logger.ok();
	}

	@AfterClass
	public static void b_deleteTestUser(){
		if(c==null){
			Log.warn("COULD NOT CONNECT TO.DB");
			return;
		}
			logger.log("delete User 'test'");

			c.remove(u, u);
			logger.ok();
			long duration=System.currentTimeMillis()-starttime;
			logger.info("test done in "+duration/1000+" S");
			logger=null;
	}

	private static String r(String s) {
		return "\033[31;1m" + s + "\033[0m";
	}

	private static String g(String s) {
		return "\033[32;1m" + s + "\033[0m";
	}

	private static String b(String s) {
		return "\033[34;1m" + s + "\033[0m";
	}
}
