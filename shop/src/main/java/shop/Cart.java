package shop;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Iterator;
import org.bson.Document;

/**
 *
 * Shopping Cart, handles all the necessary informations such as adding,
 * deleting, getting the amount of added items and informations about the
 * existence of items inside of the cart, at all.
 *
 * @author neoexpert
 */
public class Cart extends Node {

	static {
		addChildType(User.class, Cart.class);
		addChildType(Cart.class, Item.class);
	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Cart(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Cart(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
	 *
	 * @param itemID
	 * @param amount
	 */
	public void setAmount(int itemID, int amount) {
	}

	/**
	 *
	 * @param name
	 * @return boolean - empty or filled?
	 * @throws Exception
	 */
	public boolean hasItem(String name) throws Exception {
		return c.nodeExists(getID(), name);
	}

	/**
	 *
	 * @param id = Item Id
	 * @param u = User which is calling the action
	 * @return boolean
	 * @throws Exception
	 */
	public boolean deleteItem(ObjectID id, User u) throws Exception {
		Item item = getItem(id);
		if (hasItem(item.getName())) {
			c.remove(item, u);
			return true;
		}
		return false;
	}

	/**
	 *
	 * @param id = Item Id
	 * @return Item which we were looking for
	 * @throws Exception
	 */
	public Item getItem(ObjectID id) throws Exception {
		Item item = (Item) (c.getNode(id));
		if (hasItem(item.getName())) {
			return item;
		}
		return null;
	}

	/**
	 * simple Method to check if the Cart is empty
	 *
	 * @return boolean
	 */
	@Override
	public boolean isEmpty() {
		try {
			return childCount(Item.class) == 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		return true;
	}

	/**
	 *
	 * @return every children inside of the Node Iterator
	 * @throws Exception
	 */
	public Iterator<Item> getItems() throws Exception {
		return getChildren().addFieldToFilter("type", Item.class).build();

	}

}
