package web;

import core.controller.mongo.MongoController2;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import core.User;
import core.controller.*;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.*;
import ch.qos.logback.classic.*;
import com.neoexpert.Log;

import javax.servlet.ServletContext;

/**
 *
 * @author neoexpert
 */
public class WebControllerBuilder extends ControllerBuilder {

	private String domain;
	private Locale locale;

	private static void initMongoConnection(ServletContext ctx) {

		String _MONGO_USER = ctx.getInitParameter("MONGO_USER");
		if(_MONGO_USER!=null)
			MONGO_USER=_MONGO_USER;
		String _MONGO_PW = ctx.getInitParameter("MONGO_PW");
		if(_MONGO_PW!=null)
			MONGO_PW=_MONGO_PW;
		String _MONGO_HOST = ctx.getInitParameter("MONGO_HOST");
		if(_MONGO_HOST!=null)
			MONGO_HOST=_MONGO_HOST;
		String _MONGO_PORT = ctx.getInitParameter("MONGO_HOST");
		if(_MONGO_PORT!=null)
			MONGO_PORT=_MONGO_PORT;
		String _MONGO_AUTH_DB = ctx.getInitParameter("MONGO_AUTH_DB");
		if(_MONGO_AUTH_DB!=null)
			AUTH_DB=_MONGO_AUTH_DB;
		String _MONGO_DB = ctx.getInitParameter("MONGO_DB");
		if(_MONGO_DB!=null)
			MONGO_DB=_MONGO_DB;
		if (MONGO_USER != null) {
			connection_string = "mongodb://" + MONGO_USER + ":" + MONGO_PW + "@" + MONGO_HOST + ":" + MONGO_PORT + "/" + AUTH_DB;
		} else {
			connection_string = "mongodb://" + MONGO_HOST + ":" + MONGO_PORT;
		}
		Log.log(connection_string);	
	}

	/**
	 *
	 * @param request
	 * @param ctx
	 * @throws Exception
	 */
	public WebControllerBuilder(HttpServletRequest request) throws Exception {
		locale = HttpController.getLang(request);

		this.domain = request.getServerName();
		if (null == mongo) {
			initMongoConnection(request.getServletContext());
		}

		build();
	}

	public WebControllerBuilder() {
		super();
		build();
	}

	/**
	 * @param domain
	 * @param root
	 * @param locale
	 * @throws Exception
	 */
	public WebControllerBuilder(String domain, boolean root, Locale locale, ServletContext ctx) throws Exception {
		this.domain = domain;
		this.locale = locale;
		if (null == mongo) {
			initMongoConnection(ctx);
		}

		build();
	}

	/**
	 *
	 * @param ctx
	 */
	public WebControllerBuilder(ServletContext ctx) {
		if (null == mongo) {
			initMongoConnection(ctx);
		}

		build();
	}

	private HttpController hc;

	/**
	 *
	 * @param domain
	 * @return
	 */
	public static String getRootPath(String domain) {

		return STATIC_FILES + domain;
	}

	/**
	 *
	 * @param request
	 * @return
	 */
	public static String getFullURL(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		String queryString = request.getQueryString();

		if (queryString == null) {
			return requestURL.toString();
		} else {
			return requestURL.append('?').append(queryString).toString();
		}
	}

	/**
	 *
	 * @return
	 */
	public HttpController getHttpController() {
		return hc;
	}

	public Controller getController() {
		return new MongoController2(mongo, domain, locale);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Controller build() {
		hc = new HttpController(mongo, domain, locale);
		return hc;
	}

	/**
	 *
	 * @param domain
	 * @return
	 */
	public ControllerBuilder setDomain(String domain) {
		this.domain = domain;
		return this;
	}

	/**
	 *
	 * @param locale
	 * @return
	 */
	public ControllerBuilder setLocale(Locale locale) {
		this.locale = locale;
		return this;
	}

	/**
	 *
	 * @param string
	 * @return
	 */
	public ControllerBuilder setDB(String string) {
		return this;
	}
}
