package shop;

import com.neoexpert.Log;
import core.attribute.Attribute;
import core.controller.Controller;
import core.controller.ControllerBuilder;
import core.id.ObjectID;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import shop.Product;
import core.*;

/**
 *
 * @author neoexpert
 */
public class Import {

    Controller c;

    /**
     *
     * @param c
     * @throws IOException
     */
    public Import(Controller c) throws IOException {
        this.c = c;
        staticfields.add("ID");
        staticfields.add("name");
        staticfields.add("parent");
        staticfields.add("type");
        staticfields.add("flags");
    }

    boolean importCaptions = true;
    ArrayList<String> attrnames;
    ArrayList<String> attrCaptions;
    HashSet<String> staticfields = new HashSet<>();

    /**
     *
     * @param u
     * @param out
     * @param in
     * @param cat
     * @param lang
     * @return
     * @throws Exception
     */
    public int importProducts(User u, PrintWriter out, BufferedReader in, Node cat, Locale lang) throws Exception {
        int c = 0;

        String line;
        // StringBuilder responseData = new StringBuilder();
        if ((line = in.readLine()) != null) {

            String oldf = "";
            attrnames = new ArrayList<>(Arrays.asList(line.split(",")));
            for (int i = 0; i < attrnames.size(); i++) {
                String f = attrnames.get(i);
                if (f.equals("")) {
                    throw new Exception("Column " + (i + 1) + " does not have a caption after " + oldf);
                }
                oldf = f;

                if (f.charAt(0) == '"' && f.charAt(f.length() - 1) == '"') {
                    f = f.substring(1).substring(0, f.length() - 2);
                    f = f.replaceAll("\"\"", "\"");
                    attrnames.set(i, f);

                }
            }
        } else {
            return 0;
        }
        int linenr = 1;

        if (importCaptions) {
            // StringBuilder responseData = new StringBuilder();
            if ((line = in.readLine()) != null) {
                linenr++;

                attrCaptions = new ArrayList<>(Arrays.asList(line.split(",")));
                for (int i = 0; i < attrCaptions.size(); i++) {
                    String f = attrCaptions.get(i);
                    if (f.equals("")) {
                        continue;
                    }
                    if (f.charAt(0) == '"' && f.charAt(f.length() - 1) == '"') {
                        f = f.substring(1).substring(0, f.length() - 2);
                        f = f.replaceAll("\"\"", "\"");
                        attrCaptions.set(i, f);
                    }
                }
            } else {
                return 0;
            }
            for (int i = 0; i < attrnames.size(); i++) {

                String attrname = attrnames.get(i);
                if (staticfields.contains(attrname)) {
                    continue;
                }
                if (attrCaptions.size() <= i) {
                    break;
                }
                String caption = attrCaptions.get(i);
                if (caption.equals("")) {
                    continue;
                }
                if (attrname.equals("name")) {
                    continue;
                }
                try {

                    Attribute attr;
                    attr = cat.getMyAttribute(attrname);

                    //attr.getType().setCaption(attrCaptions.get(i),lang,u);
                } catch (Exception e) {
                    Log.error(e);
                }

            }
        }

        while ((line = in.readLine()) != null) {
            if (linenr == 20) {
                getClass();
            }
            linenr++;

            try {
                String[] attrvalues = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                HashMap<String, String> p = new HashMap<>();

                for (int i = 0; i < attrnames.size(); i++) {
                    String attrname = attrnames.get(i);
                    if (attrname.charAt(0) == '"' && attrname.charAt(attrname.length() - 1) == '"') {
                        attrname = attrname.substring(1).substring(0, attrnames.get(i).length() - 2);
                        attrname = attrname.replaceAll("\"\"", "\"");
                    }
                    if (i < attrvalues.length) {
                        if (attrvalues[i].length() > 0) {
                            if (attrvalues[i].charAt(0) == '"'
                                    && attrvalues[i].charAt(attrvalues[i].length() - 1) == '"') {
                                attrvalues[i] = attrvalues[i].substring(1).substring(0, attrvalues[i].length() - 2);
                                attrvalues[i] = attrvalues[i].replaceAll("\"\"", "\"");
                            }
                        }
                    }
                    if (i < attrvalues.length) {
                        p.put(attrname, attrvalues[i]);
                    }
                }
                if (c % 20 == 0) {
                    this.c = new ControllerBuilder().getController();
                }
                addProduct(u, p, cat, lang);
                c++;
            } catch (Exception e) {
                out.append(linenr + ":" + e.getMessage() + "<br>");
            }
        }
        return c;
    }

    HashMap<String, String> ids = new HashMap<>();

    private void addProduct(User u, HashMap<String, String> hm, Node container, Locale lang) throws Exception {
        Node n = null;
        String update = hm.get("update");
        if (update != null) {
            if (!update.equals("")) {
                //n = container.getAttribute(update).findOne(hm.get(update));
            }
        }
        if (n == null) {
            n = new Node(u.getID(), c);
            String type = hm.get("type");
            if (type != null) {
                if (type.equals("shop.Product")) {
                    n = new Product(u.getID(), c);
                }
            }
            String name = hm.get("name");
            n.setName(name);
            if (name.equals("")) {
                getClass();
            }
            String parent = hm.get("parent");
            if (parent != null) {
                if (!parent.equals("0")) {
                    parent = ids.get(parent + "");
                    if (parent != null) {
                        container = c.getNode(new ObjectID(parent));
                    }
                }
            }
            n = container.addChild(n, u);
            String ID = hm.get("id");
            if (ID != null) {
                ids.put(ID, n.getID() + "");
            }
        }

        for (String key : hm.keySet()) {

            //Attribute attr = n.getAttribute(key);
//			if (attr == null)
//				n.setAttribute(u, key, hm.get(key), null);
//			else {
//				//attr = (Attribute) n.addChild(attr,u);
//				attr.setValue(hm.get(key), lang,u);
//			}
        }
    }

    /**
     *
     * @param in
     * @param u
     * @throws Exception
     */
    public void importUsers(BufferedReader in, User u) throws Exception {
        String line;
        Node users = c.getNode( "USERS");
        // StringBuilder responseData = new StringBuilder();
        String[] fs;
        if ((line = in.readLine()) != null) {
            fs = line.split(",");
            for (String f : fs) {
                //Attribute a = new Attribute(u.getID(), c);
                //a.setParent(users);
                //a.setName(f);
                try {
                    //c.addNode(a,u);
                } catch (Exception e) {
                }
            }
        } else {
            return;
        }
        while ((line = in.readLine()) != null) {
            String[] attrvalues = line.split(",");
            User nu = new User(u.getID(), c);
            u.setParent(users);
            u.setName(attrvalues[0]);
            c.addNode(nu, nu);
            for (int i = 1; i < fs.length; i++) {
                nu.setAttribute(u, fs[i], attrvalues[i]).commit(u);
            }

        }
    }
}
