package shop;

import core.Node;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Suppliers extends Node {

	static {
		addChildType(Suppliers.class, Supplier.class);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Suppliers(Controller c,Document doc) {
		super(c,doc);
	}

	/**
	 *
	 * @param id
	 * @param c
	 */
	public Suppliers(ObjectID id, Controller c) {
		super(id, c);
	}
}
