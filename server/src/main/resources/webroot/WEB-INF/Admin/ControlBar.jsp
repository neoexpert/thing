<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<%@ page import="core.*"%>
<%@ page import="web.*"%>
<%@ page import="core.attribute.*"%>

<%@ page import="java.util.*"%>
<%@ page import="core.controller.*"%>
<%@ page import="templates.HtmlTemplate"%>

<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%>

<!DOCTYPE HTML>
<!-- neoexpert was here -->
<compress:html>
<html>
<head>
<meta charset="UTF-8">

<title></title>
<script src="/js/core.js?v2"></script>
<script>
var admin=true;
</script>

<jsp:include page="HEAD.jsp" />


<%
	Locale lang = (Locale) request.getAttribute("locale");
		String username = "";
		String userID = "";

		try {
			username = request.getSession().getAttribute("UserName").toString();
			userID = request.getSession().getAttribute("UserID").toString();
		} catch (Exception e) {
		}
		HttpController c=(HttpController)request.getAttribute("controller");
		Node n = (Node) request.getAttribute("node");

		Object rID = request.getAttribute("rootCategoryID");
		Integer rootID = 0;
		if (rID != null)
			rootID = Integer.valueOf(rID.toString());
%>


</head>
<body>
	<div class="site-container">
		<div class="site-pusher">

			<%
				HtmlTemplate t = (HtmlTemplate) c.getNode("DESIGN").getChild("ADMIN_HEADER");
					if (t != null)
						out.print(t.toString());
			%>

			<div id="wrapper" class="site-content">
			<jsp:include page="WRAPPER.jsp" />
			</div>
			<div class="site-cache" id="site-cache"></div>
		</div>
	</div>

	<script>
	
		$('#header__icon').click(function(e) {
			e.preventDefault();
			$('body').toggleClass('with--sidebar');
		});
		$('#site-cache').click(function(e) {
			$('body').removeClass('with--sidebar');
		});
	</script>
<div id="debugger"></div>
	<script>
		window.onerror = function (message, url, lineNo){
			$("#debugger").append('<div>Error: ' + message + '\n' + 'Line Number: ' + lineNo+"</div>");
    return true;
}
	</script>
</body>
</html>

</compress:html>
