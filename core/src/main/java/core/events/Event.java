package core.events;

import core.Node;
import core.User;
import core.controller.Controller;
import core.id.ObjectID;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import org.bson.Document;
import tools.Tools;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public class Event extends Node {

    private Node n;
    private String oldvalue;
    private String newvalue;
    private ObjectID invovedNodeID;

    /**
     *
     * @param n
     * @param event
     * @param con
     * @param oldvalue
     * @param newvalue
     */
    public Event(Node n, String event, Controller con, String oldvalue, String newvalue) {
        super(null, con);
        this.n = n;
        doc.put("event", event);
        doc.put("oldvalue", oldvalue);
        doc.put("newvalue", newvalue);
        switch(event){
            case "CREATED":
            case "DELETED":
                invovedNodeID=n.getParentID();
            default:
                invovedNodeID=n.getID();
        }
    }

   /**
     *
     * @param c
	 * @param doc
     */
    public Event(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @return
     */
    @Override
    public void onCreate(User u) {}
    @Override
    public void onUpdate(User u) {}
    @Override
    public void onDelete(User u) {}

    /**
     *
     * @return
     */
    public String toHtmlRow() {
        String html = "<tr class='" + doc.get("event") + "'>";


        html += ("<td>");
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.GERMANY);
        String formattedDate = df.format(new Date(getUpdated()));
        html += formattedDate;
        html += "(";
        html += (Tools.getDuration(System.currentTimeMillis() - getUpdated()));
        html += ")";

        html += ("</td>");

        html += ("</tr>");
        return html;
    }

    /**
     *
     * @return
     */
    public Node getNode() {
        return n;
    }

    @Override
    public String toHTMLROW(Locale locale) {
        return toHtmlRow();
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String html = doc.get("event")+"";


        html += ("<td>");
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.GERMANY);
        String formattedDate = df.format(new Date(getUpdated()));
        html += formattedDate;
        html += "(";
        html += (Tools.getDuration(System.currentTimeMillis() - getUpdated()));
        html += ")";

        html += ("</td>");

        html += ("</tr>");
        return html;
    }

    /**
     *
     * @return
     */
    @Override
    public String getPreviousValue() {
        return oldvalue;
    }

    /**
     *
     * @return
     */
    @Override
    public String getNewValue() {
        return newvalue;
    }

    public  ObjectID getInvolvedNodeID() {
        return invovedNodeID;
    }
		
		
		@Override
		public JSONObject toJSON() {
					JSONObject jo=new JSONObject();
					jo.put("event",doc.get("event"));
					jo.put("node",n.toJSON());
					return jo;
		}

}
