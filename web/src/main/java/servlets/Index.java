package servlets;

import com.neoexpert.Log;
import web.DateUtils;
import core.Node;
import core.User;
import core.controller.Controller;
import core.controller.ControllerBuilder;
import web.*;
import core.events.Request;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Index
 */
@WebServlet("/")
// @WebServlet(asyncSupported = true, urlPatterns = { "/" }, loadOnStartup = 1)
public class Index extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Index() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init(); //To change body of generated methods, choose Tools | Templates.
		try {
			HttpController c = new WebControllerBuilder("", true, Locale.ENGLISH, getServletContext()).getHttpController();
			c.init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");

		HttpController c;
		OutputStream os = response.getOutputStream();
		PrintWriter out = new PrintWriter(os);
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			out.append(e.toString());
			Log.error(e);
			return;
		}
		try {
			request.setAttribute("controller", c);
			Node root = c.getNode("CATEGORIES");

			String uri = request.getRequestURI();

			String userAgent = request.getHeader("User-Agent");

			HttpSession session = request.getSession();

			User u = c.getUser(session);
			request.setAttribute("user", u);
			boolean admin;
			if (u == null) {
				admin = false;
			} else {
				admin = u.isLoggedIn("admin");
			}
			Locale locale = HttpController.getLang(request);
			request.setAttribute("locale", locale);

			if (uri.equals("/")) {

				String query = request.getQueryString();
				String redirect = "/" + locale.getLanguage().toLowerCase(Locale.ENGLISH) + "/";
				if (query != null) {
					redirect += "?" + query;
				}
				response.sendRedirect(redirect);
				// processStats(is, os, request, c);

				c.close();

				return;
			}

			if (!checkForFiles(uri, request, response, os)) {
				if (!checkForNodes(c, uri, request, response, locale, admin, root, out, u)) {
					Log.warn("ressource not found: " + uri);
					response.setStatus(404);
					RequestDispatcher rd;
					rd = request.getRequestDispatcher("/WEB-INF/404.jsp");
					rd.forward(request, response);
				}

			}

			out.flush();

			// processStats(is, os, request, c);
		} catch (IOException | ServletException e) {
			out.append(e.toString());
			Log.error(e);
			e.printStackTrace(out);
		}
	}

	private void processStats(ServletInputStream is, ServletOutputStream os, HttpServletRequest request,
			HttpController c) {

		try {
			is.close();
			os.close();
			Node n = c.getNode("REQUESTS");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private boolean checkForNodes(final Controller c, String uri, HttpServletRequest request,
			HttpServletResponse response, Locale locale, boolean admin, Node root, PrintWriter out, final User u) throws IOException, ServletException {
		String[] cats = uri.substring(1).split("/");

		int offset = 0;

		if (cats.length > 0) {

			if (cats[0].length() == 2) {
				locale = new Locale(cats[0].toLowerCase(Locale.ENGLISH));

				HttpController.setLang(request, locale);
				offset = 1;
				if (cats.length == 1) {
					if (request.getParameter("nohome") == null) {
						root = root.getChild("home");
						if (root == null) {
							root = c.getNode("CATEGORIES");
						}
					}
					// cat.setID(cat.getID());

				}

			}

			if (cats[0].equals("css") || cats[0].equals("js")) {
				root = c.getNode(root.getParentID(), cats[0]);
				offset = 1;

			}
		}
		//HttpSession session = request.getSession();

		// root.setID(root.getID());
		for (int i = offset; i < cats.length; i++) {
			root = c.getNodeByUrl(root, cats[i].replace("%20", " "), locale);
			if (root == null) {
				return false;
			}
		}
		final String referer = ((HttpServletRequest) request).getHeader("referer");
		final String useragent = ((HttpServletRequest) request).getHeader("User-Agent");
		final Node n = root;
		new Thread(new Runnable() {
			@Override
			public void run() {
				Request r = new Request(u.getID(), c);
				r.doc.put("referer", referer);
				r.doc.put("useragent", useragent);
				r.setName(UUID.randomUUID().toString());
				n.addChild(r, u);
				n.onAccess();

			}
		}).start();

		String ifModifiedSince = request.getHeader("If-Modified-Since");
		if (ifModifiedSince != null) {
			Date ifm = DateUtils.parseDate(ifModifiedSince);
			// to check if-modified-since time is in the above format

			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			Log.log(sdf.format(new Date(ifm.getTime())) + "(" + ifm.getTime() + ") - ");
			Log.log(sdf.format(new Date(root.getUpdated())) + "(" + root.getUpdated() + ") - ");
			if (ifm.getTime() >= root.getUpdated() - 1000) {
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);

				Log.log(root.getName() + " not modified");
				return true;
			}
		}
		// response.setHeader("Last-Modified", formatted);
		response.setDateHeader("Last-Modified", root.getUpdated());
		response.setDateHeader("Expires", System.currentTimeMillis() + 6048000000L); // 10
		// week
		// in
		// future.

		request.setAttribute("ID", root.getID());
		request.setAttribute("node", root);

		String wrapper = request.getParameter("wrapper");

		RequestDispatcher rd;
		request.setAttribute("title", root.getCaption(locale));
		request.setAttribute("node", root);

		switch (root.getClass().getName()) {
			default:
				if (wrapper == null) {
					rd = request.getRequestDispatcher("/WEB-INF/ControlBar.jsp");
				} else {
					rd = request.getRequestDispatcher("/WEB-INF/WRAPPER.jsp");
				}

				rd.forward(request, response);

				break;
			case "core.Image":
				File file = new java.io.File(
						ControllerBuilder.getRootPath(request.getServerName()) + "/media/" + root.getID() + ".jpg");
				if (file.exists()) {
					FileServlet fs = new FileServlet();
					fs.init(getServletConfig());

					fs.setFile(file);
					fs.doGet(request, response);
					return true;
				}
				break;
			case "core.File":
				response.setContentType(getServletContext().getMimeType(root.getName()));
				out.append(((core.File) root).getContent());
				return true;
		}
		return true;
	}

	public static long lastmodified=System.currentTimeMillis()/1000;
	private boolean checkForFiles(String uri, HttpServletRequest request, HttpServletResponse response,OutputStream os)
			throws IOException, ServletException {
		ServletContext context = getServletContext();


		/*URI jar_uri = HttpController.getWebRootResourceUri(this, uri);
			File file = null;
			if (jar_uri != null) {
				file = new File(jar_uri);
			}
			
			if (file.exists()) {
				FileServlet fs = new FileServlet();
				fs.init(getServletConfig());

				fs.setFile(file);
				fs.doGet(request, response);
				return true;
			}*/
		InputStream in = context.getResourceAsStream("/WEB-INF/static" + uri);

		if (in != null) {
			response.setDateHeader("Last-Modified", lastmodified);
			response.setHeader("Content-Type", getServletContext().getMimeType(uri));
			//response.setHeader("Content-Length", String.valueOf(file.length()));
			//response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
			
			byte[] buf=new byte[64];
			int numRead;
			while ((numRead = in.read(buf)) >= 0) {
				os.write(buf, 0, numRead);
			}
			//org.apache.tomcat.util.http.fileupload.IOUtils.copy(in, response.getOutputStream());
			in.close();
			return true;
		}
		// out.close();

		String filename = URLDecoder.decode(uri.substring(1), "UTF-8");
		File file = new File(ControllerBuilder.getRootPath(request.getServerName()), filename);
		Log.log(file.getPath());
		Log.log(ControllerBuilder.getRootPath(request.getServerName()));

		if (file.isDirectory()) {
			response.sendRedirect("/");
		}
		if (file.exists()) {
			FileServlet fs = new FileServlet();
			fs.init(getServletConfig());
			fs.setFile(file);
			// fs.getServletContext()
			fs.doGet(request, response);

			// String ifModifiedSince = request.getHeader("If-Modified-Since");
			// if (ifModifiedSince != null) {
			// Date ifm = DateUtils.parseDate(ifModifiedSince);
			// // to check if-modified-since time is in the above format
			//
			// if (ifm.getTime() > file.lastModified()) {
			// response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			// return true;
			// }
			// }
			// response.setDateHeader("Last-Modified", file.lastModified());
			// response.setHeader("Content-Type",
			// getServletContext().getMimeType(filename));
			// response.setHeader("Content-Length",
			// String.valueOf(file.length()));
			// response.setHeader("Content-Disposition", "inline; filename=\"" +
			// file.getName() + "\"");
			//
			// Files.copy(file.toPath(), response.getOutputStream());
			return true;
		}

		return false;
	}

	/**
	 * @param request
	 * @param response
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Rest r = new Rest();
		r.doPost(request, response);
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @param session
	 * @param lang
	 * @throws ServletException
	 * @throws IOException
	 */
	public void redirectToLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Locale lang) throws ServletException, IOException {
		response.sendRedirect("/" + lang.getLanguage() + "/login");

		String uri = WebControllerBuilder.getFullURL(request);
		session.setAttribute("redirectTO", uri);
	}

}
