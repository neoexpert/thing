package servlets;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.Zombie;
import web.*;
import core.controller.NodeIterator;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MeinServlet
 */
@WebServlet("/login")
public class Login extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();

	}


	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}

		HttpSession session = request.getSession();
		User u = c.getUser(session);
		Boolean loggedIn = !(u instanceof Zombie);

		if (loggedIn) {
			String r = (String) session.getAttribute("redirectTO");
			Log.log("Login redirectto:"+r);
			if (r == null)
				if (u.isInGroup("admin")) {
					try {
						Node root = c.getRoot();
						response.getWriter().append(root.getBackURL());

					} catch (Exception e) {
						Log.error(e);
					}
				} else {
					response.getWriter().append("/");
				}

			else {
				response.getWriter().append(r);
				session.removeAttribute("redirectTO");
			}

		}
		c.close();
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		HttpSession session = request.getSession();
		Locale locale = request.getLocale();
		String lang = locale.getLanguage();

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		User u;
		User pu = c.getUser(session);

		try {
			u = c.userLogin(username, password);
		} catch (Exception e) {
			if (username == null)
				response.getWriter().println("username is null\n");
			if (password == null)
				response.getWriter().println("password is null\n");
			return;
		}
		if (u != null) {
			try {
				Node cart = pu.getChild("CART");
				if(cart!=null)
					if (!cart.isEmpty()) {
						u.removeChild("CART", u);
						cart.setParent(u);
						cart.setOwner(u)
						.save(u);
						NodeIterator<shop.Item> it = cart.getChildren(shop.Item.class).build();
						while(it.hasNext()){
							it.next()
							.setOwner(u)
							.save(u);
						}

				}
			} catch (Exception e) {
				Log.error(e);
			}
			session.setAttribute("loggedIn", true);
			session.setAttribute("UserID", u.getID());
			session.setAttribute("UserName", u.getName());
			if (u.isInGroup("admin"))
				session.setAttribute("GROUP", "admin");
			else
				session.setAttribute("GROUP", "USER");
			session.setAttribute("lang", lang);
			session.setAttribute("rootID", c.getRootID());
		}

		doGet(request, response);
		c.close();
	}

}
