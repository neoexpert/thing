package templates;

import core.Node;
import core.attribute.Attribute;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Locale;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public abstract class Template extends Node {

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Template(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userID
	 * @param con
	 */
	public Template(ObjectID userID, Controller con) {
		super(userID, con);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return getValue();

	}
	public abstract String getUnfilled(Node n);

	/**
	 *
	 * @return
	 */
	public String getWrapperTag() {
		Attribute w = getAttribute("wrapper");
		if (w != null) {
			return w.getValue().toString();
		}
		return "div";
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return (String) doc.get("value");
	}

	/**
	 *
	 * @param n
	 * @param locale
	 * @return
	 */
	public abstract String fill(Node n, Locale locale);
}
