package servlets;

import com.neoexpert.Log;
import core.User;
import core.Zombie;
import web.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import templates.Template;

/**
 * Servlet implementation class CategoryView
 */
@WebServlet("/menu")
public class Menu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Menu() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		request.setAttribute("controller", c);
		HttpSession session = request.getSession();
		User u = c.getUser(session);
		
		Template t;
		try {

			if (u instanceof Zombie)
				t = (Template) c.getNode("DESIGN").getChild("MENU");

			else
				t = (Template) c.getNode("DESIGN").getChild("USER_MENU");
			response.setContentType("text/html");
			response.getWriter().append("<div data-id='"+t.getID()+"' class='template unfilled'>");
			response.getWriter().append(t.getValue());
			response.getWriter().append("</div>");


		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
