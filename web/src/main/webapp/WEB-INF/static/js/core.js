function printError(cmd,err){
	var errorReport=JSON.stringify(cmd);

	try{
		err=JSON.parse(err);
	}
	catch(e){
		console.error(err);
		console.error(errorReport);
		return;
	}
	errorReport+=err.e+"\n";
	errorReport+="stacktrace: "+err.stacktrace+"\n";
	console.error(errorReport);
}

function request(cmd,callback){
	$.post("/rest",cmd,function(response){
		if(response===""){
			callback();
			return;
		}
		var attrs = JSON.parse(response);
		callback(attrs);
	}).fail(function(xhr, status, error) {
		printError( cmd,xhr.responseText );
	  });
}

function adminRequest(cmd,callback){
	$.post("/rest",cmd,function(response){
		if(response===""){
			callback();
			return;
		}
		try{
		var attrs = JSON.parse(response);
		}
		catch(e){
			printError( cmd,response);

			return;
		}
		callback(attrs);
	}).fail(function(xhr, status, error) {
		printError( cmd,xhr.responseText );
	  });
}


function requestText(cmd,callback,url){
	if(!url)url="/rest";
	$.post(url,cmd,function(response){
		callback(response);
	}).fail(function(xhr, status, error) {
		printError(cmd, xhr.responseText );
	  });
}


function removeParent(e){
	e=$(e);
	var id=undefined;
	while(!id)
	{
      	id=e.attr("data-id");
		if(id)break;
		e=e.parent();
  	}
  	remove(id);
	e.remove();
}

function remove(_id,callback){
	var cmd={};
	cmd.cmd="remove";
	cmd._id=_id;
	request(cmd,callback);
}

function setAttribute(ID,name,value){
	var cmd={};
	cmd.cmd="setAttr";
	cmd.ID=ID;
	cmd.name=name;
	cmd.value=value;
	adminRequest(cmd,function(){
		
	});
}

function fillTemplateAttrs(t,attrs){
	for ( var key in attrs) {
		if (attrs.hasOwnProperty(key)) {
			if(key.startsWith("attr_")){
				t.attr(key.substring(5),attrs[key].value);
			}
			else
				setAttr(key, attrs[key], t);
		}
	}
}

function fillTemplate(t,names) {
	t.removeClass("unfilled");
	if(t.hasClass("filled"))return;
	t.addClass("filled");
	var id = t.attr("data-id");
	if(!names)
		names = [];
	var es = t.find(".attribute").each(function() {
		names.push($(this).attr("data-name"));
	});
	if(names.length>0)
	{
		getNodeAttrs(id, names, 
		function(attrs) {
			fillTemplateAttrs(t,attrs);
			processTemplate(t,attrs);
		});
	}
	t.find(".child").each(function() {
		getChildTemplate(id, $(this));
	});
	t.find(".related").each(function() {
		getRelatedTemplate(id, $(this));
	});
	t.find(".children").each(function() {
		getChildrenTemplates(id, $(this));
	});
}

function initTemplate(t) {
	
}



function processTemplate(t,attrs){
	if(attrs)
	if(attrs.type)
	switch(attrs.type.value){
	case "core.attribute.Configuration":
		var name=attrs.name.value;
		var handler=window[name+"_handler"];

		
		t.on("click",function(e) {
			
			var li=$(e.target);
			while(!li.hasClass("option"))
				li=li.parent();
	    	  // when li is clicked
	    	 li.addClass("option-selected")
	    	    .siblings().removeClass("option-selected");
	    	  // swap class
			if(typeof handler=="function")
				handler(li.attr("data-name"),li.text());
	    });
		

		break;
	}

}

function getRelatedTemplate(parent,child){
	var names=[];
	child.each(function() {
		$.each(this.attributes, function(i, attrib){
			if(attrib.value==="")
				names.push("attr_"+attrib.name);
    
		});
	});
	if(child.attr("data-nofill"))
	{
		fillTemplate(child,names);
		return;
	}
	requestText({
		parent : parent,
		name:child.attr("data-name"),
    template:child.attr("data-template"),
		cmd:"getRelatedTemplate"
	},function(r) {
		var e=$(r);
      	child.replaceWith(e);
      	fillTemplate(e,names);
    });
}

function getChildTemplate(parent,child){
	requestText({
		parent : parent,
		name:child.attr("data-name"),
    template:child.attr("data-template"),
		cmd:"getChildTemplate"
	},function(responseText) {
		var e=$(responseText);
      child.append(e);
      fillTemplate(e);
    });
}

function getChildrenTemplates(id,child){
	requestText({
		ID : id,
      	cmd:"getChildrenTemplates",
      	template_prefix:child.attr("data-templateprefix")
	},function(responseText) {
      child.html(responseText);
      child.find(".unfilled").each(function(){
      	fillTemplate($(this));
      });
    });
}

function setAttr(name,a,e){
	if(name=="")
		return;
  	if(e)
		e=e.find(".attribute[data-name=" + name+"]");
  	else
		e=$(".attribute[data-name=" + name+"]");
  	if(e.hasClass("filled"))return;
	e.attr("data-parent",a.parent);

  	var dataattr=e.attr("data-attr");
  	if(dataattr){
      		e.attr(dataattr,a.value);
      		return;
    	}

  	var onload=e.attr("data-load");
  	if(onload){
    		var f=window[onload];
        	if(typeof f=="function"){
        		f(a,e);
        }
        else
           console.log(onload+" function not found");
      	return;
    }
  
  	if(a.type){
  		if(a.type.template){
  			processTemplate(a,e);
  			return;
  		}
  	}
	e.html(a.value);
}

function toggleEditMode(){
	$(".attribute").each(function(){
		var e=$(this);
		if(!e[0].hasAttribute('data-parent'))return;
        e.attr('contentEditable',true);

		e.addClass("editable");
        e.on("click", function() {
            $(this).get(0).addEventListener("input", function() {
            	setAttribute($(this).attr("data-parent"),$(this).attr("data-name"),$(this).html());
			}, false);
		});
	});
}

function getAttrs(callback) {
  	jQuery.ajaxSettings.traditional = true;
  	var names=[];
	var es=$(".attribute").each(function(){
       	names.push($(this).attr("data-name"));
    });
	if(names.length==0){
    	if(typeof callback === "function")
			callback();
        return;
    }
	request({
		ID :ID,
		cmd:"getAttrs",
		"names":names
	}, function(attrs) {
		window.node_attrs = attrs;
		setAttrs(window.node_attrs);
		if(typeof callback === "function")
			callback();
	});
}

function getNodeAttrs(ID,names,callback){
	jQuery.ajaxSettings.traditional = true;
	request({ID : ID,names : names,cmd:"getAttrs"},callback);
	jQuery.ajaxSettings.traditional = false;
}

function getChildAttrs(pID,name,names,callback){
	jQuery.ajaxSettings.traditional = true;
	request({parent:pID,name:name,names : names,cmd:"getChildAttrs"},callback);
	jQuery.ajaxSettings.traditional = false;
}

function setAttrs(attrs) {
	if(attrs.ID)
		productID = attrs.ID;
	if(attrs.caption)
		document.title = attrs.caption.value;
	if(attrs.caption)
		document.title = attrs.caption.value;
    for (var attr in attrs) {
  		if (attrs.hasOwnProperty(attr)) {
        	var a=attrs[attr];
            setAttr(attr,a);
            if(a.caption){
             	setAttr(attr+"_caption",a.caption);
    		}
  		}
	}
}

function queryStringToJSON() {            
    var pairs = location.search.slice(1).split('&');
    
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}



