package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Filemanager extends Node {
	static{
		addChildType(Filemanager.class,File.class);
		addChildType(Filemanager.class,Filemanager.class);
		addChildType(Filemanager.class,Link.class);

	}

    /**
     *
     * @param c
	 * @param doc
     */
    public Filemanager(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @param userid
     * @param c
     */
    public Filemanager(ObjectID userid, Controller c) {
		super(userid,c);
	}

}
