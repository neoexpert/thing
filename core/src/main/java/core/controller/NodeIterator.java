package core.controller;

import core.Node;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
public abstract class NodeIterator<T extends Node> implements Iterator<T>, Iterable<T> {


    private Set<String> attrs;
    private String templatename;

    /**
     *
     * @param name
     * @param value
     * @return
     */
    public abstract NodeIterator addFieldToFilter(String name, Object value);

    /**
     *
     * @return @throws RuntimeException
     */
    public abstract NodeIterator build() throws RuntimeException;

    /**
     *
     * @param attribute
     * @param value
     * @param lang
     * @param regex
     * @return
     */
    public abstract NodeIterator addAttributeToFilter(String attribute, String value, Locale lang, boolean regex);

    /**
     *
     */
    public abstract void close();

    /**
     *
     * @param path
     * @return
     */
    public abstract NodeIterator setPath(String path);

    /**
     *
     * @param name
     * @return
     */
    public abstract NodeIterator addAttributeToExists(String name);

    /**
     *
     * @param string
     * @return
     */
    public abstract NodeIterator setCallback(String string);

    /**
     *
     * @return
     */
    public abstract String getCallback();

    /**
     *
     * @param key
     * @param value
     * @param b
     * @return
     */
    public abstract NodeIterator addAttributeToFilter(String key, Object value, boolean b);

    /**
     *
     * @param sort
     * @return
     */
    public abstract NodeIterator sort(JSONObject sort);

    /**
     *
     * @param sort
     * @param desc
     * @return
     */
    public abstract NodeIterator sort(String sort, boolean desc);

    public  Set<String> getAttrs(){
        return attrs;
    }

    /**
     *
     * @param attrs
     */
    public void setAttrs(Set<String> attrs) {
        this.attrs = attrs;
    }

    public String getTemplateName() {
        return templatename;
    }
    public void setTemplateName(String templatename){
        this.templatename=templatename;
    }
}
