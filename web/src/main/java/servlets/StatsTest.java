package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class StatsTest
 */
@WebServlet(urlPatterns = { "/statstest" },asyncSupported=true)
public class StatsTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StatsTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
        @Override
	protected void doGet(final HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String s ="";
		BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
		for (String buffer;(buffer = in.readLine()) != null;s+=buffer + "\n");

		System.out.println(s);
		ServletOutputStream os = response.getOutputStream();
		PrintWriter out = new PrintWriter(os);
		out.append("Served at: ").append(request.getContextPath());
		out.flush();
		os.flush();
		os.close();
		//is.close();
		

	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
}
