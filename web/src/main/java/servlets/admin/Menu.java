package servlets.admin;

import com.neoexpert.Log;
import core.*;
import core.controller.*;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Admin
 */

@WebServlet("/admin/menu")
public class Menu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Menu() {
		super();
	}

	/**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpController c;
		try {
			c = new WebControllerBuilder(request).getHttpController();
		} catch (Exception e) {
			response.getWriter().append(e.toString());
			Log.error(e);
			return;
		}
		request.setAttribute("controller", c);

		HttpSession session = request.getSession();
		User u = c.getUser(session);
		Locale lang = HttpController.getLang(request);

		if (!u.isLoggedIn("admin")) {
			
			
			return;
		}
		Node n = c.getRoot();
		PrintWriter out = response.getWriter();
		out.print(n.getBackLink(lang));

		//c.getRootID(), Node.class, "ORDER_ID"
		Iterable<Node> it = n.getChildren().build();

		// iterate through the java resultset
		for (Node cat : it) {
			if (!cat.visibleInMenu())
				continue;
			//Node cat = it.next();

			String name = cat.getCaption(lang);

			


			out.println("<a href=\'" + cat.getBackURL() + "' onclick=\"loadArea('" + cat.getBackURL()
					+ "');return false;\">" + name + "</a>");
		}

		
		c.close();
	}

    /**
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
	}

}
