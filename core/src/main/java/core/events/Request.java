package core.events;

import core.Node;
import core.User;
import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Request extends Node {

    /**
     *
     * @param c
	 * @param doc
     */
    public Request(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @param userID
     * @param c
     */
    public Request(ObjectID userID, Controller c) {
	super(userID, c);
	// TODO Auto-generated constructor stub
    }

    /**
     *
     * @param u
     */
    @Override
    public void onCreate(User u) {

    }

    @Override
    public void onUpdate(User u) {
    }
}
