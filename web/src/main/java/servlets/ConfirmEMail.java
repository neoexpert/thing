/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.neoexpert.Log;
import core.User;
import core.attribute.Attribute;
import core.controller.ControllerBuilder;
import web.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 *
 * @author neoexpert
 */
@WebServlet(name = "ConfirmEMail", urlPatterns = {"/confirm_email"})
public class ConfirmEMail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            out.append(e.toString());
            Log.error(e);
            return;
        }
		Locale locale = HttpController.getLang(request);
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("email");
        String uuid = request.getParameter("uuid");

        User u = c.getUser(email);
        Attribute uuidattr = u.getAttribute("email_confirmation_uuid");
        if (uuidattr.getValue().equals(uuid)) {
            u.setAttribute("email_confirmed", true).commit(u);
            response.sendRedirect("/" + locale.getLanguage() + "/login");
        }
        else
        out.append("error");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
