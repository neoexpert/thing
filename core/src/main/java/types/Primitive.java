package types;

import core.Node;
import core.User;
import core.attribute.Attribute;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Locale;
import org.bson.Document;
import org.json.JSONObject;
import templates.Template;

/**
 *
 * @author neoexpert
 */
public class Primitive extends Node {

    /**
     *
     */
    public static final int TYPE_STRING = 0;

    /**
     *
     */
    public static final int TYPE_INTEGER = 1;

    /**
     *
     */
    public static final int TYPE_DOUBLE = 2;

    /**
     *
     */
    public static final int TYPE_BOOLEAN = 3;

    /**
     *
     * @param c
	 * @param doc
     */
    public Primitive(Controller c,Document doc) {
		super(c,doc);
	}

    /**
     *
     * @param userID
     * @param con
     */
    public Primitive( ObjectID userID, Controller con) {
        super(userID, con);
    }

    /**
     *
     * @return
     */
    public boolean isMultilang() {
        if (doc.containsKey("multilang")) {
            Object ml = doc.get("multilang");
            if (ml instanceof Boolean) {
                return (boolean) ml;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public int getValueType() {
        if (doc.containsKey("value_type")) {
            Object vt = doc.get("value_type");
            if (vt instanceof Integer) {
                return (Integer) vt;
            }
        }

        return TYPE_STRING;
    }

    /**
     *
     * @return
     */
    public JSONObject getInfo() {
        JSONObject jo = toJSON();

        Template t = (Template) getChild("template");
        if (t != null) {
            jo.put("template", t.getValue());
        }

        return jo;
    }

    /**
     *
     * @param a
     * @param lang
     * @return
     */
    public Object valueOf(Attribute a, Locale lang) {
        if (isMultilang()) {
            return a.getValue(lang);
        } else {
            return a.getValue();
        }
    }

    /**
     *
     * @return
     */
    public JSONObject getAdminInfo() {
        JSONObject jo = toJSON();

        Template t = (Template) getChild("admin_template");
        if (t != null) {
            jo.put("template", t.getValue());
        }

        return jo;
    }

    /**
     *
     * @param u
     */
    @Override
    public void onCreate(User u) {
        addAttr("multilang", u);
        addAttr("value_type", u);
        addAttr("hidden", u);
        super.onCreate(u);
    }

    /**
     *
     * @param attribute
     * @param value
     * @param locale
     * @param u
     */
    public void setValueOf(Attribute attribute, String value, Locale locale, User u) {
        switch (getValueType()) {
            case TYPE_STRING:
                if (isMultilang()) {
                    attribute.getParent().setAttribute(attribute.getName(), value, locale).commit(u);
                } else {
                    attribute.getParent().setAttribute(attribute.getName(), value).commit(u);
                }
                break;
            case TYPE_BOOLEAN:
                attribute.getParent().setAttribute(attribute.getName(), "true".equals(value)).commit(u);
                break;
            case TYPE_INTEGER:
                attribute.getParent().setAttribute(attribute.getName(), Integer.parseInt(value)).commit(u);
                break;
            case TYPE_DOUBLE:
                attribute.getParent().setAttribute(attribute.getName(), Double.parseDouble(value)).commit(u);
                break;
            default:
                break;
        }
    }
}
