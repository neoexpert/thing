package core;

import core.attribute.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author neoexpert
 */
public class SendEmail {

	/**
	 *
	 * @param mailsettings
	 * @throws Exception
	 */
	public SendEmail(Node mailsettings) throws Exception {
		Attribute host = mailsettings.getAttribute("host");
		if (host != null) {
			this.host = (String) host.getValue();
		} else {
			throw new Exception("no host attribute set in ROOT");
		}

		Attribute username = mailsettings.getAttribute("username");
		if (username != null) {
			this.username = (String) username.getValue();
		} else {
			throw new Exception("no username attribute set in ROOT");
		}

		Attribute pw = mailsettings.getAttribute("pw");
		if (pw != null) {
			this.pw = (String) pw.getValue();
		} else {
			throw new Exception("no pw attribute set in ROOT");
		}

		Attribute port = mailsettings.getAttribute("port");
		if (port != null) {
			this.port = (Integer) port.getValue();
		}
	}
	//final private static String password="13@N=K0ol";
	private String pw = "100%R0qu3";

	//final private static String host="smtp.office365.com";
	private String host = "smtp.gmail.com";

	private int port = 587;

	private String username = "ulf.fischbeck@gmail.com";

	/**
	 *
	 * @param toAddress
	 * @param subject
	 * @param message
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public void sendEmail(
			String toAddress, String subject, String message) throws AddressException, MessagingException {

		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, pw);
			}
		};

		Session session = Session.getInstance(properties, auth);

		// creates a new e-mail message
		Message msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(username));
		InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		msg.setContent(message, "text/html; charset=utf-8");

		// sends the e-mail
		Transport.send(msg);

	}

	public void sendEmail(
			String toAddress, String subject, String message, ArrayList<String> files) throws AddressException, MessagingException, IOException {

		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, pw);
			}
		};

		Session session = Session.getInstance(properties, auth);

		Multipart multipart = new MimeMultipart();

		final MimeBodyPart textPart = new MimeBodyPart();
		textPart.setContent(message, "text/html; charset=utf-8");
		multipart.addBodyPart(textPart);

		int i = 0;
		for (String fileb64 : files) {
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			String mime = fileb64.substring(0, fileb64.indexOf(';'));
			mime=mime.substring(mime.indexOf(':')+1);
			String file_data = fileb64.substring(fileb64.indexOf(',') + 1);

			String fileName = i++ + "";
			switch (mime) {
				case "image/jpeg":
					fileName += ".jpg";
					break;
				case "image/png":
					fileName += ".png";
					break;
				case "image/gif":
					fileName += ".gif";
					break;
				case "application/pdf":
					fileName += ".pdf";
					break;
				default:
					throw new RuntimeException("FILE TYPE NOT ALLOWED (DAMN!)");

			}

			byte[] buf = new sun.misc.BASE64Decoder().decodeBuffer(file_data);

			DataSource source = new ByteArrayDataSource(buf, mime);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
		}

		//msg.setContent(multipart);
		// creates a new e-mail message
		Message msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(username));
		InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		//msg.setContent(message,"text/html; charset=utf-8");
		msg.setContent(multipart);

		// sends the e-mail
		Transport.send(msg);

	}
}
