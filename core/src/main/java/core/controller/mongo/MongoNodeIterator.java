package core.controller.mongo;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import core.Node;
import core.controller.Controller;
import core.controller.ControllerBuilder;
import core.controller.NodeIterator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import org.bson.Document;
import org.json.JSONObject;

/**
 * Iteriert über Kategorien-Kollektionen.
 *
 * @author neoexpert
 */
public class MongoNodeIterator extends NodeIterator<Node> {

    /**
     *
     */
    public static final int INTEGER = 0;

    /**
     *
     */
    public static final int STRING = 2;

    /**
     *
     */
    public static final int TYPE = 1;

    /**
     *
     */
    protected Controller con;

    BasicDBObject fields = new BasicDBObject();
    BasicDBObject sort = new BasicDBObject();

    private FindIterable<Document> cursor;
    private MongoCursor<Document> it;
    private MongoClient mongo;
    private MongoCollection<Document> nodes;
    private String callback;

    /**
     *
     * @param categoryIterator
     */
    public MongoNodeIterator(MongoNodeIterator categoryIterator) {
        this.con = categoryIterator.con;
        this.fields = categoryIterator.fields;
        mongo = (MongoClient) con.getConnection();
		MongoDatabase db = mongo.getDatabase(ControllerBuilder.MONGO_DB);
		
		nodes = db.getCollection(ControllerBuilder.MONGO_NODES);
    }
   

    /**
     *
     * @param con
     */
    public MongoNodeIterator(Controller con) {
        this.con = con;
        mongo = (MongoClient) con.getConnection();
		MongoDatabase db = mongo.getDatabase(ControllerBuilder.MONGO_DB);
		
		nodes = db.getCollection(ControllerBuilder.MONGO_NODES);
    }

	public MongoNodeIterator(MongoController2 c, String query) {
		this.con=c;
		fields= BasicDBObject.parse(query);
        mongo = (MongoClient) con.getConnection();
		MongoDatabase db = mongo.getDatabase(ControllerBuilder.MONGO_DB);
		
		nodes = db.getCollection(ControllerBuilder.MONGO_NODES);
	}

    /**
     *
     * @param name
     * @param value
     * @return
     */
    @Override
    public NodeIterator addFieldToFilter(String name, Object value) {
        if (value instanceof Class) {
            value = ((Class<?>) value).getName();
        }
        if (value instanceof String[]) {
            BasicDBList or = new BasicDBList();

            for (String v : (String[]) value) {
                or.add(new BasicDBObject(name, v));
            }

            fields.put("$or", or);
            return this;
        }
        fields.put(name, value);
        return this;
    }

    

    /**
     *
     * @return
     */
    @Override
    public MongoNodeIterator build() {
        cursor = nodes.find(fields);
        if (!sort.isEmpty()) {
            cursor.sort(sort);
        }

        it = cursor.iterator();
        return this;

    }

    /**
     *
     */
    @Override
    public void close() {
        if(it!=null)
            it.close();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean hasNext() {
        if (it != null) {
            try {
                return it.hasNext();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public Node next() {
        if(it==null)throw new RuntimeException("iterator is not builded");

        Document doc = it.next();
         
	return MongoController2.createNode(con, doc);

    }

    /**
     *
     * @param orderby
     * @param desc
     * @return
     */
    @Override
    public NodeIterator sort(String orderby, boolean desc) {
        sort(orderby, desc ? -1 : 1);
        return this;
    }

    /**
     *
     * @param orderby
     * @param desc
     * @return
     */
    public NodeIterator sort(String orderby, int desc) {
        sort.append(orderby, desc);
        return this;
    }

    /**
     *
     * @param sort
     * @return
     */
    @Override
    public NodeIterator sort(JSONObject sort) {
        Set<String> keys = sort.keySet();
        for (String key : keys) {
            sort(key, sort.getInt(key));
        }
        return this;

    }

    /**
     *
     * @param name
     * @param value
     * @param lang
     * @param regex
     * @return
     */
    @Override
    public NodeIterator addAttributeToFilter(String name, String value, Locale lang, boolean regex) {
        if (regex) {
            value = value.replaceAll("%", ".*");
            Pattern compile = Pattern.compile(".*" + value, Pattern.CASE_INSENSITIVE);

            fields.put(name, compile);
        } else {
            fields.put(name + "." + lang, value);
        }
        // fields.putAll(or(eq(name,value)));
        return this;
    }

    /**
     *
     * @param key
     * @param value
     * @param regex
     * @return
     */
    @Override
    public NodeIterator addAttributeToFilter(String key, Object value, boolean regex) {
        if (regex) {
            value = ((String) value).replaceAll("%", ".*");
            Pattern compile = Pattern.compile(".*" + value, Pattern.CASE_INSENSITIVE);

            fields.put(key, compile);
        } else {
            fields.put(key, value);
        }
        // fields.putAll(or(eq(name,value)));
        return this;
    }

    /**
     *
     * @param name
     * @param value
     * @return
     */
    public NodeIterator addAttributeToFilter(String name, String value) {
        fields.put(name, value);
        return this;
    }

    /**
     *
     * @return
     */
    @Override
    public Iterator<Node> iterator() {
        return this;
    }

    /**
     *
     * @param path
     * @return
     */
    @Override
    public NodeIterator setPath(String path) {
        Pattern compile = Pattern.compile(path + ".*");
        fields.put("path", compile);
        return this;
    }

    /**
     *
     * @param name
     * @return
     */
    @Override
    public NodeIterator addAttributeToExists(String name) {
        fields.put(name, new Document("$exists", true));
        return this;
    }

    /**
     *
     * @param callback
     * @return
     */
    @Override
    public NodeIterator setCallback(String callback) {
        this.callback = callback;
        return this;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCallback() {
        return callback;
    }

   

}
