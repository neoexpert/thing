package servlets.admin;

import com.neoexpert.Log;
import core.*;
import core.attribute.*;
import core.controller.*;
import web.*;
import core.id.MongoObjectID;
import core.processor.Processor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;

/**
 * Servlet implementation class Admin
 */
@MultipartConfig

@WebServlet(asyncSupported = true, name = "FileUploadServlet", urlPatterns = {"/admin/*"}, loadOnStartup = 1)
public class Index extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
    }

    /**
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();
        

    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            response.getWriter().append(e.toString());
            Log.error(e);
            return;
        }

        request.setAttribute("controller", c);

        HttpSession session = request.getSession();
        User u = c.getUser(session);
        Locale lang = HttpController.getLang(request);

        if (!u.isLoggedIn("admin")) {
            String uri = WebControllerBuilder.getFullURL(request);
            session.setAttribute("redirectTO", uri);
            if (c.getNode("CATEGORIES").getChild("login") != null) {
                response.sendRedirect("/" + lang.getLanguage() + "/login");
            } else {
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");

                rd.forward(request, response);

            }

            return;
        }
        RequestDispatcher rd;
        String pi = request.getPathInfo();
        if(pi==null){
            response.sendRedirect("/admin/");
            return;
        }

        if (pi.equals("/")) {

            response.sendRedirect(c.getRoot().getBackURL());

            return;

        } 
        request.setAttribute("entity", pi);

        String ID = request.getParameter("ID");

        Node n;
        if (ID != null) {
            n = c.getNode(new MongoObjectID(ID));
            request.setAttribute("node", n);
        }

        HttpController.getLang(request);
        String wrapper = request.getParameter("wrapper");
        if (wrapper == null) {
            rd = request.getRequestDispatcher("/WEB-INF/Admin/ControlBar.jsp");
        } else {
            rd = request.getRequestDispatcher("/WEB-INF/Admin/WRAPPER.jsp");
        }

        rd.forward(request, response);

        // RequestDispatcher rd =
        // request.getRequestDispatcher("/WEB-INF/Admin/Admin.jsp");
        c.close();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            response.getWriter().append(e.toString());
            Log.error(e);
            return;
        }
        HttpSession session = request.getSession();
        User u = c.getUser(session);
        if (!u.isLoggedIn("admin")) {
            // RequestDispatcher rd =
            // request.getRequestDispatcher("/WEB-INF/login.jsp");

            // rd.forward(request, response);
            response.sendRedirect("/login");
            String uri = WebControllerBuilder.getFullURL(request);
            session.setAttribute("redirectTO", uri);
     		Log.log("redirectTO: " + uri);

            return;
        }

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");


        Locale lang = HttpController.getLang(request);

        // String parent = request.getParameter("parent");
        Processor.process(new HttpParams(request, response), c, u, lang);

    }

    private void exportCSV(Node n, PrintWriter out, Set<String> attrs, Locale lang, String type) throws JSONException {

        String v;
        if (type != null) {
            if (type.equals(n.getType())) {
                for (String attr : attrs) {
                    switch (attr) {
                        case "ID":
                            v = n.getID() + "";
                            break;
                        case "name":
                            v = n.getName();
                            break;
                        case "parent":
                            v = n.getParentID() + "";
                            break;
                        case "type":
                            v = n.getType();
                            break;
                        default:
                            Attribute a = n.getAttribute(attr);
                            if (a == null) {
                                v = "";
                                break;
                            }
                            v = a.getValue(lang).toString();
                            break;
                    }
                    out.print("\"");
                    out.print(v.replaceAll("\"", "\"\""));
                    out.print("\"");
                    out.print(",");

                }
                out.println();
            }
        }

        NodeIterator<Node> it = n.getChildren().build();

        while (it.hasNext()) {
            Node c = it.next();

            exportCSV(c, out, attrs, lang, type);
        }
    }

}
