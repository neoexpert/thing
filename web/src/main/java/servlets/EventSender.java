package servlets;

import com.neoexpert.Log;
import core.Client;
import core.User;
import web.*;
import core.events.Event;
import core.processor.JSONParams;
import core.processor.Processor;
import core.processor.ResultWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author neoexpert
 */
@ServerEndpoint(value = "/events", configurator = core.GetHttpSessionConfigurator.class)
public class EventSender {

	private static final HashMap<String, Client> CLIENTS = new HashMap<>();

	private HttpController c;

	/**
	 *
	 * @return
	 */
	public static int getClientsCount() {
		if (CLIENTS != null) {
			return CLIENTS.size();
		}
		return 0;
	}

	/**
	 *
	 * @param session
	 * @param config
	 */
	@OnOpen
	public void onOpen(Session session, EndpointConfig config) {
		Log.log("websocket oppened");
		HttpSession httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class
				.getName());
		if(httpSession==null)
		c = new WebControllerBuilder().getHttpController();
		else
		c = new WebControllerBuilder(httpSession.getServletContext())
				.getHttpController();

		String domain = (String) config.getUserProperties().get("domain");
		Locale lang = null;
		boolean su = false;

		if (httpSession != null) {
			lang = (Locale) httpSession.getAttribute("locale");
			su = httpSession.getAttribute("ROOT") != null;
		}
		if (lang
				== null) {
			lang = Locale.ENGLISH;
		}


		User u = null;
		if (httpSession
				!= null) {
			u = c.getUser(httpSession);
		}
		Client client = new WebClient(c, u, session, lang);
		if (u == null) {
			sendToClient(client, "{cmd:\"login\"}");
		}

		synchronized (CLIENTS) {
			CLIENTS.put(session.getId(), client);
		}
	}

	public static void sendToClient(Client c, String text) {
		try {
			c.send(text);
		} catch (IOException e1) {
			String sid = c.getSessionID();
			CLIENTS.remove(sid);
			try {
				c.close();
			} catch (IOException e) {
				Log.error(e);
			}

			Log.error(e1);
		}
	}

	/**
	 *
	 * @param message
	 * @param session
	 * @return
	 */
	@OnMessage
	public String onMessage(String message, Session session) {
		final JSONObject jo;
		try {
			jo = new JSONObject(message);
		} catch (JSONException e) {
			Log.error(e);
			return "error";
		}
		String cmd;
		try {
			if (jo.has("cmd")) {
				cmd = jo.getString("cmd");
			} else {
				cmd = jo.getString("type");
			}

		} catch (JSONException e) {
			Log.error(e);
			return "error";
		}

		synchronized (CLIENTS) {
			final Client client = CLIENTS.get(session.getId());

			User u = client.getUser();
			switch (cmd) {
				case "login":
					return new JSONObject()
							.put("cmd", "result")
							.put("lock", jo.getInt("lock"))
							.put("result", loginUser(jo, client)).toString();
				case "regster":
					return registerUser(jo, client);
			}
			if (u == null) {
				return "{}";
			}

			ResultWriter rw = new ResultWriter() {
				@Override
				public void write(String result) {
					try {
						client.send(result);
					} catch (IOException e) {
						Log.error(e);
					}

				}

				@Override
				public void write(JSONObject result) {
					Object lock = null;
					if (jo.has("lock")) {
						lock = jo.get("lock");
					}
					String s;
					if (lock == null) {
						s = result.toString();
					} else {
						s = new JSONObject()
								.put("cmd", "result")
								.put("lock", lock)
								.put("result", result).toString();
					}
					try {
						client.send(s);

					} catch (IOException ex) {
						Log.error(ex);
					}

				}
			};
			JSONParams pi = new JSONParams(jo, rw);
			pi.setClient(client);
			Processor.process(pi, client.getController(), u, Locale.ENGLISH);

			return "{}";
		}

	}

	private String registerUser(JSONObject jo, Client c) {
		String username;
		if (jo.has("username")) {
			username = jo.getString("username");
		} else {
			username = UUID.randomUUID().toString();
		}
		String pw = jo.getString("pw");

		User u = c.getController().addUser(username, pw);
		c.setUser(u);
		if (u == null) {
			return "{cmd:\"error\"}";
		} else {
			return new JSONObject().accumulate("cmd", "registerok").accumulate("username", u.getName()).toString();
		}
	}

	private JSONObject loginUser(JSONObject jo, Client c) {
		String username = jo.getString("username");
		String pw = jo.getString("password");

		User u = c.getController().userLogin(username, pw);
		c.setUser(u);
		if (u == null) {
			return new JSONObject().put("cmd", "wronglogin");
		} else {
			if (jo.has("version")) {
				c.setVersion(jo.getInt("version"));
			}
			JSONObject result = new JSONObject();
			result.put("cmd", "loginok");
			result.put("user", u.toJSON());
			return result;
		}
	}

	/**
	 *
	 * @param t
	 */
	@OnError
	public void onError(Session session, Throwable t) {
		synchronized (CLIENTS) {
			Client c = CLIENTS.get(session.getId());
			if (t instanceof IOException) {
				Log.log(c.getUser(), "websocket IOException");
			}
			Log.error(c.getUser(), "websocket error: " + t);
		}
	}

	/**
	 *
	 * @param session
	 */
	@OnClose
	public void onClose(Session session) {
		c.deleteSession(session.getId());
		synchronized (CLIENTS) {
			Client c = CLIENTS.remove(session.getId());
			try{
				c.close();
			}
			catch(IOException e){
			Log.error(c.getUser(),e);
			}
			Log.log(c.getUser(), c.getUser().getName() + " closed Session " + session.getId());
		}
	}

	public static void sendEvent(String session, Event e) {
		Client c = CLIENTS.get(session);

		if (c != null) {
			JSONObject jevent = new JSONObject();
			jevent.put("cmd", "event");
			jevent.put("event", e.toJSON());
			sendToClient(c, jevent.toString());
		}

	}
	/**
	 *
	 * @param event
	 * @param u
	 */
	/*public static void sendAll(Event event, User u) {
        JSONObject e= 
					new JSONObject()
						.put("cmd", "event")
						.put("type", event.getType())
            .put("name", event.getName())
            .put("previousvalue", event.getPreviousValue())
						.put("newvalue", event.getNewValue());

        if (u != null) {
            e.put("user", u.toJSON());
        }

        Node n = event.getNode();
				JSONObject jnode=n.toJSON();
        User o = n.getOwner();
				if(o!=null){
        	Object nick = o.doc.get("nick");
        	if (nick == null) {
          	jnode.put("owner_name", o.getName());
        	} else {
          	jnode.put("owner_name", nick.toString());
        	}

        	jnode.put("owner_groups", o.getGroups());
				}

        jnode.put("htmlrow", event.getNode().toHTMLROW(Locale.ENGLISH));
        e.accumulate("node", jnode);

        synchronized (CLIENTS) {
            for (Client c : CLIENTS.values()) {
		Session s=c.getSession();
                if (s.isOpen()) {
                    try {
                        s.getBasicRemote().sendText(e.toString());
                    } catch (IOException ex) {
                        // TODO Auto-generated catch block
                        // ex.printStackTrace();
                    }
                }
            }

        }
    }*/
}
