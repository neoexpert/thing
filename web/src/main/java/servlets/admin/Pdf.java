package servlets.admin;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import core.id.ObjectID;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import templates.Template;

/**
 * Servlet implementation class Test
 */
@WebServlet("/admin/pdf")
public class Pdf extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pdf() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            response.getWriter().append(e.toString());
            Log.error(e);
            return;
        }
        HttpSession session = request.getSession();
        User u = c.getUser(session);
        String wrapper = request.getParameter("wrapper");

        if (!u.isLoggedIn("admin")) {
            // RequestDispatcher rd =
            // request.getRequestDispatcher("/WEB-INF/login.jsp");

            // rd.forward(request, response);
            Locale lang = HttpController.getLang(request);

            response.sendRedirect("/" + lang + "/login");

            // Index i = new Index();
            // i.doGet(request, response);
            if (wrapper == null) {
                String uri = WebControllerBuilder.getFullURL(request);
                session.setAttribute("redirectTO", uri);
            }
            return;
        }
        ObjectID id = new ObjectID(request.getParameter("ID"));
        Template tcat;
        Node cat;
        String template;
        try {
            cat = c.getNode(id);
            template = cat.getFilledNTemplate("PDF", HttpController.getLang(request));

        } catch (Exception e) {
            Log.error(e);
            response.getWriter().append(e.toString());
            return;
        }

        String filename = ControllerBuilder.getRootPath(request.getServerName()) + "/pdf/" + cat.getID();
        FileWriter writer = new FileWriter(filename + ".html");
        try {
            writer.write(template);
        } catch (Exception e) {
            Log.error(e);
            response.getWriter().append(e.toString());
            return;
        } finally {
            writer.close();
        }

        String[] commands = {"/usr/bin/xhtml2pdf", filename + ".html"};
        // this could be set to a specific directory, if desired
        File dir = new File(ControllerBuilder.getRootPath(request.getServerName()) + "/pdf");
        BufferedReader is = null;
        BufferedReader es = null;

        try {
            Process process;
            process = Runtime.getRuntime().exec(commands, null, dir);
            //else
            //process = Runtime.getRuntime().exec(commands);
            String line;
            is = new BufferedReader(new InputStreamReader(process.getInputStream()));
            while ((line = is.readLine()) != null) {
                Log.log(line);
            }
            es = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = es.readLine()) != null) {
                Log.log(line);
            }

            int exitCode = process.waitFor();
            if (exitCode == 0) {
                File file = new File(filename + ".pdf");
                InputStream in = new FileInputStream(file);
                response.setDateHeader("Last-Modified", file.lastModified());
                response.setHeader("Content-Type", getServletContext().getMimeType(file.getAbsolutePath()));
                response.setHeader("Content-Length", String.valueOf(file.length()));
                response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
                //org.apache.tomcat.util.http.fileupload.IOUtils.copy(in, response.getOutputStream());
                //HIER IST DAS PROBLEM
                in.close();

            } else {
                response.getWriter().append("Something bad happend. Exit code: " + exitCode);
            }

        } // try
        catch (IOException | InterruptedException e) {
            Log.error(e);
        } // catch
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
            if (es != null) {
                try {
                    es.close();
                } catch (IOException e) {
                }
            }
        } // finally

    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
