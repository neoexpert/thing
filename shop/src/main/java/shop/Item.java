package shop;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.attribute.Attribute;
import core.controller.Controller;
import core.id.ObjectID;
import java.util.Iterator;
import org.bson.Document;

/**
 * Element eines Warenkorbes oder einer Besstellung.
 *
 * @author neoexpert
 */
public class Item extends Node {

	private ObjectID productID;
	private int amount = 1;

	/**
	 *
	 * @return @throws Exception
	 */
	public Product getProduct() throws Exception {
		return (Product) c.getNode(productID);
	}

	/**
	 *
	 * @param c
	 * @param doc
	 */
	public Item(Controller c, Document doc) {
		super(c, doc);
	}

	/**
	 *
	 * @param userid
	 * @param c
	 */
	public Item(ObjectID userid, Controller c) {
		super(userid, c);
	}

	/**
	 *
	 * @param con
	 */
	public Item(Controller con) {
		super(null, con);
	}

	/**
	 *
	 * @param children
	 * @param u
	 * @throws Exception
	 */
	public void addChildren(Iterator<Node> children, User u) throws Exception {
		while (children.hasNext()) {
			addChild(children.next(), u);
		}
	}

	/**
	 *
	 * @param amount
	 * @throws Exception
	 */
	public void setAmount(int amount) throws Exception {
		this.amount = amount;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	public int getAmount() throws Exception {
		return amount;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public boolean canInherit() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 *
	 * @return
	 */
	public User getSuplier() {
		String pid = (String) doc.get("PRODUCT_ID");
		Node p;
		try {
			p = c.getNode(new ObjectID(pid));

			Attribute suplier = p.getAttribute("suplier");
			return c.getUser(suplier.getValue().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		return null;
	}
}
