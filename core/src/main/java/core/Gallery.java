package core;

import core.controller.Controller;
import core.id.ObjectID;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Gallery extends Node {

	static {
		addChildType(Gallery.class, Picture_OLD.class);
		addChildType(Gallery.class, Link.class);
		

	}

	/**
	 *
	 * @param userID
	 * @param c
	 */
	public Gallery(ObjectID userID, Controller c) {
		super(userID, c);
	}

	/**
     *
     * @param c
	 * @param doc
     */
    public Gallery(Controller c,Document doc) {
		super(c,doc);
	}

}
