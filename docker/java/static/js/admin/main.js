$(function () {
	$(".myname").get(0).addEventListener("input",
			function () {
				var b = $(this).next();
				b.show();
				b.click(saveMyName);
			}, false);
});

function saveMyName() {
	var button = $(this);
	var b = $(this).prev().text();
	$.post("/admin/category", {ID: ID, cmd: "changeName", value: b},
			function (a) {
				if (a === "ok") {
					button.hide();
				} else {
					$(this).next().html(a);
				}
			});
}

function reply(me) {
	var ta = $(me).prev();
	if (ta.is(":visible"))
	{
		ta.hide();
		addChild(undefined, "core.Comment", $(me), function () {
			ta.val("");
		}, {message: ta.val()});
	} else {
		ta.show();
		ta.focus();
	}
}

function toggleTop(me) {
	$(me).prev().toggle();
}

function setImage(img, a) {
	img.attr("src", "/media/" + a.value + ".jpg");
}

function setCommentMessage(e, v) {
	e.html(v.value);
}

function editInCM(a, e) {
	e.append("<textarea></textarea>");
	var saveButton = $("<button hidden>save</button>");

	e.append(saveButton);

	e = e.find("textarea");
	e.val(a.value);
	var editor = CodeMirror.fromTextArea(e.get()[0], {
		lineNumbers: true,
		mode: "htmlmixed",
		extraKeys: {
			"Ctrl-S": function (instance) {
				$(instance.getTextArea()).next().next().click()
			},
			"Ctrl-Space": "autocomplete"
		}
	});
	editor.on("change", function () {
		editor.save();
		saveButton.show();
	});
	saveButton.click(function () {
		setAttribute(ID, "value", editor.getValue());
		saveButton.hide();
	});
}




function loadChildren(me, ID) {
	me.removeClass("unfilled");
	var attrs = {};
	attrs["ID"] = ID;
	attrs["cmd"] = "getChildrenTemplates";
	attrs["name"] = me.attr("data-name");
	attrs["type"] = me.attr("data-type");
	attrs["template"] = me.attr("data-template");


	$.post("/admin/rest", attrs, function (responseText) {
		me.html(responseText);
		me.find(".child.unfilled").each(function () {
			fillTemplate($(this));
		});
	});
}

function loadChild(me, ID) {
	me.removeClass("unfilled");

	var attrs = {};
	attrs["ID"] = ID;
	attrs["cmd"] = "getChildTemplate";
	attrs["name"] = me.attr("data-name");


	$.post("/admin/rest", attrs, function (responseText) {
		me.html(responseText);
		fillTemplate($(me));
		me.find(".child.unfilled").each(function () {
			var ID = $(this).attr("data-id");
			loadChild($(this));
		});
		me.find(".children.unfilled").each(function () {
			var ID = $(this).attr("data-id");
			loadChildren($(this), ID);
		});

	});
}
function getTemplate(ID, callback) {
	var attrs = {};
	attrs["ID"] = ID;
	attrs["cmd"] = "getTemplate";

	$.post("/admin/rest", attrs, function (responseText) {
		if (typeof callback == "function") {
			callback(responseText);
		}
	});
}




function generateTemplate() {
	$.post(window.location, {
		cmd: "generateAdminTemplate"
	}, function (responseText) {
		if (responseText !== "ok")
			alert(responseText);

	});
}

function copyNode(id, parent, callback) {
	$.post(window.location, {
		childID: id,
		cmd: "copyNode",
		parent: parent

	}, function (responseText) {
		if (typeof callback === "function") {
			try {
				var node = JSON.parse(responseText);
				callback(node);

			} catch (e) {
				alert(responseText);
				return;
			}
		}

	});
}

function moveCategory(id, parent) {
	$.post("/admin/rest", {
		ID: id,
		cmd: "moveCategory",
		parent: parent

	}, function (responseText) {
		if (responseText === "ok") {

		}
	});
}
function setComplex(id, me) {
	$.post("/admin/category", {
		ID: id,
		cmd: "setComplex",
		complex: me.checked

	}, function (responseText) {
		if (responseText === "ok") {

		}
	});
}

function saveChanged() {
	$(".changed").each(function (index) {
		$(this).next().click();
	});
}
function inheritFromParent(id, attrid, me) {
	$.post("/admin/category", {
		ID: id,
		attrid: attrid,
		cmd: "inheritFromParent",
		inheritFromParent: me.checked

	}, function (responseText) {
		var newID = parseInt(responseText);
		if (newID > 0)
			$("#" + id).attr("id", newID);
	});
}

function saveAttribute(name, me, parent) {

	var p = $(me);
	while (!parent)
	{
		p = p.parent();
		parent = p.attr("data-id");
	}
	var e = $(me).prev();
	var value = e.val();
	if (e.hasClass("CodeMirror"))
		value = e[0].CodeMirror.getValue();
	$.post("/admin", {
		ID: parent,
		attrname: name,
		cmd: "saveAttribute",
		value: value

	}, function (responseText) {
		if (responseText === "ok") {
			$(me).prev().removeClass("changed");
			$(me).hide();
			$(me).next().html("");

		} else
			$(me).next().html(responseText);
	});
}
function changeName(id, me) {
	var value = $(me).prev().val();
	$.post("/admin/category", {
		ID: id,
		cmd: "changeName",
		value: value

	}, function (responseText) {
		if (responseText === "ok") {
			$(me).prev().removeClass("changed");
			$(me).hide();
			$(me).next().html("");

		} else
			$(me).next().html(responseText);
	});
}




function deleteChildById(id, callback) {
	$.post(window.location, {
		_id: id,
		cmd: "remove"
	}, function (responseText) {
		if (responseText === "ok")
			if (typeof callback === "function")
				callback();
	});
}
function deleteChildFromParent(parent, id, callback) {
	$.post(window.location, {
		ID: parent,
		childID: id,
		cmd: "deleteChild"
	}, function (responseText) {
		if (responseText === "ok")
			if (typeof callback === "function")
				callback();
	});
}
function iOrderID(me) {
	$.post("/admin/category", {
		ID: me.value,
		cmd: "incrementOrderID"

	}, function (responseText) {
		if (responseText === "ok") {
			var oID = parseInt($(me.parentElement).find("#orderID").html());
			$(me.parentElement).find("#orderID").html("" + (oID + 1));
		}
	});
}
function dOrderID(me) {
	$.post("/admin/category", {
		ID: me.value,
		cmd: "decrementOrderID"
	}, function (responseText) {
		if (responseText === "ok") {
			var oID = parseInt($(me.parentElement).find("#orderID").html());
			$(me.parentElement).find("#orderID").html("" + (oID - 1));
		}
	});
}
function setOrderID(ID, order_id) {
	$.post("/admin/category", {
		ID: ID,
		cmd: "setOrderID",
		order_id: order_id

	}, function (responseText) {
		if (responseText === "ok") {

		}
	});
}
function toggleFlag(me, scope) {
	var checked = $(me).prop("checked");
	$.post("/admin/category", {
		ID: me.value,
		visible: checked,
		scope: scope,
		cmd: "toggleFlag"

	}, function (responseText) {
		if (responseText === "ok") {
			var oID = parseInt($(me.parentElement).find("#orderID").html());
			$(me.parentElement).find("#orderID").html("" + (oID + 1));
		}
	});
}
function addChild(name, type, e, callback, childattrs) {
	var parent = undefined;
	while (!parent)
	{
		parent = e.attr("data-id");
		e = e.parent();
	}
	var attrs = {};
	if (childattrs)
		attrs["child"] = JSON.stringify(childattrs);

	attrs["ID"] = parent;
	attrs["name"] = name;
	attrs["type"] = type;
	attrs["cmd"] = "addChild";



	$.post("/admin", attrs, function (responseText) {

		if (responseText === "wrongname") {
			tv.focus();
			return;
		}
		if (responseText === "catexists") {
			alert("Category already exists");
			return;
		}
		var response = JSON.parse(responseText);
		if ((typeof callback) == "function")
			callback(response)
		//if(go){
		//  document.location.href = response.backurl;
		//}
		//$('#categories tr:first').after(response.htmlrow);
		// $('#categories tr:first').after(
		// "<tr><td></td><td>"+type+"</td><td><a href='/admin/category?ID=" +
		// obj.ID
		// + "'>" + catname + "</a></td</tr>");

	});
}

function addChildTo(type, callback) {
	var tv = $("#newCategoryName");
	if (type === "ATTRIBUTE")
		tv = $("#newAttributeName");
	var catname = tv.val();

	$.post(window.location, {
		ID: parent,
		name: catname,
		type: type,
		cmd: "addChild"
	}, function (responseText) {

		if (responseText === "wrongname") {
			tv.focus();
			return;
		}
		if (responseText === "catexists") {
			alert("Category already exists");
			return;
		}
		var response = JSON.parse(responseText);
		if (callback)
			callback(response);
		else
			$('#categories tr:first').after(response.htmlrow);
		// $('#categories tr:first').after(
		// "<tr><td></td><td>"+type+"</td><td><a href='/admin/category?ID=" +
		// obj.ID
		// + "'>" + catname + "</a></td</tr>");

	});
}
function addThumbnail(parent) {
	var catname = "THUMBNAIL_TEMPLATE";
	var type = "TEMPLATE";
	$.post(window.location, {
		name: catname,
		type: type,
		cmd: "addChild"

	}, function (responseText) {
		if (responseText === "wrongname") {
			$("#newCategoryName").focus();
			return;
		}
		if (responseText === "catexists") {
			alert("Category already exists");
			return;
		}
		var obj = JSON.parse(responseText);

		$('#categories tr:first').after(
				"<tr><td></td><td><a href='/admin/category?ID=" + obj.ID + "'>"
				+ catname + "</a></td</tr>");

	});
}
function addTemplate() {
	var catname = "TEMPLATE";
	var type = "TEMPLATE";
	$.post(window.location, {
		name: catname,
		type: type,
		cmd: "addChild"

	}, function (responseText) {
		if (responseText === "wrongname") {
			$("#newCategoryName").focus();
			return;
		}
		if (responseText === "catexists") {
			alert("Category already exists");
			return;
		}
		var obj = JSON.parse(responseText);

		$('#categories tr:first').after(
				"<tr><td></td><td><a href='/admin/category?ID=" + obj.ID + "'>"
				+ catname + "</a></td</tr>");

	});
}
function addAttribute(id, me) {
	var attrname = $("#newAttributeName").val();
	$.post("/admin/attribute", {
		name: catname,
		catID: id
	}, function (responseText) {
		if (responseText === "wrongname") {
			$("#newAttributeName").focus();
			return;
		}
		if (responseText === "alreadyexists") {
			alert("Attribute already exists");
			return;
		}
		var obj = JSON.parse(responseText);

		$('#attributes tr:first').after(
				"<tr><td></td><td><a href='/admin/category?ID=" + obj.ID + "'>"
				+ attrname + "</a></td</tr>");

	});
}

function addToList(listID) {

	$.post(window.location, {
		listID: listID,
		cmd: "addToList"
	}, function (responseText) {

	});
}



function setLang(lang) {
	$.post("/setlang", {
		lang: lang
	}, function (responseText) {
		if (responseText == "ok")
			window.location.reload(false);
	});
}
function loadArea(url) {
	$.get(url, {
		wrapper: "huhu"
	}, function (responseText) {
		$("#wrapper").html(responseText);
		// alert(responseText);
	});
	window.history.pushState("", "", url);

}
window.addEventListener('popstate', function (event) {
	loadArea(event.state);
});

function deleteAttr(ID, name) {
	var cmd = {};
	cmd.ID = ID;
	cmd.name = name;
	cmd.cmd = "deleteAttr";
	adminRequest(cmd, function (r) {
		alert(r);
	});
}

function addAttr(ID, name) {
	var cmd = {};
	cmd.ID = ID;
	cmd.name = name;
	cmd.cmd = "addAttr";
	adminRequest(cmd, function (r) {
		alert(r);
	});
}

function processAttrTemplate(name, attr, e) {
	e.append(attr.type.template);
	switch (name) {
		case "description":
		case "meta-description":
			initTextArea(name, attr, e);

			break;
		case "multilang":
		case "hidden":
		case "dynamic":
		case "xmlparser":

			initCheckBox(name, attr, e);
			break;
		case "value_type":
			initSelectInteger(name, attr, e.find("select"));

			break;
		case "groups":
			initSet(name, attr, e);
			break;
	}
}

function addToSet(parent, name, value) {

	request({
		ID: parent,
		cmd: "addToSet",
		name: name,
		value: value

	}, function (responseText) {


	});
}
function removeFromSet(parent, name, value) {
	request({
		ID: parent,
		cmd: "removeFromSet",
		value: value,
		name: name
	}, function (responseText) {


	});
}

function initSet(name, attr, e) {
	var ul = e.find("ul");
	var arrayLength = attr.value.length;
	for (var i = 0; i < arrayLength; i++) {
		ul.append("<li>" + attr.value[i] + "</li>");
	}
	var addButton = $("<button>add</button>");
	e.append("<input type='text'>");
	e.append(addButton);
	addButton.click(function () {
		alert($(this).prev().val());
		addToSet(attr.parent, name, $(this).prev().val());
	});
}


function processDefaultAttrTemplate(name, attr, e) {
	var input = $("<input type='text'>");
	var saveButton = $("<button hidden>save</button>");
	input.on('input', function () {
		saveButton.show();
	});
	saveButton.click(function () {
		setAttribute(attr.parent, name, input.val());
		saveButton.hide();
	});
	e.append(input);
	if (!attr.type) {
		var addTypeButton = $("<button>+</button>");
		addTypeButton.click(function () {
			addType(name);
		});
		e.append(addTypeButton);
	}

	e.append(saveButton);
	input.val(attr.value);
}

function initTextArea(name, attr, e) {
	var ta = e.find("textarea");
	var saveButton = $("<button hidden>save</button>");
	saveButton.click(function () {
		setAttribute(attr.parent, name, ta.val());
		saveButton.hide();

	});
	ta.val(attr.value);
	ta.on('change keyup paste', function () {
		saveButton.show();
	});
	e.append(saveButton);
}

function addType(name) {
	var cmd = {};
	cmd.name = name
	cmd.cmd = "addType";
	adminRequest(cmd, function (r) {
		window.location = "/admin/type?ID=" + r;
	});
}
function addToMapClick(me) {
	var b = $(me);
	var value = b.prev().val();
	var key = b.prev().prev().val();
	var name = b.parent().parent().parent().attr("data-name");
	addToMap(ID, name, key, value);
}

function addToMap(ID, name, key, value) {
	var cmd = {};
	cmd.cmd = "addToMap";
	cmd.ID = ID;
	cmd.name = name;

	cmd.key = key;
	cmd.value = value;
	adminRequest(cmd, function () {

	});
}

function initMap(name, attr, e) {
	var entries = attr.value;
	e.html(attr.type.template);
	var container = e.find(".container");

	var l = entries.length;
	for (var i = 0; i < l; i++) {
		var element = $(attr.type.element_template);
		container.append(element);
		var entry = entries[i];
		var key = Object.keys(entry)[0];
		element.find(".value").val(entry[key]);
		element.find(".key").val(key);
		element.attr("data-value", key);
	}




	e.val(attr.value).change();
	e.change(function () {
		setAttribute(ID, name, $(this).val());
	});
}

function initSelectInteger(name, attr, e) {
	e.val(attr.value).change();
	e.change(function () {
		setAttribute(ID, name, $(this).val());
	});
}

function initCheckBox(name, attr, e) {
	e = e.find("input");
	e.prop('checked', attr.value);
	e.click(function () {
		setAttribute(ID, name, this.checked);
	});
}


function loadAttributes(ID) {
	var cmd = {};
	cmd["ID"] = ID;
	cmd["cmd"] = "getAttributes";
	cmd["admin"] = true;

	adminRequest(
			cmd,
			function (attrs) {
				var i = 0;
				for (var key in attrs) {
					if (attrs.hasOwnProperty(key)) {
						var attr = attrs[key];
						var type = attr.type;
						if (type)
							if (type.hidden)
								continue;

						var attrhtml = $("<div class='attribute'></div>");
						var html = "";
						html += "<div class='table_row'>";
						html += "<div class='table_cell'>";
						if (type)
							html += "<a href='/admin/type?ID=" + type._id + "'>";
						html += key;
						if (type)
							html += "</a>";
						html += "</div>";
						html += "<div class='table_cell attribute_wrapper'>";
						if (type) {
							if (type.template) {
								processAttrTemplate(key, attr, attrhtml);
							} else
								processDefaultAttrTemplate(key, attr, attrhtml);
						} else
							processDefaultAttrTemplate(key, attr, attrhtml);
						html += "</div>";
						html += "<div class='table_cell'>";
						html += "<button onclick=\"deleteAttr(ID,'" + key + "')\">delete</button>";
						html += "</div>";
						html += "</div>";
						var attrrow = $(html);
						attrhtml.attr("data-name", key);
						attrhtml.addClass("filled");
						attrrow.find(".attribute_wrapper").append(attrhtml);
						$("#attrs").append(attrrow);
					}
				}

			});
}


