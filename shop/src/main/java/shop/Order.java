package shop;

import com.neoexpert.Log;
import core.Node;
import core.SendEmail;
import core.User;
import core.controller.Controller;
import core.controller.NodeIterator;
import core.id.ObjectID;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import org.bson.Document;
import templates.HtmlTemplate;

/**
 * Enthält Artikel. hat Lieferadresse und
 *
 * @author neoexpert
 */
public class Order extends Node {

    static {
        addChildType(Order.class, Cart.class);
        addChildType(Order.class, Suppliers.class);
    }

    /**
     *
     * @param userID
     * @param con
     */
    public Order(ObjectID userID, Controller con) {
        super(userID, con);
    }

   /**
     *
     * @param c
	 * @param doc
     */
    public Order(Controller c,Document doc) {
		super(c,doc);
	}
    /**
     *
     * @param cart
     * @param u
     * @throws Exception
     */
    public void setCart(Cart cart, User u) throws Exception {
        c.moveNode(cart, this, u);
    }

    /**
     *
     * @param customer
     * @return
     * @throws Exception
     */
    public Order setCustomer(User customer) throws Exception {
        Set<String> keys = customer.doc.keySet();
		keys.stream().filter((key) -> (doc.get(key) == null)).forEach((key) -> {
			doc.put(key, customer.doc.get(key));
		});
        return this;
    }

    /**
     *
     * @param u
     * @throws Exception
     */
    public void processSuplier(User u) throws Exception {
        Suppliers supliers = new Suppliers(u.getID(), c);
        supliers.setName("SUPLIERS");
        try {
            supliers = (Suppliers) addChild(supliers, u);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.error(e);
        }
        Cart cart;
        try {
            cart = getCart();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.error(e);
            return;
        }
        Iterator<shop.Item> it;
        try {
            it = cart.getItems();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.error(e);
            return;
        }
        while (it.hasNext()) {
            Item item = (Item) it.next();
            User suplier = item.getSuplier();
            if (suplier == null) {
                continue;
            }
            Supplier s = null;
            try {
                s = (Supplier) supliers.getChild(suplier.getName());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.error(e);
            }
            if (s == null) {
                s = new Supplier(u.getID(), c);
                s.setName(suplier.getName());

                try {
                    s = (Supplier) supliers.addChild(s, u);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Log.error(e);
                }
            }
            try {
                c.copyNode(u, item, s.getID());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.error(e);
            }

        }
        sendMailToSupliers();
    }

    private void sendMailToSupliers() throws Exception {
        NodeIterator<Supplier> it = getChild("SUPLIERS")
                .getChildren(Supplier.class)
                .build();
        while (it.hasNext()) {
            Node s = it.next();
            String mail = ((HtmlTemplate) s.getTemplate()).fill(s, Locale.GERMANY);
            SendEmail sm = new SendEmail(c.getMailSettings());
            sm.sendEmail(s.getName(), "Lieferschein",
                    mail);
        }
    }

    private Cart getCart() throws Exception {
        return (Cart) getChild("CART");
    }
}
