package servlets.admin;

import com.neoexpert.Log;
import core.Node;
import core.User;
import core.controller.ControllerBuilder;
import web.*;
import ebay.EbayClient;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * Servlet implementation class Path
 */
@WebServlet("/admin/ebay")
public class Ebay extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ebay() {
        super();
        // TODO Auto-generated constructor stub
    }

    public static String from;
    public static String to;

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpController c;
        try {
            c = new WebControllerBuilder(request).getHttpController();
        } catch (Exception e) {
            response.getWriter().append(e.toString());
            Log.error(e);
            return;
        }
        request.setAttribute("controller", c);
        response.setContentType("text/html");

        if (request.getParameter("setparams") != null) {
            PrintWriter out = response.getWriter();
            out.println("<form>");
            out.println("from");
            out.println("<input type=\"text\" value=\"2017-09-20T21:59:59.005Z\" name=\"from\">");
            out.println("to");
            out.println("<input type=\"text\" value=\"2018-01-16T21:59:59.005Z\" name=\"to\">");
            out.println("<input type=\"submit\" name=\"bdaytime\">");

            out.println("</from>");

            return;
        }
        String fromdate = request.getParameter("from");
        String todate = request.getParameter("to");
        if (fromdate != null) {
            from = toISO8601UTC(fromISO8601UTC(fromdate));
        }
        if (todate != null) {
            to = toISO8601UTC(fromISO8601UTC(todate));
        }

        HttpSession session = request.getSession();
        User u = c.getUser(session);
        String wrapper = request.getParameter("wrapper");

        if (!u.isLoggedIn("admin")) {
            // RequestDispatcher rd =
            // request.getRequestDispatcher("/WEB-INF/login.jsp");

            // rd.forward(request, response);
            Locale lang = HttpController.getLang(request);

            response.sendRedirect("/" + lang.getLanguage() + "/login");

            // Index i = new Index();
            // i.doGet(request, response);
            if (wrapper == null) {
                String uri = WebControllerBuilder.getFullURL(request);

                if (request.getRequestURI().equals("/admin")) {
                    try {
                        uri = c.getRoot().getBackURL();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        Log.error(e);
                    }
                }

                session.setAttribute("redirectTO", uri);
            }
            return;
        }

        EbayClient ec = new EbayClient();
        ec.setSessionID((String) session.getAttribute("ebay_session"));
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String code = request.getParameter("code");
        String login = request.getParameter("login");
        if (login != null) {
            response.sendRedirect(
                    "https://signin.ebay.com/authorize?client_id=Alexande-Test-PRD-f2466ad41-daaad47d&response_type=code&redirect_uri=Alexander_Tyury-Alexande-Test-P-hgtzx&scope=https://api.ebay.com/oauth/api_scope/sell.inventory.readonly");
        }

        String ebaytkn = request.getParameter("ebaytkn");
        if (ebaytkn != null) {
            try {
                String authtoken = ec.fetchToken("https://api.ebay.com/ws/api.dll", out);
                session.setAttribute("authtoken", authtoken);
                //response.setContentType("text/xml");
                Node n = c.getRoot().getChild("CATEGORIES").getChild("ebay");
                //

                ec.importEbayItems(n, c, u, out, authtoken, from, to);

                //ec.importEbayItems(n,c,u,out, authtoken,"2017-09-20T21:59:59.005Z","2018-01-16T21:59:59.005Z");
            } catch (ParserConfigurationException | SAXException e) {
                // TODO Auto-generated catch block
                Log.error(e);

                out.println(e);
                e.printStackTrace(out);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.error(e);
                out.println(e);
                e.printStackTrace(out);
            }

            return;
        }
        out.println("<form><input type='text' name='apicall'><textarea name='request'></textarea><input type='submit' value='Submit'></form>");
        try {
            String req = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<GetSessionIDRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">\r\n  <RuName>Alexander_Tyury-Alexande-Test-P-hgtzx</RuName>\r\n</GetSessionIDRequest>";
            String sessid = ec.getSessionID("https://api.ebay.com/ws/api.dll", req, response, "GetSessionID");
            session.setAttribute("ebay_session", sessid);
        } catch (ParserConfigurationException | SAXException e) {
            // TODO Auto-generated catch block
            Log.error(e);
        }
        if (code == null) {
            return;
        }

        ec.getOAuthToken("https://api.ebay.com/identity/v1/oauth2/token", code, out);

        // out.println("<a
        // href='https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&runame=Alexander_Tyury-Alexande-Test-P-hgtzx&oauthparams=%26state%3Dnull%26client_id%3DAlexande-Test-PRD-f2466ad41-daaad47d%26redirect_uri%3DAlexander_Tyury-Alexande-Test-P-hgtzx%26response_type%3Dcode%26device_id%3Dnull%26display%3Dnull%26scope%3Dhttps%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.marketing.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.marketing+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.inventory.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.inventory+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.account.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.account+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.fulfillment.readonly+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.fulfillment+https%3A%2F%2Fapi.ebay.com%2Foauth%2Fapi_scope%2Fsell.analytics.readonly%26tt%3D1'>login</a>");
        // new EbayClient().testIt(out);
    }

    public static String toISO8601UTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        //yyyy-MM-dd HH:mm:ss.SSSZ
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssX");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static Date fromISO8601UTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        df.setTimeZone(tz);

        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
