/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import core.attribute.Attribute;
import core.controller.Controller;
import org.bson.Document;

/**
 *
 * @author neoexpert
 */
public class Settings extends Node {

    static {
        addChildType(Domain.class, Settings.class);
        addChildType(Settings.class, Node.class);

    }

    /**
     *
     * @param c
	 * @param doc
     */
    public Settings(Controller c,Document doc) {
		super(c,doc);
	}
    /**
     *
     * @return
     */
    public String getServerName() {
        Attribute server_name = getAttribute("server_name");
        if (server_name != null) {
            return (String) server_name.getValue();
        }
        return null;
    }

}
