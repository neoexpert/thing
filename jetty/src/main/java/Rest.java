
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Rest extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter pw;// = new PrintWriter(res.getOutputStream());
		pw=res.getWriter();

		HttpSession session = req.getSession();
		Object created = session.getAttribute("created");
		if (created == null) {
			session.setAttribute("created", new Date());
		}

		pw.println("Session = " + session.getId());
		pw.println("Created = " + created);

	}

}
