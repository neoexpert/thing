<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="core.*"%>
<%@ page import="web.*"%>
<%@ page import="core.attribute.*"%>

<%@ page import="java.util.*"%>
<%@ page import="core.controller.*"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%>

<!DOCTYPE HTML>
<!-- neoexpert was here twice with maven-->
<!--<compress:html>-->

<html>
<head>
<jsp:include page="HEAD.jsp" />
<script src="/js/core.js?v4"></script>

<%
	HttpController c = (HttpController) request.getAttribute("controller");
	Node n = (Node) request.getAttribute("node");
	Locale locale=(Locale)request.getAttribute("locale");
	Attribute titleattr=n.getAttribute("title");
	if(titleattr!=null)
		out.print("<title>"+titleattr.getValue(locale)+"</title>");
	else
		out.print("<title>"+request.getAttribute("title")+"</title>");
	Attribute metadesc=n.getAttribute("meta-description");
	if(metadesc!=null)
		out.print("<meta name=\"description\" content=\""+metadesc.getValue(locale)+"\"/>");
	Attribute metakeywords=n.getAttribute("meta-keywords");
	if(metakeywords!=null)
		out.print("<meta name=\"keywords\" content=\""+metakeywords.getValue(locale)+"\"/>");
	out.print("<meta http-equiv=\"language\" content=\""+locale.getLanguage()+"\">");
	out.print("<link rel=\"canonical\" href=\""+n.getFrontLink(locale)+"\"/>");
	out.print("<link rel=\"alternate\" hreflang=\"de\" href=\""+n.getFrontLink(new Locale("de"))+"\"/>");
	out.print("<link rel=\"alternate\" hreflang=\"en\" href=\""+n.getFrontLink(new Locale("en"))+"\"/>");
	out.print("<link rel=\"alternate\" hreflang=\"pl\" href=\""+n.getFrontLink(new Locale("pl"))+"\"/>");


	

	c.printDesignBlock("HEAD", out, false);
%>



<script>
	
<%User u = (User) request.getAttribute("user");
				if (u instanceof Zombie || u == null)
					out.print("loggedIn=false;");
				else
					out.print("loggedIn=true;");

				if (u.isInGroup("admin"))
					out.print("var admin=true;");
				else
					out.print("var admin=false;");
				Locale lang = (Locale) request.getAttribute("locale");
				out.print("var lang='" + lang.getLanguage() + "';");%>
	var mobile = true;
	var handleMediaChange = function(mediaQueryList) {
		if (mediaQueryList.matches) {
			$("nav").show();
			mobile = false;
		}
	};
	var mql = window.matchMedia("(min-width: 960px)");
	mql.addListener(handleMediaChange);

	handleMediaChange(mql);

	function togglemenu() {
		$("nav").slideToggle(1000);
	}

	window.addEventListener('popstate', function(event) {
		if (event.state != null)
			loadArea(event.state);
	});
</script>
</head>

<body>
	<%
		c.printDesignBlock("HEADER", out, true);
	%>
	<jsp:include page="WRAPPER.jsp" />
	
	<%
		c.printDesignBlock("FOOTER", out, true);
	%>
	<div id="debugger" hidden></div>
	<script>
		window.onerror = function (message, url, lineNo){
			$("#debugger").append('<div>Error: ' + message + '\n' + 'Line Number: ' + lineNo+"</div>");
    return true;
}
	</script>
	</div>
</body>

</html>
<!--</compress:html>-->

